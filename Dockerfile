### Stage Install ###
FROM node:21.1-bookworm-slim as install



WORKDIR /usr/src/app
ENV PATH /usr/src/app/node_modules/.bin:$PATH
COPY package*.json ./

RUN npm install


### STAGE: Dev
FROM node:21.1-bookworm-slim as dev
ARG BASE_URL=/
ARG MEILI_HOST=http://localhost:7700
ARG MEILI_API_KEY=api_key

ENV NUXT_APP_BASE_URL=${BASE_URL}
ENV NUXT_PUBLIC_MEILI_HOST=${MEILI_HOST}
ENV NUXT_PUBLIC_MEILI_API_KEY=${MEILI_API_KEY}


WORKDIR /usr/src/app
COPY --from=install /usr/src/app ./
COPY . /usr/src/app
EXPOSE 3000 24678 4000
CMD ["npm", "run", "dev"]


### STAGE: Build ###
FROM node:21.1-bookworm-slim as build

WORKDIR /usr/src/app
COPY --from=install /usr/src/app ./
COPY . /usr/src/app
RUN npm run build

### STAGE: serve ###
FROM node:21.1-bookworm-slim as serve

WORKDIR /usr/src/app
COPY --from=build /usr/src/app/.output ./

CMD [ "node", "server/index.mjs"]


### STAGE: Generate ###
FROM node:21.1-bookworm-slim as generate
ARG BASE_URL=/
ARG MEILI_HOST=http://localhost:7700
ARG MEILI_API_KEY

ENV NODE_OPTIONS=--max_old_space_size=8192
ENV NUXT_APP_BASE_URL=${BASE_URL}
ENV NUXT_PUBLIC_MEILI_HOST=${MEILI_HOST}
ENV NUXT_PUBLIC_MEILI_API_KEY=${MEILI_API_KEY}


WORKDIR /usr/src/app
COPY --from=install /usr/src/app ./
COPY . /usr/src/app
RUN npm run generate

### STAGE: NGINX ###
FROM nginxinc/nginx-unprivileged:1.25
# RUN rm -rf /usr/share/nginx/html/*
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=generate /usr/src/app/.output/public /etc/nginx/html
CMD ["nginx", "-g", "daemon off;"]