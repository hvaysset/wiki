import { MeiliSearch } from 'meilisearch'
import { useRuntimeConfig, watchEffect, type MaybeRef, ref, toValue } from '#imports'
import type { FacetDistribution, Hits } from 'meilisearch';
export function useFetchMsDocument(
    index: MaybeRef<string> = ref(""),
    search: Ref<string> = ref(""),
    filter: Ref<string> = ref(''),
    limit: Ref<number> = ref(1000),
    hitsPerPage: Ref<number> = ref(25),
    page: Ref<number> = ref(1),
    facets: Ref<string[]> = ref([]),
    sort: Ref<string[]> = ref([]),
) {
    const runtimeConfig = useRuntimeConfig();

    const client = new MeiliSearch({
        host: runtimeConfig.public.meiliHost,
        apiKey: runtimeConfig.public.meiliApiKey
    })
    const pending = ref(false)
    const filterError = ref(null)
    const hits: Ref<Hits<Record<string, any>>> = ref([])
    const totalHits = ref(0)
    const totalPages = ref(0)
    const facetDistribution: Ref<FacetDistribution | undefined> = ref({})
    // reset page when filter and search change
    watch(filter, () => {
        page.value = 1

    })
    watch(search, () => {
        page.value = 1

    })
    watchEffect(async () => {
        try {
            pending.value = true
            const res = await client
                .index(toValue(index))
                .search(toValue(search), {
                    limit: toValue(limit),
                    filter: toValue(filter),
                    hitsPerPage: toValue(hitsPerPage),
                    page: toValue(page),
                    facets: toValue(facets),
                    sort: toValue(sort),
                })
            filterError.value = null
            const { hits: resHits, totalHits: resTotalHits, totalPages: resTotalPages, facetDistribution: facetD } = res
            totalHits.value = resTotalHits
            hits.value = resHits
            totalPages.value = resTotalPages
            facetDistribution.value = facetD
        } catch ({ code, message }) {
            if (code === 'invalid_search_filter') {
                filterError.value = message
            }
        } finally {
            pending.value = false
        }
    })

    return { hits, totalHits, pending, filterError, totalPages, facetDistribution }
}

