import { withTrailingSlash, withLeadingSlash, joinURL } from "ufo";


export function useRefinedUrl(url: string | Ref<string>) {
    console.log(useRuntimeConfig().app.baseURL)
    const refinedUrl = computed(() => {
        const sanitzedUrl = toValue(url)

        if (sanitzedUrl.startsWith("/") && !sanitzedUrl.startsWith("//")) {
            const _base = withLeadingSlash(
                withTrailingSlash(useRuntimeConfig().app.baseURL)
            );
            console.log(_base)
            if (_base !== "/" && !sanitzedUrl.startsWith(_base)) {
                return joinURL(_base, sanitzedUrl);
            }
        }
        return toValue(url);
    });
    return { refinedUrl }
}