import { md3 } from 'vuetify/blueprints'
// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  modules: [
    '@nuxt/content',
    'vuetify-nuxt-module',
    '@vueuse/nuxt',
    '@pinia/nuxt',
    // '@unocss/nuxt',
  ],
  content: {
    documentDriven: {
      injectPage: false,
    },
    highlight: {
      theme: 'github-light',
      preload: [
        'sh',
      ]
    },
    layoutFallbacks: ['article'],
  },
  vuetify: {
    vuetifyOptions: {
      labComponents: true,
      icons: {
        defaultSet: 'mdi',
        sets: ['mdi', 'fa', 'md'],
      },
      blueprint: md3


    }
  },
  devtools: {
    enabled: false
  },
  runtimeConfig: {

    public: {
      defenseFinderWebservice: '/',
      meiliHost: 'http://localhost:7700',
      meiliApiKey: 'api_key'
    }
  },
  vue: {
    compilerOptions: {
      isCustomElement: (tag) => ['pdbe-molstar'].includes(tag),
    },
  },


})
