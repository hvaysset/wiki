# MR new system template

This is a guide to help you review an article of the wiki. Please check that all the following criteria are met to consider that a page is complete and correct.

- [ ] The article is correctly structured. It should contain :
  1. Header with the following fields (please pay attention to block indentation)

```yaml
---
title: 
tableColumns: 
  article:
    doi: 
    abstract: 
  Sensor: 
  Activator: 
  Effector: 
  PFAM: 
contributors: 
  - Firstname Lastname
relevantAbstracts:
  - doi: my-doi
---
```
  2. The following paragraphs
      - Description
      - Molecular mechanisms
      - Example of genomic structure
      - Distribution of the system among prokaryotes
      - Structure
      - Experimental validation
      - References with the following syntax : `:ref{doi=my-doi}`.

- [ ] The header contains all the required information. The information provided in the header is required to display the list of defense systems.
  - The mandatory fields are :
    - `title:`
    - `article`
    - `doi`
    - `abstract`
    - `PFAM`
  - Optional fields are :
    - `Sensor`
    - `Activator`
    - `Effector`.
  - [ ] Main title is correct. For a defense system, it should simply be the name of the system.
  - [ ] All the information is provided, correct and understandable. Rule of thumb : As a newcommer in the field, does the page provide the essential information in a clear way regarding the system ?
    - [ ] Short description of the system
    - [ ] Composition of the system
    - [ ] Mechanism of action if it is known
    - [ ] References to the literature to further explore the defense system
  - [ ] Links with the database are working.
    - [ ] Distribution of the system among prokaryotes
    - [ ] Structure display
  - [ ] At least one relevant abstract is provided.
