// this code is from : https://gitlab.com/dokos/documentation/-/blob/132cce8c6e2126d8dba8ad09b7c7dd6d08a7c461/mermaid.client.js

import mermaid from "mermaid"

export default defineNuxtPlugin((nuxtApp) => {
    mermaid.initialize({
        startOnLoad: false,
        securityLevel: 'loose',
    })
    nuxtApp.provide('mermaid', () => mermaid)
})
