<!-- The following block is th frontmatter that will be used to build the big table -->
---
title: <!--Replace this with Defense system name-->
tableColumns:
    article:
      doi: <!--Replace this with the main article doi-->
      abstract: 
        <!--Replace this with the abstract of the main article-->
    Sensor: <!--Fill sensor name here. If it's unknown : Leave it blank -->
    Activator: <!--Fill activator name here. If it's unknown : Leave it blank -->
    Effector: <!--Fill effector name here. If it's unknown : Leave it blank -->
    PFAM: <!--PFXXXXX, PFYYYYY, PFZZZZZ-->
relevantAbstracts:
    - doi: 10.xxxx/toto.yyyy.zz 
contributors:
    - John Doe
---

<!--
Markdown syntax in a nutshell, check other pages for example

Use # for titles : # Main title ; ## Secondary title ; ### Tertiary title ; etc.
Use * * for italic : *this text will be displayed in italic*
Use ** ** for bold text : **this text will be displayed in bold**
Use []() for links to external websites : ![This text will be displayed, if you click on it you will be directed to the website](https://url.of.the.website)
Use ![alt text](/system/system.svg) to embed an image. The file of the image should be in a folder with the name of the system, located in /public 
Use for references : "This is a great results :ref{doi=10.xxxx/toto.yyyy.zz}." it will write "This is a great result (Foo et al, 2023)."

That's all folks!

-->


# Defense system name
<!--Replace the main title with the name of the defense system

Example : # CBASS
-->

## Description
This paragraph contains a short description of the defense system. It should provide the essential information regarding the system at a glance.
- How many proteins compose the defense system ? What are their name and what is their biological function ?
- Optionally, you can quickly state in which model organism the system was tested and against which phages.
Please don't forget to refer to the articles that you will cite at the end of the article.
This should be precise but as synthetic as possible (rule of thumb : a dozen of lines maximum).

Example :
"RADAR is comprised of two genes, encoding respectively for an adenosine triphosphatase (RdrA) and  a divergent adenosine deaminase (RdrB) :ref{doi=10.xxxx/toto.yyyy.zz}. which are in some cases associated with a small membrane protein (RdrC or D) :ref{doi=10.xxxx/toto.yyyy.zz}.
RADAR was found to perform RNA editing of adenosine to inosine during phage infection. Editing sites are broadly distributed on the host transcriptome, which could prove deleterious to the host and explain the observed growth arrest of RADAR upon phage infection."


## Molecular mechanism
If the mechanism of action of the defense system is known, please describe it in one short paragraph. If the exact mechanism is not known, simply state it and sum up the information available in the literature.
This should be precise but as synthetic as possible (rule of thumb : a dozen of lines maximum).

Example :
The exact mechanism of action of the Shango defense system has not yet been characterized, but it was shown that the TerB domain and the catalytic activity of the ATPase and the Helicase are required to provide antiviral defense. The fact that TerB domains are known to be associated to the periplasmic membrane could indicate that Shango might be involved in membrane surveillance :ref{doi=10.xxxx/toto.yyyy.zz}.


## Example of genomic structure
<!--Automatically filled.
You can optionally describe in more details which proteins compose the system if the Description paragraph did give all the information-->

## Distribution of the system among prokaryotes
<!--Automatically filled-->

## Structure
<!--Automatically filled-->

## Experimental validation
<!--Give more information about the experimental validation :
- In which organism was it discovered ?
- In which organism was it tested ?
- Against which phages ?
- In which paper was this described ?
Or you can try to build the graph with mermaid (see help page or other page)
-->
