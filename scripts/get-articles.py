#!/usr/bin/env python3

from pyzotero import zotero
import json
import argparse


parser = argparse.ArgumentParser(
    prog="get-articles",
    description="get articles from zotero collection",
)
parser.add_argument("-k", "--key", type=str)  # option that takes a value
args = parser.parse_args()
zot = zotero.Zotero("5151022", "group", args.key)
collection = zot.collection("BSWL96X3")

tot_items = collection["meta"]["numItems"]
batch_size = 100


def get_articles(tot_items, batch_size):
    starts = range(0, tot_items, batch_size)
    for i in starts:
        items = zot.collection_items(
            "BSWL96X3",
            format="csljson",
            limit=batch_size,
            start=i,
            itemType="journalArticle",
            sort="title",
        )["items"]
        for item in items:
            yield item


items = list(get_articles(tot_items, batch_size))

json_object = json.dumps(items, indent=2)
with open("articles.json", "w") as outfile:
    outfile.write(json_object)
