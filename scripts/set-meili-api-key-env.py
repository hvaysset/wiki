#!/usr/bin/env python3

import meilisearch
import argparse


parser = argparse.ArgumentParser(
    prog="set-meili-api-key-env",
    description="Create an env file with meili api key",
)
parser.add_argument("--key", type=str)  # option that takes a value
parser.add_argument("--host", type=str)  # option that takes a value
args = parser.parse_args()

client = meilisearch.Client(args.host, args.key)
keys = client.get_keys()

api_key = [res.key for res in keys.results if res.name == "Default Search API Key"]
if len(api_key) == 1:
    print(f"MEILI_HOST={args.host}")
    print(f"MEILI_API_KEY={api_key[0]}")
