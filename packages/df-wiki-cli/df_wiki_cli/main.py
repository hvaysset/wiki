import typer
from pathlib import Path
from typing_extensions import Annotated
from df_wiki_cli.articles import fetch_articles
from df_wiki_cli.pfam import fetch_pfam
from df_wiki_cli.meilisearch import main

# from df_wiki_cli.ms import main as ms_main
app = typer.Typer()
app.add_typer(main.app, name="meilisearch")


@app.callback()
def callback():
    """
    Manage Defense Finder Wiki data.

    This is a list of commands to perform admin tasks.

    """


@app.command()
def articles(
    output: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=True,
        ),
    ] = "articles.json",
    key: Annotated[str, typer.Option(help="Zotero api key")] = "",
    batch_size: Annotated[
        int, typer.Option(help="Number articles get per request")
    ] = 100,
):
    """
    Get articles metadata from Zotero collection

    """
    if key != "":
        fetch_articles(key, batch_size, output)
    else:
        print("You must provide a zotero api key")
        raise typer.Exit(code=1)


@app.command()
def pfam(
    output: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=True,
        ),
    ]
):
    fetch_pfam(output)
