import json
from pyzotero import zotero
from pathlib import Path


def fetch_articles(key: str, batch_size: int = 100, output: Path = "articles.json"):
    zot = zotero.Zotero("5151022", "group", key)
    collection = zot.collection("BSWL96X3")
    tot_items = collection["meta"]["numItems"]

    def gen_fetch(tot_items, batch_size):
        starts = range(0, tot_items, batch_size)
        for i in starts:
            items = zot.collection_items(
                "BSWL96X3",
                format="csljson",
                limit=batch_size,
                start=i,
                itemType="journalArticle",
                sort="title",
            )["items"]
            for item in items:
                yield item

    items = list(gen_fetch(tot_items, batch_size))
    json_object = json.dumps(items, indent=2)
    with open(output, "w") as outfile:
        outfile.write(json_object)
