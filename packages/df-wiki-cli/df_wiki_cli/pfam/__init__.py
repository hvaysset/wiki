import pandas as pd
import requests
import tempfile
import gzip

URL = "https://ftp.ebi.ac.uk/pub/databases/Pfam/current_release/Pfam-A.hmm.dat.gz"


def fetch_pfam(output):
    with tempfile.NamedTemporaryFile(delete=False) as fp:
        req = requests.get(URL, allow_redirects=True)
        fp.write(req.content)
        fp.close()
        d = {}
        start = False
        with gzip.open(fp.name, "rt") as pf:
            for i, line in enumerate(pf):
                line = line.strip()
                if "# STOCKHOLM 1.0" in line or line == "//":
                    start = True
                else:
                    if start:
                        pfID = line.split()[2]
                        d[pfID] = {}
                        start = False
                    else:
                        kk, v = line.split("   ")
                        g, k = kk.split()
                        d[pfID][k] = v

        df = pd.DataFrame(d).T
        df.index.name = "ID"
        df.reset_index().to_csv(output, index=False, sep=",")
