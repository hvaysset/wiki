import typer
import meilisearch
from typing_extensions import Annotated
from pathlib import Path
from df_wiki_cli.meilisearch import update_refseq, update_structure
from enum import Enum
from types import SimpleNamespace

app = typer.Typer()


class Documents(str, Enum):
    refseq = "refseq"
    structure = "structure"


@app.callback()
def main(
    ctx: typer.Context,
    host: Annotated[
        str, typer.Option(help="Meilisearch host", envvar="MEILI_HOST")
    ] = "http://localhost:7700",
    key: Annotated[
        str, typer.Option(help="Api master key", envvar="MEILI_MASTER_KEY")
    ] = "MASTER_KEY",
):
    """
    Handle meilisearch server.

    commands to manage meilisearch server

    """
    ctx.obj = SimpleNamespace(key=key, host=host)
    # ctx.obj = SimpleNamespace()


@app.command()
def update(
    ctx: typer.Context,
    file: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=True,
        ),
    ],
    document: Annotated[
        Documents, typer.Option(case_sensitive=False)
    ] = Documents.refseq,
    content_type: Annotated[str, typer.Option(help="Content-Type header")] = "text/csv",
):
    if document == "refseq":
        update_refseq(ctx.obj.host, ctx.obj.key, file, document)
    if document == "structure":
        update_structure(ctx.obj.host, ctx.obj.key, file, document)


@app.command()
def delete_all_documents(ctx: typer.Context, id: str):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    index = client.index(id)
    index.delete_all_documents()


@app.command()
def task(ctx: typer.Context, id: str):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    print(client.get_task(id))


@app.command("get-env-var")
def get_env_var(
    ctx: typer.Context,
    output: Annotated[
        Path,
        typer.Option(
            exists=False,
            file_okay=True,
            writable=True,
        ),
    ] = "build.env",
):
    client = meilisearch.Client(ctx.obj.host, ctx.obj.key)
    keys = client.get_keys()

    api_key = [res.key for res in keys.results if res.name == "Default Search API Key"]
    if len(api_key) == 1:
        with open(output, "a") as outfile:
            outfile.write(f"MEILI_HOST={ctx.obj.host}\n")
            outfile.write(f"MEILI_API_KEY={api_key[0]}\n")
