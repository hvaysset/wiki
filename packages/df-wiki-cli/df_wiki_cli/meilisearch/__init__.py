import meilisearch
from pathlib import Path
import csv

from typing import Annotated, List, Optional
from pydantic import BaseModel, Field, BeforeValidator
from enum import Enum


class RefSeqCsv(BaseModel):
    id: str
    replicon: str
    type: str
    subtype: str
    sys_beg: str
    sys_end: str
    protein_in_syst: List[str]
    genes_count: int
    name_of_profiles_in_sys: List[str]
    accession_in_sys: List[str]
    Superkingdom: str
    phylum: str
    class_: str = Field(..., alias="class")
    order: str
    family: str
    genus: str
    species: str


class StructureTypes(str, Enum):
    Validated = "Validated"
    DF = "DF"
    na = "na"


def na_to_none(v: Optional[float]) -> Optional[float]:
    if v == "na":
        return None
    else:
        return v


NaFloat = Annotated[Optional[float], BeforeValidator(na_to_none)]


class StrucutreStatistics(BaseModel):
    id: int
    system: str
    proteins_in_the_prediction: List[str]
    prediction_type: str
    batch: int
    nb_sys: str
    type: StructureTypes
    system_number_of_genes: int
    system_genes: List[str]
    pdb: str
    pae_table: str
    plddt_table: str
    fasta_file: str
    completed: bool
    iptm_ptm: NaFloat = Field(..., alias="iptm+ptm")
    pDockQ: Optional[NaFloat]
    plddts: Optional[NaFloat]


def update_refseq(
    host: str,
    key: str,
    file: Path,
    document: str,
):
    client = meilisearch.Client(host, key)
    index = client.index(document.lower())
    documents = []
    with open(file, "r") as csvfile:
        csvreader = csv.DictReader(csvfile)
        for row in csvreader:
            row["protein_in_syst"] = split_on_comma(row["protein_in_syst"])
            row["name_of_profiles_in_sys"] = split_on_comma(
                row["name_of_profiles_in_sys"]
            )
            row["accession_in_sys"] = split_on_comma(row["accession_in_sys"])
            doc = RefSeqCsv(**row)
            documents.append(doc.model_dump(by_alias=True))
        index.add_documents_in_batches(documents, primary_key="id")
    index.update_pagination_settings({"maxTotalHits": 1000000})
    index.update_filterable_attributes(
        body=[
            "type",
            "subtype",
            "Superkingdom",
            "phylum",
            "order",
            "family",
            "genus",
            "species",
        ]
    )
    index.update_sortable_attributes(
        [
            "type",
            "subtype",
            "Superkingdom",
            "phylum",
            "order",
            "family",
            "genus",
            "species",
        ]
    )


def update_structure(
    host: str,
    key: str,
    file: Path,
    document: str,
):
    client = meilisearch.Client(host, key)
    index = client.index(document.lower())
    documents = []
    with open(file, "r") as csvfile:
        csvreader = csv.DictReader(csvfile)
        for id, row in enumerate(csvreader):
            row["proteins_in_the_prediction"] = split_on_comma(
                row["proteins_in_the_prediction"]
            )
            row["system_genes"] = split_on_comma(row["system_genes"])
            doc = StrucutreStatistics(**row, id=id)
            documents.append(doc.model_dump(by_alias=True))
        tasks = index.add_documents_in_batches(documents, primary_key="id")
        for task in tasks:
            print(task)
    pagination_settings_task = index.update_pagination_settings(
        {"maxTotalHits": 100000}
    )
    print(pagination_settings_task)
    attr_task = index.update_filterable_attributes(
        body=[
            "system",
            "completed",
            "plddts",
        ]
    )
    print(attr_task)
    index.update_sortable_attributes(
        [
            "system",
            "completed",
            "plddts",
        ]
    )


def split_on_comma(str_val: str) -> List[str]:
    for val in str_val.split(","):
        yield val.strip()
