---
title: Shango
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00270, PF00271, PF05099, PF10923, PF13208, PF15615
contributors: 
  - Hugo Vaysset
  - Aude Bernheim
relevantAbstracts:
  - doi: 10.1016/j.chom.2022.09.017
  - doi: 10.1093/nar/gkad317
---

# Shango

## Description
Shango is a three genes defense system which was discovered in parallel in two works in both *E.coli* and *P.aeruginosa* and was shown to have antiphage activity against the Lambda-phage in *E.coli* :ref{doi=10.1016/j.chom.2022.09.017} and against diverse podo- and siphoviridae in *P.aeruginosa* :ref{doi=10.1093/nar/gkad317}.

Shango is composed of (i) a TerB-like domain, (ii) an Helicase and (iii) an ATPase. The TerB domain was previously shown to be associated to the perisplasmic membrane of bacteria :ref{doi=10.4149/gpb_2011_03_286}. 

## Molecular mechanism

The exact mechanism of action of the Shango defense has not yet been characterized, but it was shown that the TerB domain and the catalytic activity of the ATPase and the Helicase are required to provide antiviral defense. The fact that TerB domains are known to be associated to the periplasmic membrane could indicate that Shango might be involved in membrane surveillance :ref{doi=10.1016/j.chom.2022.09.017}.


## Example of genomic structure

The Shango system is composed of 3 proteins: SngC, SngB and, SngA.

Here is an example found in the RefSeq database: 

![shango](/shango/Shango.svg){max-width=750px}

Shango system in the genome of *Paenibacillus sp.* (GCF_022637315.1) is composed of 3 proteins: SngA (WP_241931534.1), SngB (WP_241931535.1)and, SngC (WP_241931536.1).

## Distribution of the system among prokaryotes

The Shango system is present in a total of 385 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1112 genomes (4.9 %).

![shango](/shango/Distribution_Shango.svg){max-width=750px}

*Proportion of genome encoding the Shango system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Shango

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /shango/Shango.Shango__SngC.0.V-plddts_90.0493.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /shango/Shango.Shango__SngC.0.V-plddts_90.0493.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /shango/Shango,Shango__SngA,0,V-plddts_78.29927.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2538938181'>2538938181</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2538938182'>2538938182</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2538938183'>2538938183</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi18
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        LambdaVir
        SECphi18
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

