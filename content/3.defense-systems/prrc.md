---
title: PrrC
layout: article
tableColumns:
    article:
      doi: 10.1186/1743-422X-7-360
      abstract: |
        Over 50 years of biological research with bacteriophage T4 includes notable discoveries in post-transcriptional control, including the genetic code, mRNA, and tRNA; the very foundations of molecular biology. In this review we compile the past 10 - 15 year literature on RNA-protein interactions with T4 and some of its related phages, with particular focus on advances in mRNA decay and processing, and on translational repression. Binding of T4 proteins RegB, RegA, gp32 and gp43 to their cognate target RNAs has been characterized. For several of these, further study is needed for an atomic-level perspective, where resolved structures of RNA-protein complexes are awaiting investigation. Other features of post-transcriptional control are also summarized. These include: RNA structure at translation initiation regions that either inhibit or promote translation initiation; programmed translational bypassing, where T4 orchestrates ribosome bypass of a 50 nucleotide mRNA sequence; phage exclusion systems that involve T4-mediated activation of a latent endoribonuclease (PrrC) and cofactor-assisted activation of EF-Tu proteolysis (Gol-Lit); and potentially important findings on ADP-ribosylation (by Alt and Mod enzymes) of ribosome-associated proteins that might broadly impact protein synthesis in the infected cell. Many of these problems can continue to be addressed with T4, whereas the growing database of T4-related phage genome sequences provides new resources and potentially new phage-host systems to extend the work into a broader biological, evolutionary context.
    Sensor: Monitor the integrity of the bacterial cell machinery
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF00270, PF02384, PF04313, PF04851, PF12008, PF12161, PF13166, PF18766
---

# PrrC
## Example of genomic structure

The PrrC system is composed of 4 proteins: EcoprrI, Type_I_S, PrrC and, Type_I_REases.

Here is an example found in the RefSeq database: 

![prrc](/prrc/PrrC.svg){max-width=750px}

PrrC system in the genome of *Streptococcus canis* (GCF_900636575.1) is composed of 4 proteins: Type_I_REases (WP_003046543.1), PrrC (WP_003046540.1), Type_I_S (WP_129544911.1)and, Type_I_MTases (WP_003046534.1).

## Distribution of the system among prokaryotes

The PrrC system is present in a total of 285 different species.

Among the 22k complete genomes of RefSeq, this system is present in 705 genomes (3.1 %).

![prrc](/prrc/Distribution_PrrC.svg){max-width=750px}

*Proportion of genome encoding the PrrC system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### PrrC

::molstar-pdbe-plugin
---
height: 700
dataUrl: /prrc/PrrC__EcoprrI-plddts_91.30003.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /prrc/PrrC__PrrC-plddts_86.4815.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Jabbar_1984[<a href='https://doi.org/10.1128/JVI.51.2.522-529.1984'>Jabbar and  Snyder, 1984</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_012954793.1'>WP_012954793.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_012954794.1'>WP_012954794.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_012954795.1'>WP_012954795.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_012954796.1'>WP_012954796.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T4 & Dec8
    subgraph Title1[Reference]
        Jabbar_1984
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Lambda
        T4
        Dec8
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1006/jmbi.1995.0343
    - doi: 10.1186/1743-422X-7-360

---
::

