---
title: Septu
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13175, PF13304, PF13476
---

# Septu
## Example of genomic structure

The Septu system is composed of 2 proteins: PtuA and, PtuB.

Here is an example found in the RefSeq database: 

![septu](/septu/Septu.svg){max-width=750px}

Septu system in the genome of *Arcobacter porcinus* (GCF_004299785.2) is composed of 2 proteins: PtuA (WP_066386194.1)and, PtuB_2 (WP_066386193.1).

## Distribution of the system among prokaryotes

The Septu system is present in a total of 911 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2193 genomes (9.6 %).

![septu](/septu/Distribution_Septu.svg){max-width=750px}

*Proportion of genome encoding the Septu system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Septu

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /septu/Septu,Septu__PtuA,0,V-plddts_89.78955.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /septu/Septu,Septu__PtuB,0,V-plddts_95.05244.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /septu/Septu,Septu__PtuA,0,V-plddts_89.78955.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /septu/Septu,Septu__PtuB,0,V-plddts_95.05244.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus thuringiensis 
<a href='https://ncbi.nlm.nih.gov/protein/AMR85048.1'>AMR85048.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AMR85049.1'>AMR85049.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SBSphiJ & SBSphiC
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Origin_1[Bacillus weihenstephanensis 
<a href='https://ncbi.nlm.nih.gov/protein/ABY44616.1'>ABY44616.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ABY44615.1'>ABY44615.1</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> SBSphiC & SpBeta
    subgraph Title1[Reference]
        Doron_2018
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        SBSphiJ
        SBSphiC
        SBSphiC
        SpBeta
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.cell.2020.09.065
    - doi: 10.1093/nar/gkab883
    - doi: 10.1126/science.aar4120

---
::

