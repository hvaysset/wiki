---
title: PD-T7-3
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
---

# PD-T7-3
## Example of genomic structure

The PD-T7-3 system is composed of one protein: PD-T7-3.

Here is an example found in the RefSeq database: 

![pd-t7-3](/pd-t7-3/PD-T7-3.svg){max-width=750px}

PD-T7-3 system in the genome of *Alistipes sp.* (GCF_009557455.1) is composed of 1 protein: PD-T7-3 (WP_153498146.1).

## Distribution of the system among prokaryotes

The PD-T7-3 system is present in a total of 127 different species.

Among the 22k complete genomes of RefSeq, this system is present in 215 genomes (0.9 %).

![pd-t7-3](/pd-t7-3/Distribution_PD-T7-3.svg){max-width=750px}

*Proportion of genome encoding the PD-T7-3 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### PD-T7-3

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-t7-3/PD-T7-3__PD-T7-3-plddts_89.74752.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCP48690.1'>RCP48690.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & T5 & SECphi17 & T3 & T7
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
        T5
        SECphi17
        T3
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41564-022-01219-4

---
::
