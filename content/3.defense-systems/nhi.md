---
title: Nhi
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.03.001
      abstract: |
        The perpetual arms race between bacteria and their viruses (phages) has given rise to diverse immune systems, including restriction-modification and CRISPR-Cas, which sense and degrade phage-derived nucleic acids. These complex systems rely upon production and maintenance of multiple components to achieve antiphage defense. However, the prevalence and effectiveness of minimal, single-component systems that cleave DNA remain unknown. Here, we describe a unique mode of nucleic acid immunity mediated by a single enzyme with nuclease and helicase activities, herein referred to as Nhi (nuclease-helicase immunity). This enzyme provides robust protection against diverse staphylococcal phages and prevents phage DNA accumulation in cells stripped of all other known defenses. Our observations support a model in which Nhi targets and degrades phage-specific replication intermediates. Importantly, Nhi homologs are distributed in diverse bacteria and exhibit functional conservation, highlighting the versatility of such compact weapons as major players in antiphage defense.
    Sensor: Unknown
    Activator: Unknown
    Effector: Nucleic acid degrading (?)
    PFAM: PF01443, PF09848, PF13604
---

# Nhi
## Example of genomic structure

The Nhi system is composed of one protein: Nhi.

Here is an example found in the RefSeq database: 

![nhi](/nhi/Nhi.svg){max-width=750px}

Nhi system in the genome of *Enterococcus avium* (GCF_003711125.1) is composed of 1 protein: Nhi (WP_148712513.1).

## Distribution of the system among prokaryotes

The Nhi system is present in a total of 56 different species.

Among the 22k complete genomes of RefSeq, this system is present in 202 genomes (0.9 %).

![nhi](/nhi/Distribution_Nhi.svg){max-width=750px}

*Proportion of genome encoding the Nhi system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Nhi

::molstar-pdbe-plugin
---
height: 700
dataUrl: /nhi/Nhi__Nhi-plddts_83.89425.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[ Nhi-like
Bacillus cereus] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> phi3T & SpBeta & SPR
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_1
    Origin_1[Staphylococcus epidermidis 
<a href='https://ncbi.nlm.nih.gov/protein/AAW53361.1'>AAW53361.1</a>] --> Expressed_1[Staphylococcus epidermidis]
    Expressed_1[Staphylococcus epidermidis] ----> JBug18 & Pike & CNPx
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_1
    Origin_1[Staphylococcus epidermidis 
<a href='https://ncbi.nlm.nih.gov/protein/AAW53361.1'>AAW53361.1</a>] --> Expressed_2[Staphylococcus aureus]
    Expressed_2[Staphylococcus aureus] ----> Lorac
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_2
    Origin_2[Staphylococcus aureus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000632676.1'>WP_000632676.1</a>] --> Expressed_3[Staphylococcus aureus]
    Expressed_3[Staphylococcus aureus] ----> Lorac
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_3
    Origin_3[Staphylococcus aureus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045177897.1'>WP_045177897.1</a>] --> Expressed_4[Staphylococcus aureus]
    Expressed_4[Staphylococcus aureus] ----> Lorac
    Bari_2022[<a href='https://doi.org/10.1016/j.chom.2022.03.001'>Bari et al., 2022</a>] --> Origin_4
    Origin_4[Vibrio vulnificus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_101958732.1'>WP_101958732.1</a>] --> Expressed_5[Staphylococcus aureus]
    Expressed_5[Staphylococcus aureus] ----> Lorac
    subgraph Title1[Reference]
        Millman_2022
        Bari_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_1
        Origin_2
        Origin_3
        Origin_4
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_5
end
    subgraph Title4[Phage infected]
        phi3T
        SpBeta
        SPR
        JBug18
        Pike
        CNPx
        Lorac
        Lorac
        Lorac
        Lorac
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.03.001

---
::

