---
title: MMB_gp29_gp30
layout: article
tableColumns:
    article:
      doi: 10.1038/nmicrobiol.2016.251
      abstract: |
        Temperate phages are common, and prophages are abundant residents of sequenced bacterial genomes. Mycobacteriophages are viruses that infect mycobacterial hosts including Mycobacterium tuberculosis and Mycobacterium smegmatis, encompass substantial genetic diversity and are commonly temperate. Characterization of ten Cluster N temperate mycobacteriophages revealed at least five distinct prophage-expressed viral defence systems that interfere with the infection of lytic and temperate phages that are either closely related (homotypic defence) or unrelated (heterotypic defence) to the prophage. Target specificity is unpredictable, ranging from a single target phage to one-third of those tested. The defence systems include a single-subunit restriction system, a heterotypic exclusion system and a predicted (p)ppGpp synthetase, which blocks lytic phage growth, promotes bacterial survival and enables efficient lysogeny. The predicted (p)ppGpp synthetase coded by the Phrann prophage defends against phage Tweety infection, but Tweety codes for a tetrapeptide repeat protein, gp54, which acts as a highly effective counter-defence system. Prophage-mediated viral defence offers an efficient mechanism for bacterial success in host-virus dynamics, and counter-defence promotes phage co-evolution.
---

# MMB_gp29_gp30

## To do 

## Structure

### MMB_gp29_gp30

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mmb_gp29_gp30/MMB_gp29_gp30__gp29-plddts_82.71079.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mmb_gp29_gp30/MMB_gp29_gp30__gp30-plddts_65.6043.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Dedrick_2017[<a href='https://doi.org/10.1038/nmicrobiol.2016.251'>Dedrick et al., 2017</a>] --> Origin_0
    Origin_0[Mycobacterium MMB phage 
<a href='https://ncbi.nlm.nih.gov/protein/YP_009006630.1'>YP_009006630.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/YP_009006631.2'>YP_009006631.2</a>] --> Expressed_0[Mycobacterium smegmatis mc2 155]
    Expressed_0[Mycobacterium smegmatis mc2 155] ----> Bxz2 & Tweety & TM4
    subgraph Title1[Reference]
        Dedrick_2017
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Bxz2
        Tweety
        TM4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1038/nmicrobiol.2016.251

---
::
