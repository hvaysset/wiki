---
title: Rst_HelicaseDUF2290
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF10053, PF13538
---

# Rst_HelicaseDUF2290
## Example of genomic structure

The Rst_HelicaseDUF2290 system is composed of 2 proteins: Helicase and, DUF2290.

Here is an example found in the RefSeq database: 

![rst_helicaseduf2290](/rst_helicaseduf2290/Rst_HelicaseDUF2290.svg){max-width=750px}

Rst_HelicaseDUF2290 system in the genome of *Xylella fastidiosa* (GCF_021459885.1) is composed of 2 proteins: Helicase (WP_012338122.1)and, DUF2290 (WP_004084731.1).

## Distribution of the system among prokaryotes

The Rst_HelicaseDUF2290 system is present in a total of 124 different species.

Among the 22k complete genomes of RefSeq, this system is present in 211 genomes (0.9 %).

![rst_helicaseduf2290](/rst_helicaseduf2290/Distribution_Rst_HelicaseDUF2290.svg){max-width=750px}

*Proportion of genome encoding the Rst_HelicaseDUF2290 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Rst_HelicaseDUF2290

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_helicaseduf2290/Rst_HelicaseDUF2290,Rst_HelicaseDUF2290__DUF2290,0,V-plddts_90.99466.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_helicaseduf2290/Rst_HelicaseDUF2290,Rst_HelicaseDUF2290__Helicase,0,V-plddts_87.6023.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae P4 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_046623503.1'>WP_046623503.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_046623504.1'>WP_046623504.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.02.018

---
::

