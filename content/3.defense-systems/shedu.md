---
title: Shedu
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF14082
---

# Shedu
## Example of genomic structure

The Shedu system is composed of one protein: SduA.

Here is an example found in the RefSeq database: 

![shedu](/shedu/Shedu.svg){max-width=750px}

Shedu system in the genome of *Mycolicibacterium psychrotolerans* (GCF_010729305.1) is composed of 1 protein: SduA (WP_246228780.1).

## Distribution of the system among prokaryotes

The Shedu system is present in a total of 483 different species.

Among the 22k complete genomes of RefSeq, this system is present in 899 genomes (3.9 %).

![shedu](/shedu/Distribution_Shedu.svg){max-width=750px}

*Proportion of genome encoding the Shedu system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Shedu

::molstar-pdbe-plugin
---
height: 700
dataUrl: /shedu/Shedu__SduA-plddts_87.27097.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/ACK61957.1'>ACK61957.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> phi105 & rho14 & SPP1 & phi29
    subgraph Title1[Reference]
        Doron_2018
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        phi105
        rho14
        SPP1
        phi29
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aar4120

---
::

