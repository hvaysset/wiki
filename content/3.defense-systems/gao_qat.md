---
title: Gao_Qat
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01026, PF07693
---

# Gao_Qat
## Example of genomic structure

The Gao_Qat system is composed of 4 proteins: QatA, QatB, QatC and, QatD.

Here is an example found in the RefSeq database: 

![gao_qat](/gao_qat/Gao_Qat.svg){max-width=750px}

Gao_Qat system in the genome of *Raoultella ornithinolytica* (GCF_002214825.1) is composed of 4 proteins: QatA (WP_088883811.1), QatB (WP_127146083.1), QatC (WP_088883813.1)and, QatD (WP_088883814.1).

## Distribution of the system among prokaryotes

The Gao_Qat system is present in a total of 246 different species.

Among the 22k complete genomes of RefSeq, this system is present in 645 genomes (2.8 %).

![gao_qat](/gao_qat/Distribution_Gao_Qat.svg){max-width=750px}

*Proportion of genome encoding the Gao_Qat system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Gao_Qat

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_qat/Gao_Qat,Gao_Qat__QatA,0,V-plddts_81.28827.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_qat/Gao_Qat,Gao_Qat__QatB,0,V-plddts_86.3728.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_qat/Gao_Qat,Gao_Qat__QatC,0,V-plddts_93.16811.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_qat/Gao_Qat,Gao_Qat__QatD,0,V-plddts_94.78681.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/STG85056.1'>STG85056.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/STG85057.1'>STG85057.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/STG85058.1'>STG85058.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/STG85059.1'>STG85059.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1 & Lambda
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        P1
        Lambda
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aba0372

---
::

