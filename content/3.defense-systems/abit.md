---
title: AbiT
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF18864
---

# AbiT
## Example of genomic structure

The AbiT system is composed of 2 proteins: AbiTii and, AbiTi.

Here is an example found in the RefSeq database: 

![abit](/abit/AbiT.svg){max-width=750px}

AbiT system in the genome of *Sphaerochaeta associata* (GCF_022869165.1) is composed of 2 proteins: AbiTi (WP_244771454.1)and, AbiTii (WP_244771455.1).

## Distribution of the system among prokaryotes

The AbiT system is present in a total of 5 different species.

Among the 22k complete genomes of RefSeq, this system is present in 8 genomes (0.0 %).

![abit](/abit/Distribution_AbiT.svg){max-width=750px}

*Proportion of genome encoding the AbiT system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiT

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abit/AbiT,AbiT__AbiTi,0,V-plddts_81.84478.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abit/AbiT,AbiT__AbiTii,0,V-plddts_88.46375.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAN60762.1'>AAN60762.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AAN60763.1'>AAN60763.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        936
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006

---
::

