---
title: NixI
layout: article
tableColumns:
    article:
      doi: 10.1101/2021.07.12.452122
      abstract: |
        PLEs are phage parasites integrated into the chromosome of epidemic Vibrio cholerae. In response to infection by its viral host ICP1, PLE excises, replicates and hijacks ICP1 structural components for transduction. Through an unknown mechanism PLE prevents ICP1 from transitioning to rolling circle replication (RCR), a prerequisite for efficient packaging of the viral genome. Here, we characterize a PLE-encoded nuclease, NixI, that blocks phage development likely by nicking ICP1’s genome as it transitions to RCR. NixI-dependent cleavage sites appear in ICP1’s genome during infection of PLE(+) V. cholerae. Purified NixI demonstrates in vitro specificity for sites in ICP1’s genome and NixI activity is enhanced by a putative specificity determinant co-expressed with NixI during phage infection. Importantly, NixI is sufficient to limit ICP1 genome replication and eliminate progeny production. We identify distant NixI homologs in an expanded family of putative phage satellites in Vibrios that lack nucleotide homology to PLEs but nonetheless share genomic synteny with PLEs. More generally, our results reveal a previously unknown mechanism deployed by phage parasites to limit packaging of their viral hosts’ genome and highlight the prominent role of nuclease effectors as weapons in the arms race between antagonizing genomes.
    Sensor: Unknown
    Activator: Unknown
    Effector: Nucleic acid degrading
---

# NixI
## Example of genomic structure

The NixI system is composed of 2 proteins: NixI and, Stix.

Here is an example found in the RefSeq database: 

![nixi](/nixi/NixI.svg){max-width=750px}

NixI system in the genome of *Vibrio cholerae* (GCF_009646135.1) is composed of 2 proteins: NixI (WP_001147214.1)and, Stix (WP_000628297.1).

## Distribution of the system among prokaryotes

The NixI system is present in a total of 8 different species.

Among the 22k complete genomes of RefSeq, this system is present in 19 genomes (0.1 %).

![nixi](/nixi/Distribution_NixI.svg){max-width=750px}

*Proportion of genome encoding the NixI system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### NixI

::molstar-pdbe-plugin
---
height: 700
dataUrl: /nixi/NixI__NixI-plddts_92.38782.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Legault_2022[<a href='https://doi.org/10.1093/nar/gkac002'>LeGault et al., 2022</a>] --> Origin_0
    Origin_0[Vibrio cholerae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045177897.1'>WP_045177897.1</a>] --> Expressed_0[Vibrio cholerae]
    Expressed_0[Vibrio cholerae] ----> ICP1
    subgraph Title1[Reference]
        Legault_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        ICP1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1101/2021.07.12.452122

---
::
