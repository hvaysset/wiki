---
title: Wadjet
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Detecting invading nucleic acid
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF09660, PF09661, PF09664, PF09983, PF11795, PF11796, PF11855, PF13555, PF13558, PF13835
---

# Wadjet
## Example of genomic structure

The Wadjet system have been describe in a total of 4 subsystems.

Here is some example found in the RefSeq database:

![wadjet](/wadjet/Wadjet_I.svg){max-width=750px}

Wadjet_I subsystem in the genome of *Bifidobacterium pseudocatenulatum* (GCF_021484885.1) is composed of 4 proteins: JetA_I (WP_195524168.1), JetB_I (WP_195523897.1), JetC_I (WP_195523898.1)and, JetD_I (WP_229067172.1).

![wadjet](/wadjet/Wadjet_II.svg){max-width=750px}

Wadjet_II subsystem in the genome of *Streptomyces sp.* (GCF_023273835.1) is composed of 4 proteins: JetD_II (WP_248777007.1), JetC_II (WP_248777008.1), JetB_II (WP_248777009.1)and, JetA_II (WP_248777010.1).

![wadjet](/wadjet/Wadjet_III.svg){max-width=750px}

Wadjet_III subsystem in the genome of *Caldibacillus thermoamylovorans* (GCF_003096215.1) is composed of 4 proteins: JetD_III (WP_108897743.1), JetA_III (WP_108897744.1), JetB_III (WP_034768879.1)and, JetC_III (WP_108897745.1).

## Distribution of the system among prokaryotes

The Wadjet system is present in a total of 1151 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2380 genomes (10.4 %).

![wadjet](/wadjet/Distribution_Wadjet.svg){max-width=750px}

*Proportion of genome encoding the Wadjet system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Wadjet_I

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_I,Wadjet__JetA_I,0,V-plddts_85.26647.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_I,Wadjet__JetB_I,0,V-plddts_87.82736.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_I,Wadjet__JetC_I,0,V-plddts_84.15592.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_I,Wadjet__JetD_I,0,V-plddts_93.08112.pdb
---
::

### Wadjet_II

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_II,Wadjet__JetA_II,0,V-plddts_85.37749.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_II,Wadjet__JetB_II,0,V-plddts_94.08089.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_II,Wadjet__JetC_II,0,V-plddts_84.09293.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_II,Wadjet__JetD_II,0,V-plddts_92.28569.pdb
---
::

### Wadjet_III

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_III,Wadjet__JetA_III,0,V-plddts_80.48148.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_III,Wadjet__JetB_III,0,V-plddts_80.7136.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_III,Wadjet__JetC_III,0,V-plddts_83.96933.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /wadjet/Wadjet_III,Wadjet__JetD_III,0,V-plddts_91.07357.pdb
---
::

## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aar4120

---
::

