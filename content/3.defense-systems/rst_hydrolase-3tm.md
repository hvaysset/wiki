---
title: Rst_Hydrolase-3Tm
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13242, PF13419
---

# Rst_Hydrolase-3Tm
## Example of genomic structure

The Rst_Hydrolase-3Tm system is composed of 2 proteins: Hydrolase and, Hydrolase-Tm.

Here is an example found in the RefSeq database: 

![rst_hydrolase-3tm](/rst_hydrolase-3tm/Rst_Hydrolase-Tm.svg){max-width=750px}

Rst_Hydrolase-Tm subsystem in the genome of *Escherichia coli* (GCF_004792495.1) is composed of 2 proteins: Hydrolase-Tm (WP_000998640.1)and, Hydrolase (WP_000754434.1).

## Distribution of the system among prokaryotes

The Rst_Hydrolase-3Tm system is present in a total of 34 different species.

Among the 22k complete genomes of RefSeq, this system is present in 42 genomes (0.2 %).

![rst_hydrolase-3tm](/rst_hydrolase-3tm/Distribution_Rst_Hydrolase-3Tm.svg){max-width=750px}

*Proportion of genome encoding the Rst_Hydrolase-3Tm system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Rst_Hydrolase-Tm

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_hydrolase-3tm/Rst_Hydrolase-Tm,Rst_Hydrolase-Tm__Hydrolase,0,V-plddts_94.5717.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_hydrolase-3tm/Rst_Hydrolase-Tm,Rst_Hydrolase-Tm__Hydrolase-Tm,0,V-plddts_87.29824.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli P4 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000754434.1'>WP_000754434.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001401335.1'>WP_001401335.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.02.018

---
::

