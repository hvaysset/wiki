---
title: Gao_Upx
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
contributors:
    - Marian Dominguez-Mirazo
relevantAbstracts:
    - doi: 10.1126/science.aba0372
---

# Gao_upx
## Description
The Gao_upx system is composed by a single protein. It was predicted through a guilty by association approach independent of domain annotations and validated in a heterologous system :ref{doi=10.1126/science.aba0372}. It's been identified as part of a highly conserved core defense hotspot in *Pseudomonas aeruginosa* strains :ref{doi=10.1093/nar/gkad317}. 
## Molecular mechanisms
As far as we are aware, the molecular mechanism is unknown. 
## Example of genomic structure

The Gao_Upx system is composed of one protein: UpxA.

Here is an example found in the RefSeq database: 

![gao_upx](/gao_upx/Gao_Upx.svg){max-width=750px}

Gao_Upx system in the genome of *Salmonella sp.* (GCF_020268625.1) is composed of 1 protein: UpxA (WP_060647174.1).

## Distribution of the system among prokaryotes

The Gao_Upx system is present in a total of 31 different species.

Among the 22k complete genomes of RefSeq, this system is present in 39 genomes (0.2 %).

![gao_upx](/gao_upx/Distribution_Gao_Upx.svg){max-width=750px}

*Proportion of genome encoding the Gao_Upx system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Gao_Upx

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_upx/Gao_Upx__UpxA-plddts_90.66658.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_060647174.1'>WP_060647174.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1 & PhiV-1
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        P1
        PhiV-1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
