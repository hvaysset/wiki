---
title: Hachiman
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00270, PF00271, PF04851, PF08878, PF14130
---

# Hachiman
## Description

Hachiman Type 1 systems were the first discovered and can be found in 3.4% of microbial genomes (1). Hachiman Type 1 systems are encoded by two genes, *hamA* (annotated as a Domain of Unknown Function, DUF) and *hamB* (annotated as a helicase) (1). 

More recently, Hachiman Type 2 systems were discovered and appeared to include a third gene, encoded for a DUF protein (HamC) (2).

## Example of genomic structure

The Hachiman type I system is composed of 2 proteins: HamB and, HamA.

Here is an example found in the RefSeq database: 

![hachiman](/hachiman/Hachiman.svg){max-width=750px}

Hachiman system in the genome of *Mesorhizobium terrae* (GCF_008727715.1) is composed of 2 proteins: HamA_1 (WP_245317480.1)and, HamB (WP_065997554.1).

## Distribution of the system among prokaryotes

The Hachiman system is present in a total of 518 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1361 genomes (6.0 %).

![hachiman](/hachiman/Distribution_Hachiman.svg){max-width=750px}

*Proportion of genome encoding the Hachiman system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Hachiman

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /hachiman/Hachiman.Hachiman__HamB.0.V-plddts_88.00493.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /hachiman/Hachiman.Hachiman__HamB.0.V-plddts_88.00493.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/KLA13163.1'>KLA13163.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/KLA13162.1'>KLA13162.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SBSphiJ & phi3T & SPbeta & SPR & phi105 & rho14 & phi29
    Payne_2021[<a href='https://doi.org/10.1093/nar/gkab883'>Payne et al., 2021</a>] --> Origin_1
    Origin_1[ Hachiman Type II
Sphingopyxis witflariensis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_201791916.1'>WP_201791916.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_201791915.1'>WP_201791915.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_088473955.1'>WP_088473955.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T3 & PVP-SE1
    subgraph Title1[Reference]
        Doron_2018
        Payne_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        SBSphiJ
        phi3T
        SPbeta
        SPR
        phi105
        rho14
        phi29
        T3
        PVP-SE1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aar4120

---
::


## References

1. Doron S, Melamed S, Ofir G, et al. Systematic discovery of antiphage defense systems in the microbial pangenome. *Science*. 2018;359(6379):eaar4120. doi:10.1126/science.aar4120

2. Payne LJ, Todeschini TC, Wu Y, Perry BJ, Ronson CW, Fineran PC, Nobrega FL, Jackson SA. Identification and classification of antiviral defence systems in bacteria and archaea with PADLOC reveals new system types. Nucleic Acids Res. 2021 Nov 8;49(19):10868-10878. doi: 10.1093/nar/gkab883. PMID: 34606606; PMCID: PMC8565338.

