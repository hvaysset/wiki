---
title: SanaTA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.molcel.2013.02.002
      abstract: |
        Toxin-antitoxin (TA) modules, composed of a toxic protein and a counteracting antitoxin, play important roles in bacterial physiology. We examined the experimental insertion of 1.5 million genes from 388 microbial genomes into an Escherichia coli host using more than 8.5 million random clones. This revealed hundreds of genes (toxins) that could only be cloned when the neighboring gene (antitoxin) was present on the same clone. Clustering of these genes revealed TA families widespread in bacterial genomes, some of which deviate from the classical characteristics previously described for such modules. Introduction of these genes into E. coli validated that the toxin toxicity is mitigated by the antitoxin. Infection experiments with T7 phage showed that two of the new modules can provide resistance against phage. Moreover, our experiments revealed an "antidefense" protein in phage T7 that neutralizes phage resistance. Our results expose active fronts in the arms race between bacteria and phage.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF08843
---

# SanaTA
## Example of genomic structure

The SanaTA system is composed of 2 proteins: SanaT and, SanaA.

Here is an example found in the RefSeq database: 

![sanata](/sanata/SanaTA.svg){max-width=750px}

SanaTA system in the genome of *Methylovorus glucosetrophus* (GCF_000023745.1) is composed of 2 proteins: SanaT (WP_015830669.1)and, SanaA (WP_015830670.1).

## Distribution of the system among prokaryotes

The SanaTA system is present in a total of 504 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1071 genomes (4.7 %).

![sanata](/sanata/Distribution_SanaTA.svg){max-width=750px}

*Proportion of genome encoding the SanaTA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### SanaTA

::molstar-pdbe-plugin
---
height: 700
dataUrl: /sanata/SanaTA,SanaTA_SanaA,0,V-plddts_86.02229.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /sanata/SanaTA,SanaTA_SanaT,0,V-plddts_94.5954.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Sberro_2013[<a href='https://doi.org/10.1016/j.molcel.2013.02.002'>Sberro et al., 2013</a>] --> Origin_0
    Origin_0[Shewanella sp. ANA-3 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=639720518'>639720518</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=639720519'>639720519</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Sberro_2013
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.molcel.2013.02.002

---
::

