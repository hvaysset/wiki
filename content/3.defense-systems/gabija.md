---
title: Gabija
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Direct
    Effector: Degrading nucleic acids
    PFAM: PF00580, PF11398, PF13175, PF13245, PF13304, PF13361, PF13476
---

# Gabija
## Description

According to recent studies, GajA is a sequence-specific DNA nicking endonuclease, whose activity is inhibited by nucleotide concentration. Accordingly, GajA would be fully inhibited at cellular nucleotides concentrations. It was hypothesized that upon nucleotide depletion during phage infection, GajA would become activated (2). 

Another study suggests that the *gajB* gene could encode for an NTPase, which would form a complex with GajA to achieve anti-phage defense (3).

## Molecular mechanism

The precise mechanism of the Gabija system remains to be fully described, yet studies suggest that it could act either as a nucleic acid degrading system or as an abortive infection system.

## Example of genomic structure

The Gabija system is composed of 2 proteins: GajA and, GajB_2.

Here is an example found in the RefSeq database: 

![gabija](/gabija/Gabija.svg){max-width=750px}

Gabija system in the genome of *Vibrio parahaemolyticus* (GCF_009883895.1) is composed of 2 proteins: GajA (WP_085576823.1)and, GajB_1 (WP_031856308.1).

## Distribution of the system among prokaryotes

The Gabija system is present in a total of 1200 different species.

Among the 22k complete genomes of RefSeq, this system is present in 3762 genomes (16.5 %).

![gabija](/gabija/Distribution_Gabija.svg){max-width=750px}

*Proportion of genome encoding the Gabija system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Gabija

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gabija/Gabija,Gabija_GajB,0,V-plddts_91.3911.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gabija/Gabija,Gabija__GajA,0,V-plddts_86.08162.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus cereus strain VD045 
<a href='https://ncbi.nlm.nih.gov/protein/EJR29742.1'>EJR29742.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJR29743.1'>EJR29743.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SBSphiC & SpBeta & phi105 & rho14 & phi29
    Cheng_2021[<a href='https://doi.org/10.1093/nar/gkab277'>Cheng et al., 2021</a>] --> Origin_0
    Origin_0[Bacillus cereus strain VD045 
<a href='https://ncbi.nlm.nih.gov/protein/EJR29742.1'>EJR29742.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJR29743.1'>EJR29743.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T7
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Origin_1[Bacillus cereus strain HuB5-5 
<a href='https://ncbi.nlm.nih.gov/protein/EJQ91274.1'>EJQ91274.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJQ91275.1'>EJQ91275.1</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> SpBeta & phi105
    subgraph Title1[Reference]
        Doron_2018
        Cheng_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_2
        Expressed_1
end
    subgraph Title4[Phage infected]
        SBSphiC
        SpBeta
        phi105
        rho14
        phi29
        T7
        SpBeta
        phi105
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1093/nar/gkab277
    - doi: 10.1126/science.aar4120

---
::

