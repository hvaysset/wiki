---
title: RosmerTA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01381, PF06114, PF12844, PF13443, PF13560
---

# RosmerTA
## Example of genomic structure

The RosmerTA system is composed of 2 proteins: RmrA_2634932349 and, RmrT_2634932349.

Here is an example found in the RefSeq database: 

![rosmerta](/rosmerta/RosmerTA.svg){max-width=750px}

RosmerTA system in the genome of *Acinetobacter indicus* (GCF_002953575.2) is composed of 2 proteins: RmrT_2641389401 (WP_045796188.1)and, RmrA_2641389401 (WP_045796189.1).

## Distribution of the system among prokaryotes

The RosmerTA system is present in a total of 540 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2124 genomes (9.3 %).

![rosmerta](/rosmerta/Distribution_RosmerTA.svg){max-width=750px}

*Proportion of genome encoding the RosmerTA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### RosmerTA

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rosmerta/RosmerTA,RosmerTA__RmrA,0,V-plddts_88.40961.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rosmerta/RosmerTA,RosmerTA__RmrT,0,V-plddts_84.3404.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2658042940'>2658042940</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2658042941'>2658042941</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1 & LambdaVir
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        P1
        LambdaVir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017

---
::

