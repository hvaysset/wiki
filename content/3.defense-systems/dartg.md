---
title: DarTG
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01153-5
      abstract: |
        Toxin-antitoxin (TA) systems are broadly distributed, yet poorly conserved, genetic elements whose biological functions are unclear and controversial. Some TA systems may provide bacteria with immunity to infection by their ubiquitous viral predators, bacteriophages. To identify such TA systems, we searched bioinformatically for those frequently encoded near known phage defence genes in bacterial genomes. This search identified homologues of DarTG, a recently discovered family of TA systems whose biological functions and natural activating conditions were unclear. Representatives from two different subfamilies, DarTG1 and DarTG2, strongly protected E. coli MG1655 against different phages. We demonstrate that for each system, infection with either RB69 or T5 phage, respectively, triggers release of the DarT toxin, a DNA ADP-ribosyltransferase, that then modifies viral DNA and prevents replication, thereby blocking the production of mature virions. Further, we isolated phages that have evolved to overcome DarTG defence either through mutations to their DNA polymerase or to an anti-DarT factor, gp61.2, encoded by many T-even phages. Collectively, our results indicate that phage defence may be a common function for TA systems and reveal the mechanism by which DarTG systems inhibit phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Nucleic acid degrading (ADP-ribosylation)
    PFAM: PF01661, PF14487
---

# DarTG
## Example of genomic structure

The DarTG system is composed of 2 proteins: DarT and, DarG.

Here is an example found in the RefSeq database: 

![dartg](/dartg/DarTG.svg){max-width=750px}

DarTG system in the genome of *Mycobacterium tuberculosis* (GCF_904810345.1) is composed of 2 proteins: DarT (WP_003400548.1)and, DarG (WP_003400551.1).

## Distribution of the system among prokaryotes

The DarTG system is present in a total of 356 different species.

Among the 22k complete genomes of RefSeq, this system is present in 955 genomes (4.2 %).

![dartg](/dartg/Distribution_DarTG.svg){max-width=750px}

*Proportion of genome encoding the DarTG system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### DarTG

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dartg/DarTG,DarTG_DarG,0,DF-plddts_94.40611.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dartg/DarTG,DarTG_DarT,0,DF-plddts_94.62475.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Leroux_2022[<a href='https://doi.org/10.1038/s41564-022-01153-5'>LeRoux et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli strain C7] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> RB69 & T5 & SECphi18 & Lust
    subgraph Title1[Reference]
        Leroux_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        RB69
        T5
        SECphi18
        Lust
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41564-022-01153-5

---
::

