---
title: pAgo
layout: article
tableColumns:
    article:
      doi: 10.1186/1745-6150-4-29
      abstract: |
        BACKGROUND: In eukaryotes, RNA interference (RNAi) is a major mechanism of defense against viruses and transposable elements as well of regulating translation of endogenous mRNAs. The RNAi systems recognize the target RNA molecules via small guide RNAs that are completely or partially complementary to a region of the target. Key components of the RNAi systems are proteins of the Argonaute-PIWI family some of which function as slicers, the nucleases that cleave the target RNA that is base-paired to a guide RNA. Numerous prokaryotes possess the CRISPR-associated system (CASS) of defense against phages and plasmids that is, in part, mechanistically analogous but not homologous to eukaryotic RNAi systems. Many prokaryotes also encode homologs of Argonaute-PIWI proteins but their functions remain unknown. RESULTS: We present a detailed analysis of Argonaute-PIWI protein sequences and the genomic neighborhoods of the respective genes in prokaryotes. Whereas eukaryotic Ago/PIWI proteins always contain PAZ (oligonucleotide binding) and PIWI (active or inactivated nuclease) domains, the prokaryotic Argonaute homologs (pAgos) fall into two major groups in which the PAZ domain is either present or absent. The monophyly of each group is supported by a phylogenetic analysis of the conserved PIWI-domains. Almost all pAgos that lack a PAZ domain appear to be inactivated, and the respective genes are associated with a variety of predicted nucleases in putative operons. An additional, uncharacterized domain that is fused to various nucleases appears to be a unique signature of operons encoding the short (lacking PAZ) pAgo form. By contrast, almost all PAZ-domain containing pAgos are predicted to be active nucleases. Some proteins of this group (e.g., that from Aquifex aeolicus) have been experimentally shown to possess nuclease activity, and are not typically associated with genes for other (putative) nucleases. Given these observations, the apparent extensive horizontal transfer of pAgo genes, and their common, statistically significant over-representation in genomic neighborhoods enriched in genes encoding proteins involved in the defense against phages and/or plasmids, we hypothesize that pAgos are key components of a novel class of defense systems. The PAZ-domain containing pAgos are predicted to directly destroy virus or plasmid nucleic acids via their nuclease activity, whereas the apparently inactivated, PAZ-lacking pAgos could be structural subunits of protein complexes that contain, as active moieties, the putative nucleases that we predict to be co-expressed with these pAgos. All these nucleases are predicted to be DNA endonucleases, so it seems most probable that the putative novel phage/plasmid-defense system targets phage DNA rather than mRNAs. Given that in eukaryotic RNAi systems, the PAZ domain binds a guide RNA and positions it on the complementary region of the target, we further speculate that pAgos function on a similar principle (the guide being either DNA or RNA), and that the uncharacterized domain found in putative operons with the short forms of pAgos is a functional substitute for the PAZ domain. CONCLUSION: The hypothesis that pAgos are key components of a novel prokaryotic immune system that employs guide RNA or DNA molecules to degrade nucleic acids of invading mobile elements implies a functional analogy with the prokaryotic CASS and a direct evolutionary connection with eukaryotic RNAi. The predictions of the hypothesis including both the activities of pAgos and those of the associated endonucleases are readily amenable to experimental tests.
    Sensor: Detecting invading nucleic acid
    Activator: Direct
    Effector: Diverse (Nucleotide modifyingn, Membrane disrupting)
    PFAM: PF02171, PF13289, PF13676, PF14280, PF18742
---

# pAgo
## Example of genomic structure

The pAgo system is composed of one protein: pAgo_Short.

Here is an example found in the RefSeq database: 

![pago](/pago/pAgo.svg){max-width=750px}

pAgo system in the genome of *Ensifer adhaerens* (GCF_020405145.1) is composed of 1 protein: pAgo_LongB (WP_218685258.1).

## Distribution of the system among prokaryotes

The pAgo system is present in a total of 435 different species.

Among the 22k complete genomes of RefSeq, this system is present in 598 genomes (2.6 %).

![pago](/pago/Distribution_pAgo.svg){max-width=750px}

*Proportion of genome encoding the pAgo system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### pAgo

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongA-plddts_90.01396.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongA-plddts_90.01396.pdb
---
::

Example 3:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongA-plddts_90.01396.pdb
---
::

Example 4:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongA-plddts_90.01396.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongB-plddts_92.47739.pdb
---
::

Example 5:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongA-plddts_90.01396.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongB-plddts_92.47739.pdb
---
::

Example 6:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongA-plddts_90.01396.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pago/pAgo__pAgo_LongB-plddts_92.47739.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Kuzmenko_2020[<a href='https://doi.org/10.1038/s41586-020-2605-1'>Kuzmenko et al., 2020</a>] --> Origin_0
    Origin_0[ Ago
Clostridium butyricum 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045143632.1'>WP_045143632.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> M13 & P1vir
    Xing_2022[<a href='https://doi.org/10.1128/mbio.03656-21'>Xing et al., 2022</a>] --> Origin_1
    Origin_1[Natronobacterium gregoryi 
<a href='https://ncbi.nlm.nih.gov/protein/WP_005580376.1'>WP_005580376.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T7
    Zaremba_2022[<a href='https://doi.org/10.1038/s41564-022-01239-0'>Zaremba et al., 2022</a>] --> Origin_2
    Origin_2[ GsSir2/Ago
Geobacter sulfurreducens 
<a href='https://ncbi.nlm.nih.gov/protein/WP_010942012.1'>WP_010942012.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_010942011.1'>WP_010942011.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> LambdaVir & SECphi27
    Zaremba_2022[<a href='https://doi.org/10.1038/s41564-022-01239-0'>Zaremba et al., 2022</a>] --> Origin_2
    Origin_2[ GsSir2/Ago
Geobacter sulfurreducens 
<a href='https://ncbi.nlm.nih.gov/protein/WP_010942012.1'>WP_010942012.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_010942011.1'>WP_010942011.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> LambdaVir & SECphi27
    Zaremba_2022[<a href='https://doi.org/10.1038/s41564-022-01239-0'>Zaremba et al., 2022</a>] --> Origin_3
    Origin_3[ CcSir2/Ago
Caballeronia cordobensis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_053571900.1'>WP_053571900.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_053571899.1'>WP_053571899.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> LambdaVir
    Zaremba_2022[<a href='https://doi.org/10.1038/s41564-022-01239-0'>Zaremba et al., 2022</a>] --> Origin_4
    Origin_4[ PgSir2/Ago
Paraburkholderia graminis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_006053074.1'>WP_006053074.1</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> LambdaVir & SECphi27
    Lisitskaya_2022[<a href='https://doi.org/10.1093/nar/gkad290'>Lisitskaya et al., 2023</a>] --> Origin_5
    Origin_5[ Ago
Exiguobacterium marinum] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> P1vir
    Garb_2022[<a href='https://doi.org/10.1038/s41564-022-01207-8'>Garb et al., 2022</a>] --> Origin_6
    Origin_6[ Sir2/Ago
Geobacter sulfurreducens 
<a href='https://ncbi.nlm.nih.gov/protein/NP_952413'>NP_952413</a>, <a href='https://ncbi.nlm.nih.gov/protein/NP_952414'>NP_952414</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> LambdaVir
    Zeng_2021[<a href='https://doi.org/10.1016/j.chom.2022.04.015'>Zeng et al., 2022</a>] --> Origin_7
    Origin_7[ SiAgo/Aga1/Aga2
Sulfolobus islandicus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_012735993.1'>WP_012735993.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_012718851.1'>WP_012718851.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_012735992.1'>WP_012735992.1</a>] --> Expressed_7[Sulfolobus islandicus]
    Expressed_7[Sulfolobus islandicus] ----> SMV1
    subgraph Title1[Reference]
        Kuzmenko_2020
        Xing_2022
        Zaremba_2022
        Lisitskaya_2022
        Garb_2022
        Zeng_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_2
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
end
    subgraph Title4[Phage infected]
        M13
        P1vir
        T7
        LambdaVir
        SECphi27
        LambdaVir
        SECphi27
        LambdaVir
        LambdaVir
        SECphi27
        P1vir
        LambdaVir
        SMV1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.cell.2022.03.012
    - doi: 10.1016/j.chom.2022.04.015
    - doi: 10.1038/s41564-022-01207-8
    - doi: 10.1038/s41586-020-2605-1
    - doi: 10.1186/1745-6150-4-29
    - doi: 10.1038/s41564-022-01239-0

---
::

