---
title: Bunzi
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
contributors: 
  - Aude Bernheim
relevantAbstracts:
  - doi:10.1016/j.chom.2022.09.017
---

# Bunzi

## Description
The Bunzi system is composed of 2 proteins: BnzB and, BnzA. Bunzi is a serpent water spirit and goddess of rain in traditional Kongo religion. 
There was some homology noted with PFAMS Pfam08000, Pfam05099, Pfam07889 and a TerB domain :ref{doi=10.1016/j.chom.2022.09.017}.

## Molecular Mechanism
As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The Bunzi system is composed of 2 proteins: BnzB and, BnzA.

Here is an example found in the RefSeq database: 

![bunzi](/bunzi/Bunzi.svg){max-width=750px}

Bunzi system in the genome of *Mammaliicoccus lentus* (GCF_014070215.1) is composed of 2 proteins: BnzB (WP_107556517.1)and, BnzA (WP_107556516.1).

## Distribution of the system among prokaryotes

The Bunzi system is present in a total of 86 different species.

Among the 22k complete genomes of RefSeq, this system is present in 286 genomes (1.3 %).

![bunzi](/bunzi/Distribution_Bunzi.svg){max-width=750px}

*Proportion of genome encoding the Bunzi system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Bunzi

::molstar-pdbe-plugin
---
height: 700
dataUrl: /bunzi/Bunzi,Bunzi__BnzA,0,V-plddts_83.13475.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /bunzi/Bunzi,Bunzi__BnzB,0,V-plddts_86.79774.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Ligilactobacillus animalis 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2658999466'>2658999466</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2658999467'>2658999467</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> AR9 & PBS1
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        AR9
        PBS1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>

