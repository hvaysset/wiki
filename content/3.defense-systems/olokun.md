---
title: Olokun
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
---

# Olokun
## Example of genomic structure

The Olokun system is composed of 2 proteins: OloA and, OloB.

Here is an example found in the RefSeq database: 

![olokun](/olokun/Olokun.svg){max-width=750px}

Olokun system in the genome of *Vibrio cyclitrophicus* (GCF_023206035.1) is composed of 2 proteins: OloA (WP_016800143.1)and, OloB (WP_029203700.1).

## Distribution of the system among prokaryotes

The Olokun system is present in a total of 132 different species.

Among the 22k complete genomes of RefSeq, this system is present in 252 genomes (1.1 %).

![olokun](/olokun/Distribution_Olokun.svg){max-width=750px}

*Proportion of genome encoding the Olokun system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Olokun

::molstar-pdbe-plugin
---
height: 700
dataUrl: /olokun/Olokun,Olokun__OloA,0,V-plddts_89.84697.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /olokun/Olokun,Olokun__OloB,0,V-plddts_85.67714.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2610314205'>2610314205</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2610314206'>2610314206</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi27
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        LambdaVir
        SECphi27
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017

---
::
