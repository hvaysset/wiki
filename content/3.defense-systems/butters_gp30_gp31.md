---
title: Butters_gp30_gp31
layout: article
tableColumns:
    article:
      doi: 10.1128/mSystems.00534-20
      abstract: |
        Many sequenced bacterial genomes, including those of pathogenic bacteria, contain prophages. Some prophages encode defense systems that protect their bacterial host against heterotypic viral attack. Understanding the mechanisms undergirding these defense systems is crucial to appreciate the scope of bacterial immunity against viral infections and will be critical for better implementation of phage therapy that would require evasion of these defenses. Furthermore, such knowledge of prophage-encoded defense mechanisms may be useful for developing novel genetic tools for engineering phage-resistant bacteria of industrial importance., A diverse set of prophage-mediated mechanisms protecting bacterial hosts from infection has been recently uncovered within cluster N mycobacteriophages isolated on the host, Mycobacterium smegmatis mc2155. In that context, we unveil a novel defense mechanism in cluster N prophage Butters. By using bioinformatics analyses, phage plating efficiency experiments, microscopy, and immunoprecipitation assays, we show that Butters genes located in the central region of the genome play a key role in the defense against heterotypic viral attack. Our study suggests that a two-component system, articulated by interactions between protein products of genes 30 and 31, confers defense against heterotypic phage infection by PurpleHaze (cluster A/subcluster A3) or Alma (cluster A/subcluster A9) but is insufficient to confer defense against attack by the heterotypic phage Island3 (cluster I/subcluster I1). Therefore, based on heterotypic phage plating efficiencies on the Butters lysogen, additional prophage genes required for defense are implicated and further show specificity of prophage-encoded defense systems., IMPORTANCE Many sequenced bacterial genomes, including those of pathogenic bacteria, contain prophages. Some prophages encode defense systems that protect their bacterial host against heterotypic viral attack. Understanding the mechanisms undergirding these defense systems is crucial to appreciate the scope of bacterial immunity against viral infections and will be critical for better implementation of phage therapy that would require evasion of these defenses. Furthermore, such knowledge of prophage-encoded defense mechanisms may be useful for developing novel genetic tools for engineering phage-resistant bacteria of industrial importance.
---

# Butters_gp30_gp31

## To do 

## Structure

### Butters_gp30_gp31

::molstar-pdbe-plugin
---
height: 700
dataUrl: /butters_gp30_gp31/Butters_gp30_gp31__Butters_gp30-plddts_79.33298.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /butters_gp30_gp31/Butters_gp30_gp31__Butters_gp31-plddts_84.75463.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Mageeney_2020[<a href='https://doi.org/10.1128/mSystems.00534-20'>Mageeney et al., 2020</a>] --> Origin_0
    Origin_0[Mycobacterium phage Butters 
<a href='https://ncbi.nlm.nih.gov/protein/YP_007869841.1'>YP_007869841.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/YP_007869842.1'>YP_007869842.1</a>] --> Expressed_0[Mycobacterium smegmatis]
    Expressed_0[Mycobacterium smegmatis] ----> PurpleHazeandAlma
    subgraph Title1[Reference]
        Mageeney_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        PurpleHazeandAlma
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1128/mSystems.00534-20

---
::
