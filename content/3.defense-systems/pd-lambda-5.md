---
title: PD-Lambda-5
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF02086
---

# PD-Lambda-5
## Example of genomic structure

The PD-Lambda-5 system is composed of 2 proteins: PD-Lambda-5_A and, PD-Lambda-5_B.

Here is an example found in the RefSeq database: 

![pd-lambda-5](/pd-lambda-5/PD-Lambda-5.svg){max-width=750px}

PD-Lambda-5 system in the genome of *Chromobacterium rhizoryzae* (GCF_020544465.1) is composed of 2 proteins: PD-Lambda-5_B (WP_227108065.1)and, PD-Lambda-5_A (WP_227108067.1).

## Distribution of the system among prokaryotes

The PD-Lambda-5 system is present in a total of 210 different species.

Among the 22k complete genomes of RefSeq, this system is present in 361 genomes (1.6 %).

![pd-lambda-5](/pd-lambda-5/Distribution_PD-Lambda-5.svg){max-width=750px}

*Proportion of genome encoding the PD-Lambda-5 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### PD-Lambda-5

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-lambda-5/PD-Lambda-5,PD-Lambda-5__PD-Lambda-5_A,0,V-plddts_83.30421.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-lambda-5/PD-Lambda-5,PD-Lambda-5__PD-Lambda-5_B,0,V-plddts_90.05221.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCQ13837.1'>RCQ13837.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/RCQ13838.1'>RCQ13838.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & LambdaVir & SECphi17 & SECphi18 & SECphi27 & T3 & T7
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
        LambdaVir
        SECphi17
        SECphi18
        SECphi27
        T3
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41564-022-01219-4

---
::

