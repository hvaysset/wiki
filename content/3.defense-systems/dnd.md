---
title: Dnd
layout: article
tableColumns:
    article:
      doi: 10.1038/nchembio.2007.39
      abstract: |
        Modifications of the canonical structures of DNA and RNA play critical roles in cell physiology, DNA replication, transcription and translation in all organisms. We now report that bacterial dnd gene clusters incorporate sulfur into the DNA backbone as a sequence-selective, stereospecific phosphorothioate modification. To our knowledge, unlike any other DNA or RNA modification systems, DNA phosphorothioation by dnd gene clusters is the first physiological modification described on the DNA backbone.
    Sensor: Detecting invading nucleic acid
    Activator: Unknown
    Effector: Nucleic acid degrading
    PFAM: PF00266, PF01507, PF01935, PF08870, PF13476, PF14072
---

# Dnd
## Example of genomic structure

The Dnd system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

![dnd](/dnd/Dnd_ABCDE.svg){max-width=750px}

Dnd_ABCDE subsystem in the genome of *Vibrio tritonius* (GCF_001547935.1) is composed of 6 proteins: DndA (WP_068714508.1), DndB (WP_068714510.1), DndC (WP_068714512.1), DndD (WP_068714514.1), DndE (WP_068714516.1)and, DndD (WP_068714526.1).

![dnd](/dnd/Dnd_ABCDEFGH.svg){max-width=750px}

Dnd_ABCDEFGH subsystem in the genome of *Vibrio sp.* (GCF_023716625.1) is composed of 8 proteins: DptF (WP_252041715.1), DptG (WP_252041716.1), DptH (WP_252041717.1), DndE (WP_252041720.1), DndD (WP_252041722.1), DndC (WP_252041723.1), DndB (WP_252041724.1)and, DndA (WP_252041725.1).

## Distribution of the system among prokaryotes

The Dnd system is present in a total of 218 different species.

Among the 22k complete genomes of RefSeq, this system is present in 388 genomes (1.7 %).

![dnd](/dnd/Distribution_Dnd.svg){max-width=750px}

*Proportion of genome encoding the Dnd system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Dnd_ABCDE

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDE,Dnd__DndA,0,DF-plddts_93.47347.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDE,Dnd__DndB,0,DF-plddts_92.88424.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDE,Dnd__DndC,0,DF-plddts_88.29934.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDE,Dnd__DndD,0,DF-plddts_86.40104.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDE,Dnd__DndE,0,DF-plddts_94.0153.pdb
---
::

### Dnd_ABCDEFGH

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDEFGH,Dnd_ABCDEFGH__DptF,0,DF-plddts_90.5398.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDEFGH,Dnd_ABCDEFGH__DptG,0,DF-plddts_92.29053.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDEFGH,Dnd_ABCDEFGH__DptH,0,DF-plddts_83.00895.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDEFGH,Dnd__DndA,0,DF-plddts_95.7234.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDEFGH,Dnd__DndB,0,DF-plddts_94.63597.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDEFGH,Dnd__DndC,0,DF-plddts_90.06321.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDEFGH,Dnd__DndD,0,DF-plddts_85.70431.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dnd/Dnd_ABCDEFGH,Dnd__DndE,0,DF-plddts_95.24932.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Xiong_2019[<a href='https://doi.org/10.1038/s41467-019-09390-9'>Xiong et al., 2019</a>] --> Origin_0
    Origin_0[ DndCDEA-PbeABCD
Halalkalicoccus jeotgali] --> Expressed_0[Natrinema sp. CJ7-F]
    Expressed_0[Natrinema sp. CJ7-F] ----> SNJ1
    subgraph Title1[Reference]
        Xiong_2019
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        SNJ1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/nchembio.2007.39
    - doi: 10.1038/s41467-019-09390-9

---
::

