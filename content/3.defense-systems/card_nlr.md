---
title: CARD_NLR
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.05.28.542683
      abstract: |
        Caspase recruitment domains (CARDs) and pyrin domains are important facilitators of inflammasome activity and pyroptosis. Upon pathogen recognition by NLR proteins, CARDs recruit and activate caspases, which, in turn, activate gasdermin pore forming proteins to and induce pyroptotic cell death. Here we show that CARD-like domains are present in defense systems that protect bacteria against phage. The bacterial CARD is essential for protease-mediated activation of certain bacterial gasdermins, which promote cell death once phage infection is recognized. We further show that multiple anti-phage defense systems utilize CARD-like domains to activate a variety of cell death effectors. We find that these systems are triggered by a conserved immune evasion protein that phages use to overcome the bacterial defense system RexAB, demonstrating that phage proteins inhibiting one defense system can activate another. We also detect a phage protein with a predicted CARD-like structure that can inhibit the CARD-containing bacterial gasdermin system. Our results suggest that CARD domains represent an ancient component of innate immune systems conserved from bacteria to humans, and that CARD-dependent activation of gasdermins is conserved in organisms across the tree of life.
    PFAM: PF00082, PF00089, PF00614, PF01223, PF13091, PF13191, PF13365
---

# CARD_NLR

## To do 

## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1101/2023.05.28.542683

---
::

