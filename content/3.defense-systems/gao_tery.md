---
title: Gao_TerY
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
---

# Gao_TerY
## Example of genomic structure

The Gao_TerY system is composed of 3 proteins: TerYC, TerYB and, TerYA.

Here is an example found in the RefSeq database: 

![gao_tery](/gao_tery/Gao_TerY.svg){max-width=750px}

Gao_TerY system in the genome of *Burkholderia contaminans* (GCF_018223785.1) is composed of 3 proteins: TerYA (WP_039364687.1), TerYB (WP_039364686.1)and, TerYC (WP_039364684.1).

## Distribution of the system among prokaryotes

The Gao_TerY system is present in a total of 69 different species.

Among the 22k complete genomes of RefSeq, this system is present in 126 genomes (0.6 %).

![gao_tery](/gao_tery/Distribution_Gao_TerY.svg){max-width=750px}

*Proportion of genome encoding the Gao_TerY system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Gao_TerY

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_tery/Gao_TerY,Gao_TerY__TerYA,0,V-plddts_91.43497.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_tery/Gao_TerY,Gao_TerY__TerYB,0,V-plddts_95.75285.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_tery/Gao_TerY,Gao_TerY__TerYC,0,V-plddts_81.328.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Citrobacter gillenii 
<a href='https://ncbi.nlm.nih.gov/protein/WP_115257868.1'>WP_115257868.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_115257869.1'>WP_115257869.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_115257870.1'>WP_115257870.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T3 & T7 & PhiV-1
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T3
        T7
        PhiV-1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aba0372

---
::
