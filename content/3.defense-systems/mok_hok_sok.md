---
title: Mok_Hok_Sok
layout: article
tableColumns:
    article:
      doi: 10.1128/jb.178.7.2044-2050.1996
      abstract: |
        The hok (host killing) and sok (suppressor of killing) genes (hok/sok) efficiently maintain the low-copy-number plasmid R1. To investigate whether the hok/sok locus evolved as a phage-exclusion mechanism, Escherichia coli cells that contain hok/sok on ...
    Sensor: Monitoring of the host cell machinery (?)
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01848
---

# Mok_Hok_Sok
## Example of genomic structure

The Mok_Hok_Sok system is composed of 2 proteins: Mok and, Hok.

Here is an example found in the RefSeq database: 

![mok_hok_sok](/mok_hok_sok/Mok_Hok_Sok.svg){max-width=750px}

Mok_Hok_Sok system in the genome of *Raoultella terrigena* (GCF_015571975.1) is composed of 2 proteins: Hok (WP_227629320.1)and, Mok (WP_227699927.1).

## Distribution of the system among prokaryotes

The Mok_Hok_Sok system is present in a total of 57 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1687 genomes (7.4 %).

![mok_hok_sok](/mok_hok_sok/Distribution_Mok_Hok_Sok.svg){max-width=750px}

*Proportion of genome encoding the Mok_Hok_Sok system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Mok_Hok_Sok

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mok_hok_sok/Mok_Hok_Sok__Hok-plddts_80.98652.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Pecota_1996[<a href='https://doi.org/10.1128/jb.178.7.2044-2050.1996'>Pecota and  Wood, 1996</a>] --> Origin_0
    Origin_0[R1 plasmid of Salmonella paratyphi 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001372321.1'>WP_001372321.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4 & LambdaVir
    subgraph Title1[Reference]
        Pecota_1996
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T4
        LambdaVir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1128/jb.178.7.2044-2050.1996

---
::

