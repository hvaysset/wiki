---
title: Lit
layout: article
tableColumns:
    article:
      doi: 10.1186/1743-422X-7-360
      abstract: |
        Over 50 years of biological research with bacteriophage T4 includes notable discoveries in post-transcriptional control, including the genetic code, mRNA, and tRNA; the very foundations of molecular biology. In this review we compile the past 10 - 15 year literature on RNA-protein interactions with T4 and some of its related phages, with particular focus on advances in mRNA decay and processing, and on translational repression. Binding of T4 proteins RegB, RegA, gp32 and gp43 to their cognate target RNAs has been characterized. For several of these, further study is needed for an atomic-level perspective, where resolved structures of RNA-protein complexes are awaiting investigation. Other features of post-transcriptional control are also summarized. These include: RNA structure at translation initiation regions that either inhibit or promote translation initiation; programmed translational bypassing, where T4 orchestrates ribosome bypass of a 50 nucleotide mRNA sequence; phage exclusion systems that involve T4-mediated activation of a latent endoribonuclease (PrrC) and cofactor-assisted activation of EF-Tu proteolysis (Gol-Lit); and potentially important findings on ADP-ribosylation (by Alt and Mod enzymes) of ribosome-associated proteins that might broadly impact protein synthesis in the infected cell. Many of these problems can continue to be addressed with T4, whereas the growing database of T4-related phage genome sequences provides new resources and potentially new phage-host systems to extend the work into a broader biological, evolutionary context.
    Sensor: Monitoring host integrity
    Activator: Direct
    Effector: Other (Cleaves an elongation factor, inhibiting cellular translation
    PFAM: PF10463
---

# Lit
## Example of genomic structure

The Lit system is composed of one protein: Lit.

Here is an example found in the RefSeq database: 

![lit](/lit/Lit.svg){max-width=750px}

Lit system in the genome of *Stenotrophomonas maltophilia* (GCF_012647025.1) is composed of 1 protein: Lit (WP_061201506.1).

## Distribution of the system among prokaryotes

The Lit system is present in a total of 193 different species.

Among the 22k complete genomes of RefSeq, this system is present in 455 genomes (2.0 %).

![lit](/lit/Distribution_Lit.svg){max-width=750px}

*Proportion of genome encoding the Lit system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Lit

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lit/Lit__Lit-plddts_91.19971.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Yu_1994[<a href='https://doi.org/10.1073/pnas.91.2.802'>Yu and  Snyder, 1994</a>] --> Origin_0
    Origin_0[Escherichia coli defective prophage e14 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001257372.1'>WP_001257372.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4
    subgraph Title1[Reference]
        Yu_1994
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1073/pnas.91.2.802
    - doi: 10.1074/jbc.M002546200
    - doi: 10.1186/1743-422X-7-360

---
::

