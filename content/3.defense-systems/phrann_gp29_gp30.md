---
title: Phrann_gp29_gp30
layout: article
tableColumns:
    article:
      doi: 10.1038/nmicrobiol.2016.251
      abstract: |
        Temperate phages are common, and prophages are abundant residents of sequenced bacterial genomes. Mycobacteriophages are viruses that infect mycobacterial hosts including Mycobacterium tuberculosis and Mycobacterium smegmatis, encompass substantial genetic diversity and are commonly temperate. Characterization of ten Cluster N temperate mycobacteriophages revealed at least five distinct prophage-expressed viral defence systems that interfere with the infection of lytic and temperate phages that are either closely related (homotypic defence) or unrelated (heterotypic defence) to the prophage. Target specificity is unpredictable, ranging from a single target phage to one-third of those tested. The defence systems include a single-subunit restriction system, a heterotypic exclusion system and a predicted (p)ppGpp synthetase, which blocks lytic phage growth, promotes bacterial survival and enables efficient lysogeny. The predicted (p)ppGpp synthetase coded by the Phrann prophage defends against phage Tweety infection, but Tweety codes for a tetrapeptide repeat protein, gp54, which acts as a highly effective counter-defence system. Prophage-mediated viral defence offers an efficient mechanism for bacterial success in host-virus dynamics, and counter-defence promotes phage co-evolution.
    PFAM: PF04607
---

# Phrann_gp29_gp30

## Example of genomic structure

The phrann_gp29_gp30 system is composed of 2 proteins: gp30 and, gp29.

Here is an example found in the RefSeq database: 

![phrann_gp29_gp30](/phrann_gp29_gp30/phrann_gp29_gp30.svg){max-width=750px}

phrann_gp29_gp30 system in the genome of *Mycobacterium tuberculosis* (GCF_002448055.1) is composed of 2 proteins: gp29 (WP_003407164.1)and, gp30 (WP_003407167.1).

## Distribution of the system among prokaryotes

The phrann_gp29_gp30 system is present in a total of 35 different species.

Among the 22k complete genomes of RefSeq, this system is present in 314 genomes (1.4 %).

![phrann_gp29_gp30](/phrann_gp29_gp30/Distribution_phrann_gp29_gp30.svg){max-width=750px}

*Proportion of genome encoding the phrann_gp29_gp30 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Experimental validation
<mermaid>
graph LR;
    Dedrick_2017[<a href='https://doi.org/10.1038/nmicrobiol.2016.251'>Dedrick et al., 2017</a>] --> Origin_0
    Origin_0[Mycobacterium phage Phrann 
<a href='https://ncbi.nlm.nih.gov/protein/YP_009304221.1'>YP_009304221.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/YP_009304222.1'>YP_009304222.1</a>] --> Expressed_0[Mycobacterium smegmatis mc2 155]
    Expressed_0[Mycobacterium smegmatis mc2 155] ----> Tweety
    subgraph Title1[Reference]
        Dedrick_2017
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Tweety
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/nmicrobiol.2016.251

---
::

