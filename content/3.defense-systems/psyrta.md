---
title: PsyrTA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00270, PF00271, PF02481, PF04851, PF18306
---

# PsyrTA
## Example of genomic structure

The PsyrTA system is composed of 2 proteins: PsyrT and, PsyrA.

Here is an example found in the RefSeq database: 

![psyrta](/psyrta/PsyrTA.svg){max-width=750px}

PsyrTA system in the genome of *Photorhabdus laumondii* (GCF_003343225.1) is composed of 2 proteins: PsyrT (WP_109791883.1)and, PsyrA (WP_113049635.1).

## Distribution of the system among prokaryotes

The PsyrTA system is present in a total of 281 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1435 genomes (6.3 %).

![psyrta](/psyrta/Distribution_PsyrTA.svg){max-width=750px}

*Proportion of genome encoding the PsyrTA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### PsyrTA

::molstar-pdbe-plugin
---
height: 700
dataUrl: /psyrta/PsyrTA,PsyrTA__PsyrA,0,V-plddts_88.4921.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /psyrta/PsyrTA,PsyrTA__PsyrT,0,V-plddts_88.00999.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus sp. FJAT-29814 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2746452868'>2746452868</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2746452867'>2746452867</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1016/j.molcel.2013.02.002

---
::

