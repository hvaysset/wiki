---
title: SpbK
layout: article
tableColumns:
    article:
      doi: 10.1371/journal.pgen.1010065
      abstract: |
        Most bacterial genomes contain horizontally acquired and transmissible mobile genetic elements, including temperate bacteriophages and integrative and conjugative elements. Little is known about how these elements interact and co-evolved as parts of their host genomes. In many cases, it is not known what advantages, if any, these elements provide to their bacterial hosts. Most strains of Bacillus subtilis contain the temperate phage SPÃŸ and the integrative and conjugative element ICEBs1. Here we show that the presence of ICEBs1 in cells protects populations of B. subtilis from predation by SPÃŸ, likely providing selective pressure for the maintenance of ICEBs1 in B. subtilis. A single gene in ICEBs1 (yddK, now called spbK for SPÃŸ killing) was both necessary and sufficient for this protection. spbK inhibited production of SPÃŸ, during both activation of a lysogen and following de novo infection. We found that expression spbK, together with the SPÃŸ gene yonE constitutes an abortive infection system that leads to cell death. spbK encodes a TIR (Toll-interleukin-1 receptor)-domain protein with similarity to some plant antiviral proteins and animal innate immune signaling proteins. We postulate that many uncharacterized cargo genes in ICEs may confer selective advantage to cells by protecting against other mobile elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13676
---

# SpbK
## Example of genomic structure

The SpbK system is composed of one protein: SpbK.

Here is an example found in the RefSeq database: 

![spbk](/spbk/SpbK.svg){max-width=750px}

SpbK system in the genome of *Clostridium beijerinckii* (GCF_900010805.1) is composed of 1 protein: SpbK (WP_077841417.1).

## Distribution of the system among prokaryotes

The SpbK system is present in a total of 88 different species.

Among the 22k complete genomes of RefSeq, this system is present in 219 genomes (1.0 %).

![spbk](/spbk/Distribution_SpbK.svg){max-width=750px}

*Proportion of genome encoding the SpbK system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### SpbK

::molstar-pdbe-plugin
---
height: 700
dataUrl: /spbk/SpbK__SpbK-plddts_78.5012.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Johnson_2022[<a href='https://doi.org/10.1371/journal.pgen.1010065'>Johnson et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus subtilis 
<a href='https://ncbi.nlm.nih.gov/protein/NP_388381.1'>NP_388381.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SPbeta
    subgraph Title1[Reference]
        Johnson_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        SPbeta
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1371/journal.pgen.1010065

---
::

