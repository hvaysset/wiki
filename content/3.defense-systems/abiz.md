---
title: AbiZ
layout: article
tableColumns:
    article:
      doi: 10.1128/JB.00904-06
      abstract: |
        The conjugative plasmid pTR2030 has been used extensively to confer phage resistance in commercial Lactococcus starter cultures. The plasmid harbors a 16-kb region, flanked by insertion sequence (IS) elements, that encodes the restriction/modification system LlaI and carries an abortive infection gene, abiA. The AbiA system inhibits both prolate and small isometric phages by interfering with the early stages of phage DNA replication. However, abiA alone does not account for the full abortive activity reported for pTR2030. In this study, a 7.5-kb region positioned within the IS elements and downstream of abiA was sequenced to reveal seven additional open reading frames (ORFs). A single ORF, designated abiZ, was found to be responsible for a significant reduction in plaque size and an efficiency of plaquing (EOP) of 10?6, without affecting phage adsorption. AbiZ causes phage ?31-infected Lactococcus lactis NCK203 to lyse 15 min early, reducing the burst size of ?31 100-fold. Thirteen of 14 phages of the P335 group were sensitive to AbiZ, through reduction in either plaque size, EOP, or both. The predicted AbiZ protein contains two predicted transmembrane helices but shows no significant DNA homologies. When the phage ?31 lysin and holin genes were cloned into the nisin-inducible shuttle vector pMSP3545, nisin induction of holin and lysin caused partial lysis of NCK203. In the presence of AbiZ, lysis occurred 30 min earlier. In holin-induced cells, membrane permeability as measured using propidium iodide was greater in the presence of AbiZ. These results suggest that AbiZ may interact cooperatively with holin to cause premature lysis.
    Sensor: Unknown
    Activator: Unknown
    Effector: Membrane disrupting
---

# AbiZ
## Example of genomic structure

The AbiZ system is composed of one protein: AbiZ.

Here is an example found in the RefSeq database: 

![abiz](/abiz/AbiZ.svg){max-width=750px}

AbiZ system in the genome of *Streptococcus oralis* (GCF_019334565.1) is composed of 1 protein: AbiZ (WP_215804505.1).

## Distribution of the system among prokaryotes

The AbiZ system is present in a total of 191 different species.

Among the 22k complete genomes of RefSeq, this system is present in 831 genomes (3.6 %).

![abiz](/abiz/Distribution_AbiZ.svg){max-width=750px}

*Proportion of genome encoding the AbiZ system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiZ

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abiz/AbiZ__AbiZ-plddts_78.85683.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Durmaz_2007[<a href='https://doi.org/10.1128/JB.00904-06'>Durmaz and  Klaenhammer, 2007</a>] --> Origin_0
    Origin_0[Lactococcus lactis 
<a href='https://ncbi.nlm.nih.gov/protein/ABI93964.1'>ABI93964.1</a>] --> Expressed_0[Lactococcus lactis]
    Expressed_0[Lactococcus lactis] ----> Phi31.2 & ul36 & phi31 & phi48 & phi31.1 & Q30 & Q36 & Q33 & phi50 & phi48
    subgraph Title1[Reference]
        Durmaz_2007
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Phi31.2
        ul36
        phi31
        phi48
        phi31.1
        Q30
        Q36
        Q33
        phi50
        phi48
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1128/JB.00904-06

---
::
