---
title: PD-Lambda-3
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF09509
---

# PD-Lambda-3
## Example of genomic structure

The PD-Lambda-3 system is composed of 2 proteins: PD-Lambda-3_B and, PD-Lambda-3_A.

Here is an example found in the RefSeq database: 

![pd-lambda-3](/pd-lambda-3/PD-Lambda-3.svg){max-width=750px}

PD-Lambda-3 system in the genome of *Serratia rubidaea* (GCF_900478395.1) is composed of 2 proteins: PD-Lambda-3_A (WP_227673955.1)and, PD-Lambda-3_B (WP_047063039.1).

## Distribution of the system among prokaryotes

The PD-Lambda-3 system is present in a total of 24 different species.

Among the 22k complete genomes of RefSeq, this system is present in 74 genomes (0.3 %).

![pd-lambda-3](/pd-lambda-3/Distribution_PD-Lambda-3.svg){max-width=750px}

*Proportion of genome encoding the PD-Lambda-3 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### PD-Lambda-3

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-lambda-3/PD-Lambda-3,PD-Lambda-3__PD-Lambda-3_A,0,V-plddts_93.15726.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-lambda-3/PD-Lambda-3,PD-Lambda-3__PD-Lambda-3_B,0,V-plddts_92.44549.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-lambda-3/PD-Lambda-3,PD-Lambda-3__PD-Lambda-3_C,0,V-plddts_92.92218.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCP74640.1'>RCP74640.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/RCP74641.1'>RCP74641.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/RCP74642.1'>RCP74642.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        LambdaVir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41564-022-01219-4

---
::

