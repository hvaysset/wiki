---
title: AbiR
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00176, PF00271, PF04545, PF04851, PF13091
---

# AbiR
## Example of genomic structure

The AbiR system is composed of 3 proteins: AbiRc, AbiRb and, AbiRa.

Here is an example found in the RefSeq database: 

![abir](/abir/AbiR.svg){max-width=750px}

AbiR system in the genome of *Pediococcus pentosaceus* (GCF_019614475.1) is composed of 3 proteins: AbiRa (WP_220689027.1), AbiRb (WP_011673124.1)and, AbiRc (WP_011673125.1).

## Distribution of the system among prokaryotes

The AbiR system is present in a total of 31 different species.

Among the 22k complete genomes of RefSeq, this system is present in 56 genomes (0.2 %).

![abir](/abir/Distribution_AbiR.svg){max-width=750px}

*Proportion of genome encoding the AbiR system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiR

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abir/AbiR,AbiR__AbiRa,0,V-plddts_88.45598.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abir/AbiR,AbiR__AbiRb,0,V-plddts_92.82829.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abir/AbiR,AbiR__AbiRc,0,V-plddts_86.60767.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAF75619.1'>AAF75619.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AAF75618.1'>AAF75618.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/AAF75617.1'>AAF75617.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> c2
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        c2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006

---
::

