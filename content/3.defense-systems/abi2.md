---
title: Abi2
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: ''
    Activator: ''
    Effector: ''
    PFAM: PF07751
relevantAbstracts:
  - doi: 10.1016/j.mib.2005.06.006
---

# Abi2

The Abi2 system is composed of one protein: Abi_2.

Here is an example found in the RefSeq database:


![abi2](/abi2/Abi2.svg)

Abi2 system in the genome of *Clostridium butyricum* (GCF_014131795.1) is composed of 1 protein: Abi_2 (WP_035763709.1).

## Distribution of the system among prokaryotes

The Abi2 system is present in a total of 176 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1210 genomes (5.3 %).

![abi2](/abi2/Distribution_Abi2.svg)

*Proportion of genome encoding the Abi2 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

