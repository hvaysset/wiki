---
title: CBASS
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-020-0777-y
      abstract: |
        Cyclic-oligonucleotide-based anti-phage signalling systems (CBASS) are a family of defence systems against bacteriophages (hereafter phages) that share ancestry with the cGAS-STING innate immune pathway in animals. CBASS systems are composed of an oligonucleotide cyclase, which generates signalling cyclic oligonucleotides in response to phage infection, and an effector that is activated by the cyclic oligonucleotides and promotes cell death. Cell death occurs before phage replication is completed, therefore preventing the spread of phages to nearby cells. Here, we analysed 38,000 bacterial and archaeal genomes and identified more than 5,000 CBASS systems, which have diverse architectures with multiple signalling molecules, effectors and ancillary genes. We propose a classification system for CBASS that groups systems according to their operon organization, signalling molecules and effector function. Four major CBASS types were identified, sharing at least six effector subtypes that promote cell death by membrane impairment, DNA degradation or other means. We observed evidence of extensive gain and loss of CBASS systems, as well as shuffling of effector genes between systems. We expect that our classification and nomenclature scheme will guide future research in the developing CBASS field.
    Sensor: Unknown
    Activator: Signaling molecules
    Effector: Divers (Nucleic acid degrading, Nucleotide modifying, Membrane disrupting)
    PFAM: PF00004, PF00027, PF00899, PF01048, PF01734, PF06508, PF10137, PF14461, PF14464, PF18134, PF18138, PF18144, PF18145, PF18153, PF18159, PF18167, PF18173, PF18178, PF18179, PF18186, PF18303, PF18967
---

# CBASS
## Example of genomic structure

The CBASS system have been describe in a total of 5 subsystems.

Here is some example found in the RefSeq database:

![cbass](/cbass/CBASS_I.svg){max-width=750px}

CBASS_I subsystem in the genome of *Rhizobium leguminosarum* (GCF_002243365.1) is composed of 2 proteins: 4TM_new (WP_094230678.1)and, Cyclase_SMODS (WP_094230679.1).

![cbass](/cbass/CBASS_II.svg){max-width=750px}

CBASS_II subsystem in the genome of *Parvularcula bermudensis* (GCF_000152825.2) is composed of 3 proteins: 4TM_new (WP_013299178.1), Cyclase_II (WP_148235131.1)and, AG_E2_Prok-E2_B (WP_013299180.1).

![cbass](/cbass/CBASS_III.svg){max-width=750px}

CBASS_III subsystem in the genome of *Methylocella tundrae* (GCF_900749825.1) is composed of 5 proteins: Endonuc_small (WP_134490779.1), Cyclase_SMODS (WP_134490781.1), bacHORMA_2 (WP_134490783.1), HORMA (WP_134490785.1)and, TRIP13 (WP_134490787.1).

![cbass](/cbass/CBASS_IV.svg){max-width=750px}

CBASS_IV subsystem in the genome of *Bacillus sp.* (GCF_022809835.1) is composed of 4 proteins: 2TM_type_IV (WP_243501124.1), QueC (WP_206906219.1), TGT (WP_243501126.1)and, Cyclase_SMODS (WP_243501127.1).

## Distribution of the system among prokaryotes

The CBASS system is present in a total of 1062 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2938 genomes (12.9 %).

![cbass](/cbass/Distribution_CBASS.svg){max-width=750px}

*Proportion of genome encoding the CBASS system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### CBASS_I

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_I,CBASS__Cyclase_II,0,V-plddts_92.48144.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_I,CBASS__TM,0,V-plddts_91.45177.pdb
---
::

### CBASS_II

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_II,CBASS__AG_E2_Prok-E2,0,V-plddts_89.8018.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_II,CBASS__Cyclase_II,0,V-plddts_89.65358.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_II,CBASS__Jab,0,V-plddts_95.53434.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_II,CBASS__Phospholipase,0,V-plddts_89.1622.pdb
---
::

### CBASS_III

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_III,CBASS__Cyclase_II,0,V-plddts_97.70389.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_III,CBASS__Endonuc_small,0,V-plddts_97.03481.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_III,CBASS__HORMA,0,V-plddts_89.50696.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_III,CBASS__TRIP13,0,V-plddts_89.6492.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_III,CBASS__bacHORMA_2,0,V-plddts_84.43633.pdb
---
::

### CBASS_IV

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_IV,CBASS__2TM_type_IV,0,DF-plddts_87.89742.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_IV,CBASS__Cyclase_SMODS,0,DF-plddts_87.98201.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_IV,CBASS__OGG,0,DF-plddts_95.21551.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_IV,CBASS__QueC,0,DF-plddts_93.98141.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /cbass/CBASS_IV,CBASS__TGT,0,DF-plddts_93.84001.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Cohen_2019[<a href='https://doi.org/10.1038/s41586-019-1605-5'>Cohen et al., 2019</a>] --> Origin_0
    Origin_0[Vibrio cholerae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001901330.1'>WP_001901330.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001133548.1'>WP_001133548.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_001884104.1'>WP_001884104.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_223225479.1'>WP_223225479.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1 & T2
    Cohen_2019[<a href='https://doi.org/10.1038/s41586-019-1605-5'>Cohen et al., 2019</a>] --> Origin_1
    Origin_1[Escherichia coli] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> P1 & T2 & T4 & T5 & T6 & LambdaVir
    Lowey_2020[<a href='https://doi.org/10.1016/j.cell.2020.05.019'>Lowey et al., 2020</a>] --> Origin_2
    Origin_2[Enterobacter cloacae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_032676399.1'>WP_032676399.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_032676400.1'>WP_032676400.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_050010101.1'>WP_050010101.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_157903655.1'>WP_157903655.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T7
    Huiting_2022[<a href='https://doi.org/10.1016/j.cell.2022.12.041'>Huiting et al., 2023</a>] --> Origin_3
    Origin_3[Pseudomonas aeruginosa] --> Expressed_3[Pseudomonas aeruginosa]
    Expressed_3[Pseudomonas aeruginosa] ----> PaMx41 & PaMx33 & PaMx35 & PaMx43
    subgraph Title1[Reference]
        Cohen_2019
        Lowey_2020
        Huiting_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Phage infected]
        P1
        T2
        P1
        T2
        T4
        T5
        T6
        LambdaVir
        T2
        T7
        PaMx41
        PaMx33
        PaMx35
        PaMx43
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.molcel.2019.12.009
    - doi: 10.1016/j.molcel.2021.10.020
    - doi: 10.1038/s41564-020-0777-y
    - doi: 10.1038/s41586-019-1605-5
    - doi: 10.1038/s41586-020-2719-5

---
::

