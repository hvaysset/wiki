---
title: Eleos
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00350, PF01926, PF18709
---

# Eleos
The Eleos system was previously described as the Dynamins-like system in (Millman et al, 2022).

## Example of genomic structure
The Eleos system is composed of 2 proteins: LeoA and, LeoBC. Sometimes, the systems is in three genes: LeoA, LeoB and LeoC.

Here is an example found in the RefSeq database: 

![eleos](/eleos/Eleos.svg){max-width=750px}

Dynamins system in the genome of *Pseudomonas aeruginosa* (GCF_002223805.1) is composed of 2 proteins: LeoBC (WP_024947442.1)and, LeoA (WP_024947443.1).

## Distribution of the system among prokaryotes

The Eleos system is present in a total of 807 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2652 genomes (11.6 %).

![eleos](/eleos/Distribution_Eleos.svg){max-width=750px}

*Proportion of genome encoding the Eleos system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Eleos

::molstar-pdbe-plugin
---
height: 700
dataUrl: /eleos/Eleos,Eleos__LeoA,0,V-plddts_81.66314.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /eleos/Eleos,Eleos__LeoBC,0,V-plddts_83.32056.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus vietnamensis 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2732656981'>2732656981</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2732656980'>2732656980</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> AR9 & PBS1
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        AR9
        PBS1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017

---
::

