---
title: Lamassu-Fam
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Diverse (Nucleic acid degrading (?), Nucleotide modifying (?), Membrane disrupting (?))
    PFAM: PF00753, PF02463, PF05057, PF12532, PF13175, PF13289, PF13476, PF14130
---

# Lamassu-Fam
## Description

The original types of Lamassu systems are Lamassu Type 1 and 2. They both necessarily comprise two genes *lmuA* and *lmuB*, to which a third gene (*lmuC*) is added in the case of Lamassu Type 2.  

More recently, Lamassu has been suggested to be a large family of defense systems, that can be classified into multiple subtypes. 

These systems all encode the *lmuB* gene, and in most cases also comprise *lmuC.* In addition to these two core genes, Lamassu systems of various subtypes encode a third protein, hypothesized to be the Abi effector protein (3). This effector  can not only be LmuA (Lamassu Type1 and 2) but also proteins encoding endonuclease domains, SIR2-domains, or even hydrolase domains (3). Systems of the extended Lamassu-family can be found in 10% of prokaryotic genomes (3).

## Molecular mechanism

Lamassu systems function through abortive infection (Abi), but their molecular mechanism remains to be described.

## Example of genomic structure

The majority of the Lamassu-Fam systems are composed of 3 proteins: LmuA, LmuB and, an accessory LmuC proteins.

Here is an example of a Lamassu-Fam_Cap4_nuclease found in the RefSeq database: 

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Cap4_nuclease.svg){max-width=750px}

Lamassu-Fam_Cap4_nuclease subsystem in the genome of *Pseudomonas sp.* (GCF_016925675.1) is composed of 3 proteins: LmuB_SMC_Hydrolase_protease (WP_205519025.1), LmuC_acc_Cap4_nuclease (WP_205478326.1)and, LmuA_effector_Cap4_nuclease_II (WP_205478325.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Mrr.svg){max-width=750px}

Lamassu-Fam_Mrr subsystem in the genome of *Escherichia coli* (GCF_011404895.1) is composed of 2 proteins: LmuA_effector_Mrr (WP_044864610.1)and, LmuB_SMC_Cap4_nuclease_II (WP_226199836.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Hydrolase.svg){max-width=750px}

Lamassu-Fam_Hydrolase subsystem in the genome of *Caldisphaera lagunensis* (GCF_000317795.1) is composed of 2 proteins: LmuA_effector_Hydrolase (WP_015232255.1)and, LmuB_SMC_Hydrolase_protease (WP_015232260.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Lipase.svg){max-width=750px}

Lamassu-Fam_Lipase subsystem in the genome of *Bradyrhizobium elkanii* (GCF_012871055.1) is composed of 2 proteins: LmuA_effector_Lipase (WP_172647146.1)and, LmuB_SMC_Lipase (WP_172647148.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Hydrolase_protease.svg){max-width=750px}

Lamassu-Fam_Hydrolase_protease subsystem in the genome of *Klebsiella pneumoniae* (GCF_022453565.1) is composed of 3 proteins: LmuB_SMC_Cap4_nuclease_II (WP_023301569.1), LmuA_effector_Protease (WP_023301563.1)and, LmuA_effector_Hydrolase (WP_023301562.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Hypothetical.svg){max-width=750px}

Lamassu-Fam_Hypothetical subsystem in the genome of *Streptococcus constellatus* (GCF_016127875.1) is composed of 2 proteins: LmuB_SMC_Cap4_nuclease_II (WP_198458038.1)and, LmuA_effector_hypothetical (WP_198458040.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Protease.svg){max-width=750px}

Lamassu-Fam_Protease subsystem in the genome of *Azospirillum brasilense* (GCF_022023855.1) is composed of 2 proteins: LmuA_effector_Protease (WP_237905456.1)and, LmuB_SMC_Cap4_nuclease_II (WP_237905457.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_PDDEXK.svg){max-width=750px}

Lamassu-Fam_PDDEXK subsystem in the genome of *Janthinobacterium sp.* (GCF_000013625.1) is composed of 2 proteins: LmuA_effector_PDDEXK (WP_012078862.1)and, LmuB_SMC_Cap4_nuclease_II (WP_012078864.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Sir2.svg){max-width=750px}

Lamassu-Fam_Sir2 subsystem in the genome of *Paenibacillus polymyxa* (GCF_022492955.1) is composed of 4 proteins: LmuB_SMC_Cap4_nuclease_II (WP_240753063.1), LmuB_SMC_Sir2 (WP_240753064.1), LmuC_acc_Sir2 (WP_240753066.1)and, LmuA_effector_Sir2 (WP_240753072.1).


![lamassu-fam](/lamassu-fam/Lamassu-Fam_FMO.svg){max-width=750px}

Lamassu-Fam_FMO subsystem in the genome of *Acinetobacter johnsonii* (GCF_021496365.1) is composed of 2 proteins: LmuA_effector_FMO (WP_234965678.1)and, LmuB_SMC_FMO (WP_234965680.1).

![lamassu-fam](/lamassu-fam/Lamassu-Fam_Amidase.svg){max-width=750px}

Lamassu-Fam_Amidase subsystem in the genome of *Bradyrhizobium arachidis* (GCF_015291705.1) is composed of 2 proteins: LmuA_effector_Amidase (WP_143130692.1)and, LmuB_SMC_Amidase (WP_092217687.1).

## Distribution of the system among prokaryotes

The Lamassu-Fam system is present in a total of 1189 different species.

Among the 22k complete genomes of RefSeq, this system is present in 3939 genomes (17.3 %).

![lamassu-fam](/lamassu-fam/Distribution_Lamassu-Fam.svg){max-width=750}

*Proportion of genome encoding the Lamassu-Fam system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Lamassu-Amidase

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Amidase,Lamassu-Fam__LmuA_effector_Amidase,0,DF-plddts_93.15132.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Amidase,Lamassu-Fam__LmuB_SMC_Amidase,0,DF-plddts_83.58631.pdb
---
::

### Lamassu-Cap4_nuclease

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Cap4_nuclease,Lamassu-Fam__LmuA_effector_Cap4_nuclease_II,0,DF-plddts_93.57957.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Cap4_nuclease,Lamassu-Fam__LmuB_SMC_Cap4_nuclease_II,0,DF-plddts_90.05853.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Cap4_nuclease,Lamassu-Fam__LmuC_acc_Cap4_nuclease,0,DF-plddts_91.47415.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Cap4_nuclease,Lamassu-Fam__LmuA_effector_Cap4_nuclease_II,0,DF-plddts_93.57957.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Cap4_nuclease,Lamassu-Fam__LmuB_SMC_Cap4_nuclease_II,0,DF-plddts_90.05853.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Cap4_nuclease,Lamassu-Fam__LmuC_acc_Cap4_nuclease,0,DF-plddts_91.47415.pdb
---
::

### Lamassu-FMO

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-FMO,Lamassu-Fam__LmuA_effector_FMO,0,DF-plddts_90.83272.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-FMO,Lamassu-Fam__LmuB_SMC_FMO,0,DF-plddts_83.19523.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-FMO,Lamassu-Fam__LmuC_acc_FMO,0,DF-plddts_89.32543.pdb
---
::

### Lamassu-Fam_Hydrolase_Protease

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_Hydrolase_Protease,Lamassu-Fam__LmuA_effector_Hydrolase,0,V-plddts_93.45211.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_Hydrolase_Protease,Lamassu-Fam__LmuA_effector_Protease,0,V-plddts_90.19112.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_Hydrolase_Protease,Lamassu-Fam__LmuB_SMC_Hydrolase_protease,0,V-plddts_80.40324.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_Hydrolase_Protease,Lamassu-Fam__LmuC_acc_hydrolase_protease,0,V-plddts_89.02396.pdb
---
::

### Lamassu-Fam_Mrr

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_Mrr,Lamassu-Fam__LmuA_effector_Mrr,0,V-plddts_84.76528.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_Mrr,Lamassu-Fam__LmuB_SMC_Mrr,0,V-plddts_85.79974.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_Mrr,Lamassu-Fam__LmuC_acc_Mrr,0,V-plddts_86.75101.pdb
---
::

### Lamassu-Fam_PDDEXK

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_PDDEXK,Lamassu-Fam__LmuA_effector_PDDEXK,0,V-plddts_90.62277.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_PDDEXK,Lamassu-Fam__LmuB_SMC_PDDEXK,0,V-plddts_83.57058.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Fam_PDDEXK,Lamassu-Fam__LmuC_acc_PDDEXK,0,V-plddts_93.07571.pdb
---
::

### Lamassu-Hypothetical

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Hypothetical,Lamassu-Fam__LmuA_effector_hypothetical,0,DF-plddts_87.29909.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Hypothetical,Lamassu-Fam__LmuB_SMC_hypothetical,0,DF-plddts_90.35013.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Hypothetical,Lamassu-Fam__LmuC_acc_hypothetical,0,DF-plddts_90.49821.pdb
---
::

### Lamassu-Lipase

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Lipase,Lamassu-Fam__LmuA_effector_Lipase,0,DF-plddts_86.47244.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Lipase,Lamassu-Fam__LmuB_SMC_Lipase,0,DF-plddts_89.07634.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Lipase,Lamassu-Fam__LmuC_acc_Lipase,0,DF-plddts_86.70507.pdb
---
::

### Lamassu-Sir2

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Sir2,Lamassu-Fam__LmuA_effector_Sir2,0,DF-plddts_89.3866.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Sir2,Lamassu-Fam__LmuB_SMC_Sir2,0,DF-plddts_86.39095.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /lamassu-fam/Lamassu-Sir2,Lamassu-Fam__LmuC_acc_Sir2,0,DF-plddts_85.94734.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus sp. NIO-1130 
<a href='https://ncbi.nlm.nih.gov/protein/SCC38433.1'>SCC38433.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/SCC38445.1'>SCC38445.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> phi3T & SpBeta & SPR
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Origin_1[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/EJR12435.1'>EJR12435.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJR12434.1'>EJR12434.1</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> SpBeta
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_2
    Origin_2[ LmuB+LmuC+Hydrolase Protease
Bacillus cereus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999254'>2624999254</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999255'>2624999255</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999257'>2624999257</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999256'>2624999256</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T4
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_2
    Origin_2[ LmuB+LmuC+Hydrolase Protease
Bacillus cereus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999254'>2624999254</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999255'>2624999255</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999257'>2624999257</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2624999256'>2624999256</a>] --> Expressed_3[Bacillus subtilis]
    Expressed_3[Bacillus subtilis] ----> SpBeta & phi105 & Rho14 & SPP1 & phi29
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_3
    Origin_3[ LmuB+LmuC+Mrr endonuclease
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=646870492'>646870492</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=646870494'>646870494</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=646870493'>646870493</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> LambdaVir & SECphi27
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_4
    Origin_4[ LmuB+LmuC+PDDEXK nuclease
Bacillus cereus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2729045962'>2729045962</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2729045961'>2729045961</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2729045960'>2729045960</a>] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> LambdaVir
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_5
    Origin_5[ LmuB+LmuC+PDDEXK nuclease
Bacillus sp. UNCCL81 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2595091739'>2595091739</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2595091740'>2595091740</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2595091741'>2595091741</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> LambdaVir
    Payne_2021[<a href='https://doi.org/10.1093/nar/gkab883'>Payne et al., 2021</a>] --> Origin_6
    Origin_6[ LmuA+LmuC+LmuB 
Janthinobacterium agaricidamnosum 
<a href='https://ncbi.nlm.nih.gov/protein/WP_038488270.1'>WP_038488270.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_038488273.1'>WP_038488273.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_156484105.1'>WP_156484105.1</a>] --> Expressed_7[Escherichia coli]
    Expressed_7[Escherichia coli] ----> T1 & T3 & T7 & LambdaVir & PVP-SE1
    Jaskolska_2022[<a href='https://doi.org/10.1038/s41586-022-04546-y'>Jaskólska et al., 2022</a>] --> Origin_7
    Origin_7[ DdmABC
Vibrio cholerae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000466016.1'>WP_000466016.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000917213.1'>WP_000917213.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_000654528.1'>WP_000654528.1</a>] --> Expressed_8[Escherichia coli]
    Expressed_8[Escherichia coli] ----> P1 & Lambda
    subgraph Title1[Reference]
        Doron_2018
        Millman_2022
        Payne_2021
        Jaskolska_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_2
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
        Expressed_8
end
    subgraph Title4[Phage infected]
        phi3T
        SpBeta
        SPR
        SpBeta
        T4
        SpBeta
        phi105
        Rho14
        SPP1
        phi29
        LambdaVir
        SECphi27
        LambdaVir
        LambdaVir
        T1
        T3
        T7
        LambdaVir
        PVP-SE1
        P1
        Lambda
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1038/s41586-022-04546-y
    - doi: 10.1126/science.aar4120

---
::


## References

1. Doron S, Melamed S, Ofir G, et al. Systematic discovery of antiphage defense systems in the microbial pangenome. *Science*. 2018;359(6379):eaar4120. doi:10.1126/science.aar4120

2. Payne LJ, Todeschini TC, Wu Y, et al. Identification and classification of antiviral defence systems in bacteria and archaea with PADLOC reveals new system types. *Nucleic Acids Res*. 2021;49(19):10868-10878. doi:10.1093/nar/gkab883

3. Millman, A., Melamed, S., Leavitt, A., Doron, S., Bernheim, A., HÃ¶r, J., Lopatina, A., Ofir, G., Hochhauser, D., Stokar-Avihail, A., Tal, N., Sharir, S., Voichek, M., Erez, Z., Ferrer, J.L.M., Dar, D., Kacen, A., Amitai, G., Sorek, R., 2022. An expanding arsenal of immune systems that protect bacteria from phages. bioRxiv. https://doi.org/10.1101/2022.05.11.491447

