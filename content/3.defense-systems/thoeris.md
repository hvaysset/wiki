---
title: Thoeris
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Signaling
    Effector: Nucleotide modifying
    PFAM: PF08937, PF13289, PF18185
---

# Thoeris
## Example of genomic structure

The Thoeris system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

![thoeris](/thoeris/Thoeris_I.svg){max-width=750px}

Thoeris_I subsystem in the genome of *Bacillus thuringiensis* (GCF_020809205.1) is composed of 2 proteins: ThsA_new_grand (WP_021728720.1)and, ThsB_Global (WP_021728719.1).

![thoeris](/thoeris/Thoeris_II.svg){max-width=750px}

Thoeris_II subsystem in the genome of *Acinetobacter baumannii* (GCF_014672775.1) is composed of 2 proteins: ThsB_Global (WP_000120680.1)and, ThsA_new_petit (WP_005134880.1).

## Distribution of the system among prokaryotes

The Thoeris system is present in a total of 286 different species.

Among the 22k complete genomes of RefSeq, this system is present in 812 genomes (3.6 %).

![thoeris](/thoeris/Distribution_Thoeris.svg){max-width=750px}

*Proportion of genome encoding the Thoeris system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Thoeris_I

::molstar-pdbe-plugin
---
height: 700
dataUrl: /thoeris/Thoeris_I,Thoeris_I__ThsA_new_grand,0,V-plddts_93.92302.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /thoeris/Thoeris_I,Thoeris__ThsB_Global,0,V-plddts_90.12702.pdb
---
::

### Thoeris_II

::molstar-pdbe-plugin
---
height: 700
dataUrl: /thoeris/Thoeris_II,Thoeris_II__ThsA_new_petit,0,V-plddts_88.7532.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /thoeris/Thoeris_II,Thoeris__ThsB_Global,0,V-plddts_93.32159.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Bacillus amyloliquefaciens 
<a href='https://ncbi.nlm.nih.gov/protein/AFJ62118.1'>AFJ62118.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AFJ62119.1'>AFJ62119.1</a>] --> Expressed_0[Bacillus subtilis]
    Expressed_0[Bacillus subtilis] ----> SPO1 & SBSphiJ & SBSphiC
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Ofir_2021[<a href='https://doi.org/10.1038/s41586-021-04098-7'>Ofir et al., 2021</a>] --> Origin_1
    Origin_1[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/EJR09241.1'>EJR09241.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/EJR09240.1'>EJR09240.1</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> phi29 & SBSphiC & SPO1 & SBSphiJ
    Ofir_2021[<a href='https://doi.org/10.1038/s41586-021-04098-7'>Ofir et al., 2021</a>] --> Origin_2
    Origin_2[Bacillus dafuensis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_057775117.1'>WP_057775117.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_057775115.1'>WP_057775115.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_057775113.1'>WP_057775113.1</a>] --> Expressed_2[Bacillus subtilis]
    Expressed_2[Bacillus subtilis] ----> phi3T & SPBeta & SPR & SBSphi11 & SBSphi13 & phi29 & SBSphiJ & SPO1
    subgraph Title1[Reference]
        Doron_2018
        Ofir_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_1
        Expressed_2
end
    subgraph Title4[Phage infected]
        SPO1
        SBSphiJ
        SBSphiC
        phi29
        SBSphiC
        SPO1
        SBSphiJ
        phi29
        SBSphiC
        SPO1
        SBSphiJ
        phi3T
        SPBeta
        SPR
        SBSphi11
        SBSphi13
        phi29
        SBSphiJ
        SPO1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41586-021-04098-7
    - doi: 10.1126/science.aar4120

---
::

