---
title: Retron
layout: article
tableColumns:
    article:
      doi: 10.1093/nar/gkaa1149
      abstract: |
        Bacterial retrons consist of a reverse transcriptase (RT) and a contiguous non-coding RNA (ncRNA) gene. One third of annotated retrons carry additional open reading frames (ORFs), the contribution and significance of which in retron biology remains to be determined. In this study we developed a computational pipeline for the systematic prediction of genes specifically associated with retron RTs based on a previously reported large dataset representative of the diversity of prokaryotic RTs. We found that retrons generally comprise a tripartite system composed of the ncRNA, the RT and an additional protein or RT-fused domain with diverse enzymatic functions. These retron systems are highly modular, and their components have coevolved to different extents. Based on the additional module, we classified retrons into 13 types, some of which include additional variants. Our findings provide a basis for future studies on the biological function of retrons and for expanding their biotechnological applications.
    Sensor: Monitor the integrity of the bacterial cell machinery
    Activator: Unknown
    Effector: Diverse
    PFAM: PF00078, PF00089, PF01381, PF01582, PF12686, PF12844, PF13175, PF13304, PF13365, PF13476, PF13560, PF13676
---

# Retron
## Description

Retrons are genetic elements constituted of a non-coding RNA (ncRNA) associated with a reverse-transcriptase (RT). The RT reverse-transcribes part of the ncRNA to generate an RNA-DNA hybrid molecule. Although the existence of retrons have been known for decades, their biological functions were unknown. Recent studies revealed that most retrons could in fact be anti-phage systems (1,2). 

![retron](/retron/Retron_mestre_et_al_fig_1.jpg)

_Fig 1. (Mestre et al., 2020) Structure and organisation of a retron_ . The two non-coding contiguous inverted sequences (named msr and msd) are transcribed as a single RNA. The RT recognizes its specific structure and reverse-transcribes it, generating an RNA-DNA hybrid


The majority of retrons are encoded on a gene cassette that encodes the retron and one or two additional proteins, which act as the retrons effectors. Bioinformatic prediction reveals that these effectors are very diverse and include transmembrane proteins, proteases, Cold-shock proteins, TIR domains proteins, ATPase, endonucleases, etc. Interestingly, several of these effector domains have already been described in other defense systems, including CBASS and Septu. Most retrons appear to act through an Abortive infection strategy (1).

## Molecular mechanisms

The *E.coli* retron system Ec48 mediates growth arrest upon sensing the inactivation of the bacterial RecBCD complex, a key element of the bacterial DNA repair system and immunity (1). Another study demonstrates that several retrons are part of Toxin-Antitoxin systems, where the RT-msDNA complex acts as an antitoxin that binds to and inhibits its cognate toxin. The tempering of the RT-msDNA, possibly by phage-encoded anti-RM systems, abolishes the antitoxin properties of the retron element, resulting in cell death mediated by the toxin activity (2). 


## Example of genomic structure

The Retron system have been describe in a total of 16 subsystems.

Here is some example found in the RefSeq database:

![retron](/retron/Retron_II.svg){max-width=750px}

Retron_II subsystem in the genome of *Klebsiella pneumoniae* (GCF_904866255.1) is composed of 2 proteins: NDT2 (WP_057222224.1)and, RT_Tot (WP_048289034.1).

![retron](/retron/Retron_IV.svg){max-width=750px}

Retron_IV subsystem in the genome of *Aliivibrio fischeri* (GCF_000011805.1) is composed of 2 proteins: RT_Tot (WP_011261677.1)and, 2TM (WP_236727775.1).

![retron](/retron/Retron_I_A.svg){max-width=750px}

Retron_I_A subsystem in the genome of *Vibrio harveyi* (GCF_009184745.1) is composed of 3 proteins: RT_Tot (WP_152163686.1), ATPase_TypeIA (WP_152163687.1)and, HNH_TIGR02646 (WP_152163688.1).

![retron](/retron/Retron_I_B.svg){max-width=750px}

Retron_I_B subsystem in the genome of *Vibrio vulnificus* (GCF_009665475.1) is composed of 2 proteins: ATPase_TOPRIM_COG3593 (WP_103277404.1)and, RT_Tot (WP_043877188.1).

![retron](/retron/Retron_I_C.svg){max-width=750px}

Retron_I_C subsystem in the genome of *Listeria monocytogenes* (GCF_905219385.1) is composed of 1 protein: RT_1_C2 (WP_003726410.1).

![retron](/retron/Retron_V.svg){max-width=750px}

Retron_V subsystem in the genome of *Proteus terrae* (GCF_013171285.1) is composed of 2 proteins: CSD (WP_004244726.1)and, RT_Tot (WP_109418979.1).

![retron](/retron/Retron_VI.svg){max-width=750px}

Retron_VI subsystem in the genome of *Pseudomonas eucalypticola* (GCF_013374995.1) is composed of 2 proteins: HTH (WP_245217789.1)and, RT_Tot (WP_176571652.1).

![retron](/retron/Retron_VII_1.svg){max-width=750px}

Retron_VII_1 subsystem in the genome of *Pseudoxanthomonas mexicana* (GCF_014397415.1) is composed of 1 protein: RT_7_A1 (WP_187572543.1).

![retron](/retron/Retron_VII_2.svg){max-width=750px}

Retron_VII_2 subsystem in the genome of *Bacillus mycoides* (GCF_018742105.1) is composed of 2 proteins: DUF3800 (WP_215564565.1)and, RT_Tot (WP_215564572.1).

![retron](/retron/Retron_XI.svg){max-width=750px}

Retron_XI subsystem in the genome of *Planococcus kocurii* (GCF_001465835.2) is composed of 1 protein: RT_11 (WP_058386256.1).

![retron](/retron/Retron_XII.svg){max-width=750px}

Retron_XII subsystem in the genome of *Stenotrophomonas acidaminiphila* (GCF_014109845.1) is composed of 1 protein: RT_12 (WP_182333825.1).

![retron](/retron/Retron_XIII.svg){max-width=750px}

Retron_XIII subsystem in the genome of *Delftia acidovorans* (GCF_016026535.1) is composed of 3 proteins: ARM (WP_197944577.1), WHSWIM (WP_197944578.1)and, RT_Tot (WP_065344905.1).

## Distribution of the system among prokaryotes

The Retron system is present in a total of 731 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2601 genomes (11.4 %).

![retron](/retron/Distribution_Retron.svg){max-width=750px}

*Proportion of genome encoding the Retron system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Retron_II

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_II,Retron_II__NDT,Ec73,V-plddts_91.19742.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_II,Retron__RT_Tot,Ec73,V-plddts_94.26558.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_II,Retron_II__NDT,Ec73,V-plddts_91.19742.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_II,Retron__RT_Tot,Ec73,V-plddts_94.26558.pdb
---
::

### Retron_III

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_III,Retron_III__PRTase,0,DF-plddts_93.05385.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_III,Retron_III__WH,0,DF-plddts_81.40731.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_III,Retron__RT_Tot,0,DF-plddts_95.80558.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_III,Retron_III__PRTase,0,DF-plddts_93.05385.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_III,Retron_III__WH,0,DF-plddts_81.40731.pdb
---
::

### Retron_IV

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_IV,Retron_IV__2TM,Ec48,V-plddts_90.18352.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_IV,Retron__RT_Tot,Ec48,V-plddts_92.96552.pdb
---
::

### Retron_I_A

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_A.Retron_I_A__HNH_TIGR02646.Ec78.V-plddts_94.92113.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_A.Retron_I_A__HNH_TIGR02646.Ec83.V-plddts_95.89569.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_A.Retron_I_A__HNH_TIGR02646.Vc95.V-plddts_96.13478.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_A.Retron_I_A__HNH_TIGR02646.Ec78.V-plddts_94.92113.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_A.Retron_I_A__HNH_TIGR02646.Ec83.V-plddts_95.89569.pdb
---
::

Example 3:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_A.Retron_I_A__HNH_TIGR02646.Ec78.V-plddts_94.92113.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_A.Retron_I_A__HNH_TIGR02646.Ec83.V-plddts_95.89569.pdb
---
::

### Retron_I_B

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_B,Retron_I_B__ATPase_TOPRIM_COG3593,Eco8,V-plddts_86.92789.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_I_B,Retron__RT_Tot,Eco8,V-plddts_93.00774.pdb
---
::

### Retron_VI

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_VI,Possible_Retron_VI__SP,0,DF-plddts_81.90355.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_VI,Retron_VI__HTH,0,DF-plddts_86.36271.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_VI,Retron__RT_Tot,0,DF-plddts_95.37927.pdb
---
::

### Retron_VII_2

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_VII_2,Retron_VII_2__DUF3800,0,DF-plddts_91.55254.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /retron/Retron_VII_2,Retron__RT_Tot,0,DF-plddts_95.60845.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[ SLATT + RT_G2_intron
Klebsiella pneumoniae's PICI KpCIUCICRE 8 
<a href='https://ncbi.nlm.nih.gov/protein/WP_023301280.1'>WP_023301280.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_023301281.1'>WP_023301281.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & HK97 & HK544 & HK578 & T7
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[ SLATT + RT_G2_intron
Klebsiella pneumoniae's PICI KpCIUCICRE 8 
<a href='https://ncbi.nlm.nih.gov/protein/WP_023301280.1'>WP_023301280.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_023301281.1'>WP_023301281.1</a>] --> Expressed_1[Samonella enterica]
    Expressed_1[Samonella enterica] ----> P22 & BTP1 & ES18
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[ SLATT + RT_G2_intron
Klebsiella pneumoniae's PICI KpCIUCICRE 8 
<a href='https://ncbi.nlm.nih.gov/protein/WP_023301280.1'>WP_023301280.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_023301281.1'>WP_023301281.1</a>] --> Expressed_2[Klebsiella pneumoniae]
    Expressed_2[Klebsiella pneumoniae] ----> Pokey & Raw & Eggy & KaID
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_1
    Origin_1[ RT Ec67 + TOPRIM
Klebsiella pneumoniae's PICI KpCIB28906 
<a href='https://ncbi.nlm.nih.gov/protein/WP_053810728.1'>WP_053810728.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T4 & T5 & HK578 & T7
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_1
    Origin_1[ RT Ec67 + TOPRIM
Klebsiella pneumoniae's PICI KpCIB28906 
<a href='https://ncbi.nlm.nih.gov/protein/WP_053810728.1'>WP_053810728.1</a>] --> Expressed_4[Samonella enterica]
    Expressed_4[Samonella enterica] ----> det7
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_1
    Origin_1[ RT Ec67 + TOPRIM
Klebsiella pneumoniae's PICI KpCIB28906 
<a href='https://ncbi.nlm.nih.gov/protein/WP_053810728.1'>WP_053810728.1</a>] --> Expressed_4[Samonella enterica]
    Expressed_4[Samonella enterica] ----> Pokey & KalD
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_2
    Origin_2[ Retron-TIR
Shigella dysenteriae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_005025120.1'>WP_005025120.1</a>] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> T2 & T4 & T3 & T7 & PhiV-1
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_3
    Origin_3[ Retron Ec67 + TOPRIM
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000169432.1'>WP_000169432.1</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> T2 & T4 & T5
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_4
    Origin_4[ Retron Ec86 + Nuc_deoxy
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001034589.1'>WP_001034589.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001320043.1'>WP_001320043.1</a>] --> Expressed_7[Escherichia coli]
    Expressed_7[Escherichia coli] ----> T4
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_5
    Origin_5[ Retron Ec78 + ATPase + HNH
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001549208.1'>WP_001549208.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001549209.1'>WP_001549209.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_001549210.1'>WP_001549210.1</a>] --> Expressed_8[Escherichia coli]
    Expressed_8[Escherichia coli] ----> T5
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_6
    Origin_6[ Ec73
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_005025120.1*'>WP_005025120.1*</a>] --> Expressed_9[Escherichia coli]
    Expressed_9[Escherichia coli] ----> SECphi4 & SECphi6 & SECphi27 & P1 & T7
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_7
    Origin_7[ Ec86
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2514747571'>2514747571</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2514747569'>2514747569</a>] --> Expressed_10[Escherichia coli]
    Expressed_10[Escherichia coli] ----> T5
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_8
    Origin_8[ Ec48
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2642317602'>2642317602</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2642317601'>2642317601</a>] --> Expressed_11[Escherichia coli]
    Expressed_11[Escherichia coli] ----> Lambda-Vir & T5 & T2 & T4 & T7
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_9
    Origin_9[ Ec67
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2721121890'>2721121890</a>] --> Expressed_12[Escherichia coli]
    Expressed_12[Escherichia coli] ----> T5
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_10
    Origin_10[ Se72
Salmonella enterica 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2633939248'>2633939248</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2633939247'>2633939247</a>] --> Expressed_13[Escherichia coli]
    Expressed_13[Escherichia coli] ----> Lambda-Vir
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_11
    Origin_11[ Ec78
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2647069770'>2647069770</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2647069771'>2647069771</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2647069772'>2647069772</a>] --> Expressed_14[Escherichia coli]
    Expressed_14[Escherichia coli] ----> T5
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_12
    Origin_12[ Ec83
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2712077840'>2712077840</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2712077841'>2712077841</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2712077841'>2712077841</a>] --> Expressed_15[Escherichia coli]
    Expressed_15[Escherichia coli] ----> T2 & T4 & T6
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_13
    Origin_13[ Vc95
Vibrio cholerae 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2598877024'>2598877024</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2598877023'>2598877023</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2598877022'>2598877022</a>] --> Expressed_16[Escherichia coli]
    Expressed_16[Escherichia coli] ----> T2 & T4 & T6
    Millman_2020[<a href='https://doi.org/10.1038/s41564-020-0777-y'>Millman et al., 2020</a>] --> Origin_14
    Origin_14[ Retron-Eco8
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2693183786'>2693183786</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2693183785'>2693183785</a>] --> Expressed_17[Escherichia coli]
    Expressed_17[Escherichia coli] ----> SECphi4 & SECphi6 & SECphi18 & T4 & T6 & T7
    Bobonis_2022[<a href='https://doi.org/10.1038/s41586-022-05091-4'>Bobonis et al., 2022</a>] --> Origin_15
    Origin_15[ Retron-Sen2
Salmonella enterica serovar Typhimurium  
<a href='https://ncbi.nlm.nih.gov/protein/NP_462744.1'>NP_462744.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/NP_462745.3'>NP_462745.3</a>] --> Expressed_18[Escherichia coli]
    Expressed_18[Escherichia coli] ----> T5
    Bobonis_2022[<a href='https://doi.org/10.1038/s41586-022-05091-4'>Bobonis et al., 2022</a>] --> Origin_16
    Origin_16[ Retron-Eco9
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000422112.1'>WP_000422112.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_062914741.1'>WP_062914741.1</a>] --> Expressed_19[Escherichia coli]
    Expressed_19[Escherichia coli] ----> P1vir & T2 & T3 & T5 & T7 & Ffm & Br60
    Bobonis_2022[<a href='https://doi.org/10.1038/s41586-022-05091-4'>Bobonis et al., 2022</a>] --> Origin_17
    Origin_17[ Retron-Eco1
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001320043.1'>WP_001320043.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001034589.1'>WP_001034589.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/'></a>] --> Expressed_20[Escherichia coli]
    Expressed_20[Escherichia coli] ----> T5
    subgraph Title1[Reference]
        Fillol-Salom_2022
        Gao_2020
        Millman_2020
        Bobonis_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_0
        Origin_1
        Origin_1
        Origin_1
        Origin_2
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
        Origin_8
        Origin_9
        Origin_10
        Origin_11
        Origin_12
        Origin_13
        Origin_14
        Origin_15
        Origin_16
        Origin_17
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
        Expressed_8
        Expressed_9
        Expressed_10
        Expressed_11
        Expressed_12
        Expressed_13
        Expressed_14
        Expressed_15
        Expressed_16
        Expressed_17
        Expressed_18
        Expressed_19
        Expressed_20
end
    subgraph Title4[Phage infected]
        T5
        HK97
        HK544
        HK578
        T7
        P22
        BTP1
        ES18
        Pokey
        Raw
        Eggy
        KaID
        T4
        T5
        HK578
        T7
        det7
        Pokey
        KalD
        T2
        T4
        T3
        T7
        PhiV-1
        T2
        T4
        T5
        T4
        T5
        SECphi4
        SECphi6
        SECphi27
        P1
        T7
        T5
        Lambda-Vir
        T5
        T2
        T4
        T7
        T5
        Lambda-Vir
        T5
        T2
        T4
        T6
        T2
        T4
        T6
        SECphi4
        SECphi6
        SECphi18
        T4
        T6
        T7
        T5
        P1vir
        T2
        T3
        T5
        T7
        Ffm
        Br60
        T5
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.cell.2020.09.065
    - doi: 10.1093/nar/gkaa1149

---
::

