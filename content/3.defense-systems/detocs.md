---
title: Detocs
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2023.07.020
      abstract: |
        During viral infection, cells can deploy immune strategies that deprive viruses of molecules essential for their replication. Here, we report a family of immune effectors in bacteria that, upon phage infection, degrade cellular adenosine triphosphate (ATP) and deoxyadenosine triphosphate (dATP) by cleaving the N-glycosidic bond between the adenine and sugar moieties. These ATP nucleosidase effectors are widely distributed within multiple bacterial defense systems, including cyclic oligonucleotide-based antiviral signaling systems (CBASS), prokaryotic argonautes, and nucleotide-binding leucine-rich repeat (NLR)-like proteins, and we show that ATP and dATP degradation during infection halts phage propagation. By analyzing homologs of the immune ATP nucleosidase domain, we discover and characterize Detocs, a family of bacterial defense systems with a two-component phosphotransfer-signaling architecture. The immune ATP nucleosidase domain is also encoded within diverse eukaryotic proteins with immune-like architectures, and we show biochemically that eukaryotic homologs preserve the ATP nucleosidase activity. Our findings suggest that ATP and dATP degradation is a cell-autonomous innate immune strategy conserved across the tree of life.
    PFAM: PF01048, PF18742
relevantAbstracts: 
    - doi: 10.1016/j.cell.2023.07.020
contributors: 
    - François Rousset
---


# Detocs



## Description
Detocs (**De**fensive **T**w**o**-**C**omponent **S**ystem) is a family of 3-gene defense systems that mediate anti-phage activity by abortive infection.

## Molecular mechanism

Detocs shares homology with two-component signal transduction systems. Two-component systems are ubiquitous in prokaryotes and comprise a sensor kinase that typically senses an environmental signal through its N-terminal domain, triggering autophosphorylation of a conserved histidine residue near the C-terminal kinase domain. The phosphate group is then transferred to a conserved aspartate on the N-terminal receiver domain of the second protein, called the “response regulator”. Phosphorylation of the receiver domain activates the C-terminal domain of the response regulator, usually a DNA-binding domain that regulates the expression of target genes. In Detocs, the N-terminal sensor domain on the histidine kinase (DtcA) is intracellular and comprises tetratricopeptide repeats that are believed to sense phage infection; the C-terminal domain of the response regulator (DtcC) is replaced a PNP domain that was shown to specifically cleave ATP molecules into adenine and ribose-5’-triphosphate, both in vitro and during phage infection. Detocs activity leads to a drastic reduction in ATP and dATP levels during infection and to an accumulation of adenine. In parallel, ADP, AMP, dADP and dAMP levels are also reduced, likely in an indirect manner. Detocs induces growth arrest of T5-infected cells, but not of SECphi27-infected cells, suggesting that the outcome of infection following ATP degradation is phage-specific.

While the genetic architecture of Detocs is similar to that of regulatory two-component systems, Detocs also encodes another protein, DtcB, with a standalone receiver domain that is not linked to any effector domain. A point mutation in the receiving aspartate of DtcB is toxic, while overexpression of DtcB impairs the defense capacity of Detocs. Therefore, DtcB likely serves as a “buffer” protein that absorbs phosphate signals that result from inadvertent leaky activation of DtcA in the absence of phage infection, thus preventing autoimmunity.

While 80% of Detocs operons encode PNP effectors, in a minority of these operons the PNP is replaced by other domains known to function as cell-killing effectors in bacterial defense systems, including endonuclease and transmembrane-spanning domains. A Detocs operon with a transmembrane α/β hydrolase effector from Enterobacter cloacae JD6301 was able to efficiently protect E. coli against diverse phages (Rousset et al., 2023).

## Distribution of the system among prokaryotes

Detocs is encoded in Proteobacteria, Bacteroidetes, Firmicutes, Planctomycetes and Chloroflexi phyla.

## Experimental validation
<mermaid>
graph LR;
    Rousset_2023[<a href='https://doi.org/10.1016/j.cell.2023.07.020'>Rousset et al., 2023</a>] --> Origin_0
    Origin_0[Vibrio alginolyticus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2645761408'>2645761408</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2645761407'>2645761407</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2645761406'>2645761406</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & T6
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_1
    Origin_1[ Detocs Hydrolase
Enterobacter cloacae 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2540965173'>2540965173</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2540965172'>2540965172</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2540965171'>2540965171</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> SECPhi27 & T5 & SECPhi18 & SECPhi6 & T2 & T4 & T7
    subgraph Title1[Reference]
        Rousset_2023
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        T2
        T4
        T5
        T6
        SECPhi27
        T5
        SECPhi18
        SECPhi6
        T2
        T4
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Structure

### Detocs

::molstar-pdbe-plugin
---
height: 700
dataUrl: /detocs/Detocs__dtcA-plddts_86.06608.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /detocs/Detocs__dtcB-plddts_95.55971.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /detocs/Detocs__dtcC-plddts_94.54261.pdb
---
::

### Detocs_hydrolase

::molstar-pdbe-plugin
---
height: 700
dataUrl: /detocs/Detocs_hydrolase__dtcA-plddts_85.48132.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /detocs/Detocs_hydrolase__dtcB-plddts_93.6662.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /detocs/Detocs_hydrolase__dtcC-plddts_89.47253.pdb
---
::

