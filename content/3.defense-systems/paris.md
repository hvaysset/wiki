---
title: Paris
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Sensing of phage protein
    Activator: Unknown
    Effector: Unknown
---

# Paris

## Description

PARIS (for Phage Anti-Restriction-Induced System) is a novel anti-phage system. PARIS is found in 4% of prokaryotic genomes. It comprises an ATPase associated with a DUF4435 protein, which can be found either as a two-gene cassette or a single-gene fusion (1).

This system relies on an unknown [Abortive infection](/general-concepts/abortive-infection) mechanism to trigger growth arrest upon sensing a phage-encoded protein (Ocr). Interestingly, the Ocr protein has been found to inhibit R-M systems and BREX systems, making PARIS a suitable defense mechanism against RM resistant and/or BREX resistant phages (1, 2, 3). 

## Example of genomic structure

The Paris system have been describe in a total of 4 subsystems.

Here is some example found in the RefSeq database:

![paris](/paris/PARIS_I.svg){max-width=750px}

PARIS_I subsystem in the genome of *Salmonella enterica* (GCF_020715485.1) is composed of 2 proteins: AAA_15 (WP_001520831.1)and, DUF4435 (WP_010989064.1).

![paris](/paris/PARIS_II.svg){max-width=750px}

PARIS_II subsystem in the genome of *Enterobacter cloacae* (GCF_023238665.1) is composed of 2 proteins: DUF4435 (WP_071830092.1)and, AAA_21 (WP_061772587.1).

![paris](/paris/PARIS_II_merge.svg){max-width=750px}

PARIS_II_merge subsystem in the genome of *Desulfovibrio desulfuricans* (GCF_017815575.1) is composed of 1 protein: AAA_21_DUF4435 (WP_209818471.1).

![paris](/paris/PARIS_I_merge.svg){max-width=750px}

PARIS_I_merge subsystem in the genome of *Sideroxydans lithotrophicus* (GCF_000025705.1) is composed of 1 protein: AAA_15_DUF4435 (WP_013030315.1).

## Distribution of the system among prokaryotes

The Paris system is present in a total of 463 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1145 genomes (5.0 %).

![paris](/paris/Distribution_Paris.svg){max-width=750px}

*Proportion of genome encoding the Paris system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### PARIS_I

::molstar-pdbe-plugin
---
height: 700
dataUrl: /paris/PARIS_I,PARIS_I__AAA_15,0,V-plddts_82.06453.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /paris/PARIS_I,Rst_PARIS__DUF4435,0,V-plddts_91.32921.pdb
---
::

### PARIS_II

::molstar-pdbe-plugin
---
height: 700
dataUrl: /paris/PARIS_II,PARIS_II__AAA_21,0,V-plddts_81.78189.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /paris/PARIS_II,Rst_PARIS__DUF4435,0,V-plddts_95.50028.pdb
---
::

### PARIS_II_merge

::molstar-pdbe-plugin
---
height: 700
dataUrl: /paris/PARIS_II_merge__AAA_21_DUF4435-plddts_93.06529.pdb
---
::

### PARIS_I_merge

::molstar-pdbe-plugin
---
height: 700
dataUrl: /paris/PARIS_I_merge__AAA_15_DUF4435-plddts_85.1867.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[ Paris 1
Escherichia coli  P4 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000334847.1'>WP_000334847.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000342409.1'>WP_000342409.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T4 & CLB_P2 & LF82_P8 & Al505_P2 & T7
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_1
    Origin_1[ Paris 2
Escherichia coli P4 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001007866.1'>WP_001007866.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000093097.1'>WP_000093097.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> Lambda & T4 & CLB_P2 & LF82_P8 & T7
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        Lambda
        T4
        CLB_P2
        LF82_P8
        Al505_P2
        T7
        Lambda
        T4
        CLB_P2
        LF82_P8
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.02.018


---
::
