---
title: Gao_Hhe
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF04480, PF13086, PF13087, PF13195, PF18741
---

# Gao_Hhe
## Example of genomic structure

The Gao_Hhe system is composed of one protein: HheA.

Here is an example found in the RefSeq database: 

![gao_hhe](/gao_hhe/Gao_Hhe.svg){max-width=750px}

Gao_Hhe system in the genome of *Klebsiella pneumoniae* (GCF_011742415.2) is composed of 1 protein: HheA (WP_021314612.1).

## Distribution of the system among prokaryotes

The Gao_Hhe system is present in a total of 49 different species.

Among the 22k complete genomes of RefSeq, this system is present in 279 genomes (1.2 %).

![gao_hhe](/gao_hhe/Distribution_Gao_Hhe.svg){max-width=750px}

*Proportion of genome encoding the Gao_Hhe system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Gao_Hhe

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_hhe/Gao_Hhe__HheA-plddts_81.41701.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_032200272.1'>WP_032200272.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T3 & T7 & PhiV-1
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Lambda
        T3
        T7
        PhiV-1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aba0372

---
::

