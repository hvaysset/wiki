---
title: GAPS4
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.03.28.534373
      abstract: |
        Bacteria are found in ongoing conflicts with rivals and predators, which lead to an evolutionary arms race and the development of innate and adaptive immune systems. Although diverse bacterial immunity mechanisms have been recently identified, many remain unknown, and their dissemination within bacterial populations is poorly understood. Here, we describe a widespread genetic element, defined by the Gamma-Mobile-Trio (GMT) proteins, that serves as a mobile bacterial weapons armory. We show that GMT islands have cargo comprising various combinations of secreted antibacterial toxins, anti-phage defense systems, and secreted anti-eukaryotic toxins. This finding led us to identify four new anti-phage defense systems encoded within GMT islands and reveal their active domains and mechanisms of action. We also find the phage protein that triggers the activation of one of these systems. Thus, we can identify novel toxins and defense systems by investigating proteins of unknown function encoded within GMT islands. Our findings imply that the concept of "defense islands" may be broadened to include other types of bacterial innate immunity mechanisms, such as antibacterial and anti-eukaryotic toxins that appear to stockpile with anti-phage defense systems within GMT weapon islands.
---

# GAPS4

## To do 

## Structure

### GAPS4

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gaps4/GAPS4__GAPS4a-plddts_91.74015.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gaps4/GAPS4__GAPS4b-plddts_86.45931.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Mahata_2023[<a href='https://doi.org/10.1101/2023.03.28.534373'>Mahata et al., 2023</a>] --> Origin_0
    Origin_0[Vibrio parahaemolyticus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_055466293.1'>WP_055466293.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_055466294.1'>WP_055466294.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7 & T4 & P1-vir & Lambda-vir
    subgraph Title1[Reference]
        Mahata_2023
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T7
        T4
        P1-vir
        Lambda-vir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1101/2023.03.28.534373

---
::
