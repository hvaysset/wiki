---
title: BstA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2021.09.002
      abstract: |
        Temperate phages are pervasive in bacterial genomes, existing as vertically inherited islands termed prophages. Prophages are vulnerable to predation of their host bacterium by exogenous phages. Here, we identify BstA, a family of prophage-encoded phage-defense proteins in diverse Gram-negative bacteria. BstA localizes to sites of exogenous phage DNA replication and mediates abortive infection, suppressing the competing phage epidemic. During lytic replication, the BstA-encoding prophage is not itself inhibited by BstA due to self-immunity conferred by the anti-BstA (aba) element, a short stretch of DNA within the bstA locus. Inhibition of phage replication by distinct BstA proteins from Salmonella, Klebsiella, and Escherichia prophages is generally interchangeable, but each possesses a cognate aba element. The specificity of the aba element ensures that immunity is exclusive to the replicating prophage, preventing exploitation by variant BstA-encoding phages. The BstA protein allows prophages to defend host cells against exogenous phage attack without sacrificing the ability to replicate lytically.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
---

# BstA
## Description

BstA is a family of defense systems. BtsA systems from *Salmonella enterica subsp. enterica*, *Klebsiella pneumoniae* and *Escherichia coli* have been shown to provide resistance against a large diversity of phages when expressed in a *S. enterica* or *E.coli* host (1).

The majority of BstA systems appear to be prophage-encoded, as 79% of BstA homologs found in a set of Gram-negative bacterial genomes were associted with phage genes (1).

The defense mechanism encoded by BstA remains to be elucidated. Experimental observation suggest that BtsA could act through an abortive infection mechanism. Fluorescence microscopy experiments suggest that the BstA protein colocalizes with phage DNA. The BstA protein appears to inhibit phage DNA replication during lytic phage infection cycles (1).

Interestingly, part of the BstA locus appears to encode an anti-BstA genetic element (*aba*), which prevents auto-immunity for prophages encoding the BstA locus. The aba element appears to be specific to a given BstA locus, as replacing the aba element from a BstA locus with the aba element from an other BstA system does not prevent auto-immunity (1). 

## Example of genomic structure

The BstA system is composed of one protein: BstA.

Here is an example found in the RefSeq database: 

![bsta](/bsta/BstA.svg){max-width=750px}

BstA system in the genome of *Providencia rustigianii* (GCF_900635875.1) is composed of 1 protein: BstA (WP_126437212.1).

## Distribution of the system among prokaryotes

The BstA system is present in a total of 81 different species.

Among the 22k complete genomes of RefSeq, this system is present in 236 genomes (1.0 %).

![bsta](/bsta/Distribution_BstA.svg){max-width=750px}

*Proportion of genome encoding the BstA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### BstA

::molstar-pdbe-plugin
---
height: 700
dataUrl: /bsta/BstA__BstA-plddts_84.76542.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /bsta/BstA__BstA1-plddts_85.78689.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /bsta/BstA__BstA2-plddts_92.11235.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Owen_2021[<a href='https://doi.org/10.1016/j.chom.2021.09.002'>Owen et al., 2021</a>] --> Origin_0
    Origin_0[Salmonella Typhimurium's BTP1 prophage 
<a href='https://ncbi.nlm.nih.gov/protein/CBG23363.1'>CBG23363.1</a>] --> Expressed_0[Salmonella Typhimurium]
    Expressed_0[Salmonella Typhimurium] ----> P22 & ES18 & 9NA
    Owen_2021[<a href='https://doi.org/10.1016/j.chom.2021.09.002'>Owen et al., 2021</a>] --> Origin_0
    Origin_0[Salmonella Typhimurium's BTP1 prophage 
<a href='https://ncbi.nlm.nih.gov/protein/CBG23363.1'>CBG23363.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> Lambda & Phi80 & P1vir & T7
    Owen_2021[<a href='https://doi.org/10.1016/j.chom.2021.09.002'>Owen et al., 2021</a>] --> Origin_1
    Origin_1[Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/CDO13226.1'>CDO13226.1</a>] --> Expressed_1[Salmonella Typhimurium]
    Expressed_1[Salmonella Typhimurium] ----> BTP1 & P22 & ES18 & P22HT & 9NA & FelixO1
    Owen_2021[<a href='https://doi.org/10.1016/j.chom.2021.09.002'>Owen et al., 2021</a>] --> Origin_2
    Origin_2[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000026554.1'>WP_000026554.1</a>] --> Expressed_2[Salmonella Typhimurium]
    Expressed_2[Salmonella Typhimurium] ----> BTP1 & P22 & ES18 & P22HT & 9NA
    subgraph Title1[Reference]
        Owen_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_3
        Expressed_1
        Expressed_2
end
    subgraph Title4[Phage infected]
        P22
        ES18
        9NA
        Lambda
        Phi80
        P1vir
        T7
        BTP1
        P22
        ES18
        P22HT
        9NA
        FelixO1
        BTP1
        P22
        ES18
        P22HT
        9NA
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2021.09.002

---
::


## References

1. Owen SV, Wenner N, Dulberger CL, Rodwell EV, Bowers-Barnard A, Quinones-Olvera N, Rigden DJ, Rubin EJ, Garner EC, Baym M, Hinton JCD. Prophages encode phage-defense systems with cognate self-immunity. Cell Host Microbe. 2021 Nov 10;29(11):1620-1633.e8. doi: 10.1016/j.chom.2021.09.002. Epub 2021 Sep 30. PMID: 34597593; PMCID: PMC8585504.
