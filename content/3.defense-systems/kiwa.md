---
title: Kiwa
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF16162
---

# Kiwa
## Example of genomic structure

The Kiwa system is composed of 2 proteins: KwaA and, KwaB.

Here is an example found in the RefSeq database: 

![kiwa](/kiwa/Kiwa.svg){max-width=750px}

Kiwa system in the genome of *Aggregatibacter actinomycetemcomitans* (GCF_001690155.1) is composed of 2 proteins: KwaB (WP_005553122.1)and, KwaA (WP_005540311.1).

## Distribution of the system among prokaryotes

The Kiwa system is present in a total of 355 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1104 genomes (4.8 %).

![kiwa](/kiwa/Distribution_Kiwa.svg){max-width=750px}

*Proportion of genome encoding the Kiwa system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Kiwa

::molstar-pdbe-plugin
---
height: 700
dataUrl: /kiwa/Kiwa,Kiwa__KwaA,0,V-plddts_86.99693.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /kiwa/Kiwa,Kiwa__KwaB,0,V-plddts_93.65136.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/AEZ43441.1'>AEZ43441.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AEZ43440.1'>AEZ43440.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi18
    subgraph Title1[Reference]
        Doron_2018
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        LambdaVir
        SECphi18
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aar4120

---
::

