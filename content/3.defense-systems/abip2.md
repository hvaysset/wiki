---
title: AbiP2
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00078
---

# AbiP2
## Example of genomic structure

The AbiP2 system is composed of one protein: AbiP2.

Here is an example found in the RefSeq database: 

![abip2](/abip2/AbiP2.svg){max-width=750px}

AbiP2 system in the genome of *Casimicrobium huifangae* (GCF_009746125.1) is composed of 1 protein: AbiP2 (WP_156862066.1).

## Distribution of the system among prokaryotes

The AbiP2 system is present in a total of 98 different species.

Among the 22k complete genomes of RefSeq, this system is present in 299 genomes (1.3 %).

![abip2](/abip2/Distribution_AbiP2.svg){max-width=750px}

*Proportion of genome encoding the AbiP2 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiP2

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abip2/AbiP2__AbiP2-plddts_93.08218.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[ RT-Abi-P2
Escherichia coli  
<a href='https://ncbi.nlm.nih.gov/protein/WP_047657908.1'>WP_047657908.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_1
    Origin_1[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045971693.1'>WP_045971693.1</a>] --> Expressed_1[lactococci]
    Expressed_1[lactococci] ----> 936
    subgraph Title1[Reference]
        Gao_2020
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        T5
        936
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1093/nar/gkac467

---
::

