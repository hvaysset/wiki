---
title: AbiB
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
---

# AbiB
The AbiB system is composed of one protein: AbiB.

Here is an example found in the RefSeq database: 

![abib](/abib/AbiB.svg){max-width=750px}

AbiB system in the genome of *Lactococcus lactis* (GCF_020221755.1) is composed of 1 protein: AbiB (WP_047687114.1).

## Distribution of the system among prokaryotes

The AbiB system is present in a total of 5 different species.

Among the 22k complete genomes of RefSeq, this system is present in 13 genomes (0.1 %).

![abib](/abib/Distribution_AbiB.svg){max-width=750px}

*Proportion of genome encoding the AbiB system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiB

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abib/AbiB__AbiB-plddts_74.5545.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAA25158.1'>AAA25158.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        936
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


