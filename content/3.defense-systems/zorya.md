---
title: Zorya
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00176, PF00271, PF00691, PF04851, PF15611
---

# Zorya
## Example of genomic structure

The Zorya system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

![zorya](/zorya/Zorya_TypeI.svg){max-width=750px}

Zorya_TypeI subsystem in the genome of *Pseudomonas aeruginosa* (GCF_002085605.1) is composed of 4 proteins: ZorD (WP_015649020.1), ZorC (WP_015649021.1), ZorB (WP_015649022.1)and, ZorA (WP_025297974.1).

![zorya](/zorya/Zorya_TypeII.svg){max-width=750px}

Zorya_TypeII subsystem in the genome of *Legionella longbeachae* (GCF_011465255.1) is composed of 3 proteins: ZorA2 (WP_050777601.1), ZorB (WP_003632756.1)and, ZorE (WP_050777600.1).

## Distribution of the system among prokaryotes

The Zorya system is present in a total of 304 different species.

Among the 22k complete genomes of RefSeq, this system is present in 840 genomes (3.7 %).

![zorya](/zorya/Distribution_Zorya.svg){max-width=750px}

*Proportion of genome encoding the Zorya system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Zorya_TypeI

::molstar-pdbe-plugin
---
height: 700
dataUrl: /zorya/Zorya_TypeI,Zorya_TypeI__ZorC,0,V-plddts_85.7807.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /zorya/Zorya_TypeI,Zorya_TypeI__ZorD,0,V-plddts_80.74261.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /zorya/Zorya_TypeI,Zorya__ZorA,0,V-plddts_72.58295.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /zorya/Zorya_TypeI,Zorya__ZorB,0,V-plddts_88.9782.pdb
---
::

### Zorya_TypeII

::molstar-pdbe-plugin
---
height: 700
dataUrl: /zorya/Zorya_TypeII,Zorya_TypeII__ZorE,0,V-plddts_88.59937.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /zorya/Zorya_TypeII,Zorya__ZorA,0,V-plddts_77.19519.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /zorya/Zorya_TypeII,Zorya__ZorB,0,V-plddts_91.39341.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[ Type I
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/ABV17222.1'>ABV17222.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ABV17786.1'>ABV17786.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ABV16709.1'>ABV16709.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ABV17208.1'>ABV17208.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi27 & T7
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_1
    Origin_1[ Type II
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/ACA79490.1'>ACA79490.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ACA79491.1'>ACA79491.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ACA79492.1'>ACA79492.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T7 & SECphi17
    Payne_2021[<a href='https://doi.org/10.1093/nar/gkab883'>Payne et al., 2021</a>] --> Origin_2
    Origin_2[ Type III
Stenotrophomonas nitritireducens 
<a href='https://ncbi.nlm.nih.gov/protein/WP_055768786.1'>WP_055768786.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_055768783.1'>WP_055768783.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_055768781.1'>WP_055768781.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_055768778.1'>WP_055768778.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T1 & T4 & T7 & LambdaVir & PVP-SE1
    subgraph Title1[Reference]
        Doron_2018
        Payne_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
end
    subgraph Title4[Phage infected]
        LambdaVir
        SECphi27
        T7
        T7
        SECphi17
        T1
        T4
        T7
        LambdaVir
        PVP-SE1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1093/nar/gkab883
    - doi: 10.1126/science.aar4120

---
::

