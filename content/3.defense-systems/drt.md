---
title: DRT
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00078
---

# DRT
## Example of genomic structure

The DRT system have been describe in a total of 9 subsystems.

Here is some example found in the RefSeq database:

![drt](/drt/DRT6.svg){max-width=750px}

DRT6 subsystem in the genome of *Methylobacterium sp.* (GCF_003254375.1) is composed of 1 protein: DRT6 (WP_111474389.1).

![drt](/drt/DRT8.svg){max-width=750px}

DRT8 subsystem in the genome of *Undibacterium sp.* (GCF_009937955.1) is composed of 2 proteins: DRT8b (WP_162060770.1)and, DRT8 (WP_162060771.1).

![drt](/drt/DRT9.svg){max-width=750px}

DRT9 subsystem in the genome of *Pseudomonas aeruginosa* (GCF_016864415.1) is composed of 1 protein: DRT9 (WP_071567741.1).

![drt](/drt/DRT_1.svg){max-width=750px}

DRT_1 subsystem in the genome of *Vibrio parahaemolyticus* (GCF_000430405.1) is composed of 2 proteins: drt1a (WP_020841728.1)and, drt1b (WP_020841729.1).

![drt](/drt/DRT_2.svg){max-width=750px}

DRT_2 subsystem in the genome of *Klebsiella variicola* (GCF_018324045.1) is composed of 1 protein: drt2 (WP_020244644.1).

![drt](/drt/DRT_3.svg){max-width=750px}

DRT_3 subsystem in the genome of *Vibrio mimicus* (GCF_019048845.1) is composed of 2 proteins: drt3a (WP_217011272.1)and, drt3b (WP_217011273.1).

![drt](/drt/DRT_4.svg){max-width=750px}

DRT_4 subsystem in the genome of *Escherichia albertii* (GCF_003316815.1) is composed of 1 protein: drt4 (WP_103054060.1).

![drt](/drt/DRT_5.svg){max-width=750px}

DRT_5 subsystem in the genome of *Escherichia coli* (GCF_016904115.1) is composed of 1 protein: drt5 (WP_001524904.1).

## Distribution of the system among prokaryotes

The DRT system is present in a total of 573 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1365 genomes (6.0 %).

![drt](/drt/Distribution_DRT.svg){max-width=750px}

*Proportion of genome encoding the DRT system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### DRT6

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT6__DRT6-plddts_92.73056.pdb
---
::

### DRT7

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT7__DRT7-plddts_85.85621.pdb
---
::

### DRT8

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT8__DRT8-plddts_92.44735.pdb
---
::

### DRT9

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT9__DRT9-plddts_91.47402.pdb
---
::

### DRT_1

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT_1,DRT_1__drt1a,0,V-plddts_88.59974.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT_1,DRT_1__drt1b,0,V-plddts_92.26985.pdb
---
::

### DRT_2

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT_2__drt2-plddts_95.09027.pdb
---
::

### DRT_3

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT_3,DRT_3__drt3a,0,V-plddts_91.16835.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT_3,DRT_3__drt3b,0,V-plddts_87.11665.pdb
---
::

### DRT_4

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT_4__drt4-plddts_92.79751.pdb
---
::

### DRT_5

::molstar-pdbe-plugin
---
height: 700
dataUrl: /drt/DRT_5__drt5-plddts_90.89502.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[ RT-nitrilase UG1 Type 1
Klebsiella pneumoniae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_115196278.1'>WP_115196278.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_040189938.1'>WP_040189938.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_1
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_1
    Origin_1[ RT UG2 Type 2
Salmonella enterica 
<a href='https://ncbi.nlm.nih.gov/protein/WP_012737279.1'>WP_012737279.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T5 & T2
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_2
    Origin_2[ RT UG3 + RT UG8 type 3
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_087902017.1'>WP_087902017.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_062891751.1'>WP_062891751.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T5 & Lambda
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_3
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_3
    Origin_3[ RT UG15 Type 4
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/GCK53192.1'>GCK53192.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T5 & T3 & T7 & Phi-V1 & ZL19
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_4
    Origin_4[ RT UG16 Type 5
Escherichia coli  
<a href='https://ncbi.nlm.nih.gov/protein/WP_001524904.1'>WP_001524904.1</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> T2
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_5
    Origin_5[ RT UG10 Type 7
Escherichia coli  
<a href='https://ncbi.nlm.nih.gov/protein/WP_096936834.1'>WP_096936834.1</a>] --> Expressed_5[Escherichia coli]
    Expressed_5[Escherichia coli] ----> T2 & T5 & ZL-19
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_6
    Origin_6[ RT UG7 Type 8
Escherichia coli  
<a href='https://ncbi.nlm.nih.gov/protein/WP_123894217.1'>WP_123894217.1</a>] --> Expressed_6[Escherichia coli]
    Expressed_6[Escherichia coli] ----> T2 & T5
    Mestre_2022[<a href='https://doi.org/10.1093/nar/gkac467'>Mestre et al., 2022</a>] --> Origin_7
    Origin_7[ RT UG28 Type 9
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/EAO1508900.1'>EAO1508900.1</a>] --> Expressed_7[Escherichia coli]
    Expressed_7[Escherichia coli] ----> T2 & T5 & ZL-19
    subgraph Title1[Reference]
        Gao_2020
        Mestre_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_1
        Origin_2
        Origin_3
        Origin_3
        Origin_4
        Origin_5
        Origin_6
        Origin_7
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_3
        Expressed_4
        Expressed_5
        Expressed_6
        Expressed_7
end
    subgraph Title4[Phage infected]
        T2
        T4
        T5
        T5
        T2
        T5
        T2
        T2
        T5
        Lambda
        T5
        T3
        T7
        Phi-V1
        ZL19
        T5
        T3
        T7
        Phi-V1
        ZL19
        T2
        T2
        T5
        ZL-19
        T2
        T5
        T2
        T5
        ZL-19
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1093/nar/gkac467
    - doi: 10.1126/science.aba0372

---
::

