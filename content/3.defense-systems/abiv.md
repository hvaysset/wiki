---
title: AbiV
layout: article
tableColumns:
    article:
      doi: 10.1128/AEM.00780-08
      abstract: |
        Insertional mutagenesis with pGhost9::ISS1 resulted in independent insertions in a 350-bp region of the chromosome of Lactococcus lactis subsp. cremoris MG1363 that conferred phage resistance to the integrants. The orientation and location of the insertions suggested that the phage resistance phenotype was caused by a chromosomal gene turned on by a promoter from the inserted construct. Reverse transcription-PCR analysis confirmed that there were higher levels of transcription of a downstream open reading frame (ORF) in the phage-resistant integrants than in the phage-sensitive strain L. lactis MG1363. This gene was also found to confer phage resistance to L. lactis MG1363 when it was cloned into an expression vector. A subsequent frameshift mutation in the ORF completely eliminated the phage resistance phenotype, confirming that the ORF was necessary for phage resistance. This ORF provided resistance against virulent lactococcal phages belonging to the 936 and c2 species with an efficiency of plaquing of 10?4, but it did not protect against members of the P335 species. A high level of expression of the ORF did not affect the cellular growth rate. Assays for phage adsorption, DNA ejection, restriction/modification activity, plaque size, phage DNA replication, and cell survival showed that the ORF encoded an abortive infection (Abi) mechanism. Sequence analysis revealed a deduced protein consisting of 201 amino acids which, in its native state, probably forms a dimer in the cytosol. Similarity searches revealed no homology to other phage resistance mechanisms, and thus, this novel Abi mechanism was designated AbiV. The mode of action of AbiV is unknown, but the activity of AbiV prevented cleavage of the replicated phage DNA of 936-like phages.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF18728
---

# AbiV
## Example of genomic structure

The AbiV system is composed of one protein: AbiV.

Here is an example found in the RefSeq database: 

![abiv](/abiv/AbiV.svg){max-width=750px}

AbiV system in the genome of *Lactococcus cremoris* (GCF_017376415.1) is composed of 1 protein: AbiV (WP_011834704.1).

## Distribution of the system among prokaryotes

The AbiV system is present in a total of 76 different species.

Among the 22k complete genomes of RefSeq, this system is present in 126 genomes (0.6 %).

![abiv](/abiv/Distribution_AbiV.svg){max-width=750px}

*Proportion of genome encoding the AbiV system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiV

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abiv/AbiV__AbiV-plddts_93.56204.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Haaber_2008[<a href='https://doi.org/10.1128/AEM.00780-08'>Haaber et al., 2008</a>] --> Origin_0
    Origin_0[Lactococcus lactis 
<a href='https://ncbi.nlm.nih.gov/protein/AAK16428.1'>AAK16428.1</a>] --> Expressed_0[Lactococcus lactis]
    Expressed_0[Lactococcus lactis] ----> sk1 & p2 & jj50 & P008 & bIL170 & c2 & bIL67 & ml3 & eb1
    subgraph Title1[Reference]
        Haaber_2008
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        sk1
        p2
        jj50
        P008
        bIL170
        c2
        bIL67
        ml3
        eb1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1128/AEM.00780-08

---
::

