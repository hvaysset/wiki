---
title: Mokosh
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00069, PF07714, PF08378, PF13086, PF13087, PF13091, PF13245, PF13604
---

# Mokosh
## Example of genomic structure

The Mokosh system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

![mokosh](/mokosh/Mokosh_TypeI.svg){max-width=750px}

Mokosh_TypeI subsystem in the genome of *Vibrio alginolyticus* (GCF_022343125.1) is composed of 2 proteins: MkoB2 (WP_238970063.1)and, MkoA2 (WP_238970065.1).

![mokosh](/mokosh/Mokosh_TypeII.svg){max-width=750px}

Mokosh_TypeII subsystem in the genome of *Shigella flexneri* (GCF_022354205.1) is composed of 1 protein: MkoC (WP_000344091.1).

## Distribution of the system among prokaryotes

The Mokosh system is present in a total of 605 different species.

Among the 22k complete genomes of RefSeq, this system is present in 2540 genomes (11.1 %).

![mokosh](/mokosh/Distribution_Mokosh.svg){max-width=750px}

*Proportion of genome encoding the Mokosh system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Mokosh_TypeI

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /mokosh/Mokosh_TypeI,Mokosh_TypeI__MkoA,0,V-plddts_81.37154.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mokosh/Mokosh_TypeI,Mokosh_TypeI__MkoB,0,V-plddts_91.10094.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /mokosh/Mokosh_TypeI,Mokosh_TypeI__MkoA,0,V-plddts_81.37154.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mokosh/Mokosh_TypeI,Mokosh_TypeI__MkoB,0,V-plddts_91.10094.pdb
---
::

### Mokosh_TypeII

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mokosh/Mokosh_TypeII__MkoC-plddts_90.69139.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[ Type I
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2597791081'>2597791081</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2597791080'>2597791080</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & LambdaVir & T5 & SECphi27
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_1
    Origin_1[ Type I
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2720391838'>2720391838</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2720391839'>2720391839</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> LambdaVir
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_2
    Origin_2[ Type II
Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2586378093'>2586378093</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> SECphi17
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
        LambdaVir
        T5
        SECphi27
        LambdaVir
        SECphi17
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017

---
::

