---
title: RloC
layout: article
tableColumns:
    article:
      doi: 10.1111/j.1365-2958.2008.06387.x
      abstract: |
        The conserved bacterial protein RloC, a distant homologue of the tRNA(Lys) anticodon nuclease (ACNase) PrrC, is shown here to act as a wobble nucleotide-excising and Zn(++)-responsive tRNase. The more familiar PrrC is silenced by a genetically linked type I DNA restriction-modification (R-M) enzyme, activated by a phage anti-DNA restriction factor and counteracted by phage tRNA repair enzymes. RloC shares PrrC's ABC ATPase motifs and catalytic ACNase triad but features a distinct zinc-hook/coiled-coil insert that renders its ATPase domain similar to Rad50 and related DNA repair proteins. Geobacillus kaustophilus RloC expressed in Escherichia coli exhibited ACNase activity that differed from PrrC's in substrate preference and ability to excise the wobble nucleotide. The latter specificity could impede reversal by phage tRNA repair enzymes and account perhaps for RloC's more frequent occurrence. Mutagenesis and functional assays confirmed RloC's catalytic triad assignment and implicated its zinc hook in regulating the ACNase function. Unlike PrrC, RloC is rarely linked to a type I R-M system but other genomic attributes suggest their possible interaction in trans. As DNA damage alleviates type I DNA restriction, we further propose that these related perturbations prompt RloC to disable translation and thus ward off phage escaping DNA restriction during the recovery from DNA damage.
    Sensor: Monitor the integrity of the bacterial cell machinery
    Activator: Unknown
    Effector: Nucleic acid degrading
    PFAM: PF13166
---

# RloC
## Example of genomic structure

The RloC system is composed of one protein: RloC.

Here is an example found in the RefSeq database: 

![rloc](/rloc/RloC.svg){max-width=750px}

RloC system in the genome of *Flavobacterium arcticum* (GCF_003344925.1) is composed of 1 protein: RloC (WP_114676820.1).

## Distribution of the system among prokaryotes

The RloC system is present in a total of 803 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1961 genomes (8.6 %).

![rloc](/rloc/Distribution_RloC.svg){max-width=750px}

*Proportion of genome encoding the RloC system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### RloC

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rloc/RloC__RloC-plddts_86.73175.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Penner_1995[<a href='https://doi.org/10.1006/jmbi.1995.0343'>Penner et al., 1995</a>] --> Origin_0
    Origin_0[Escherichia coli] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4
    subgraph Title1[Reference]
        Penner_1995
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1111/j.1365-2958.2008.06387.x
    - doi: 10.1111/mmi.13074

---
::

