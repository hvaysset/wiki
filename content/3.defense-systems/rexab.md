---
title: RexAB
layout: article
tableColumns:
    article:
      doi: 10.1101/gad.6.3.497
      abstract: |
        The rexA and rexB genes of bacteriophage lambda encode a two-component system that aborts lytic growth of bacterial viruses. Rex exclusion is characterized by termination of macromolecular synthesis, loss of active transport, the hydrolysis of ATP, and cell death. By analogy to colicins E1 and K, these results can be explained by depolarization of the cytoplasmic membrane. We have fractionated cells to determine the intracellular location of the RexB protein and made RexB-alkaline phosphatase fusions to analyze its membrane topology. The RexB protein appears to be a polytopic transmembrane protein. We suggest that RexB proteins form ion channels that, in response to lytic growth of bacteriophages, depolarize the cytoplasmic membrane. The Rex system requires a mechanism to prevent lambda itself from being excluded during lytic growth. We have determined that overexpression of RexB in lambda lysogens prevents the exclusion of both T4 rII mutants and lambda ren mutants. We suspect that overexpression of RexB is the basis for preventing self-exclusion following the induction of a lambda lysogen and that RexB overexpression is accomplished through transcriptional regulation.
    Sensor: Sensing of complex phage protein/DNA
    Activator: Direct
    Effector: Membrane disrupting
    PFAM: PF15968, PF15969
---

# RexAB
## Example of genomic structure

The RexAB system is composed of 2 proteins: RexA and, RexB.

Here is an example found in the RefSeq database: 

![rexab](/rexab/RexAB.svg){max-width=750px}

RexAB system in the genome of *Escherichia coli* (GCF_008033315.1) is composed of 2 proteins: RexA (WP_000788349.1)and, RexB (WP_001245922.1).

## Distribution of the system among prokaryotes

The RexAB system is present in a total of 17 different species.

Among the 22k complete genomes of RefSeq, this system is present in 73 genomes (0.3 %).

![rexab](/rexab/Distribution_RexAB.svg){max-width=750px}

*Proportion of genome encoding the RexAB system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### RexAB

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rexab/RexAB,RexAB_RexA,0,V-plddts_80.37265.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rexab/RexAB,RexAB_RexB,0,V-plddts_87.11344.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Parma_1992[<a href='https://doi.org/10.1101/gad.6.3.497'>Parma et al., 1992</a>] --> Origin_0
    Origin_0[Escherichia coli lambda prophage 
<a href='https://ncbi.nlm.nih.gov/protein/NP_040626.1'>NP_040626.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/NP_040627.1'>NP_040627.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4 & Lamboidphages
    subgraph Title1[Reference]
        Parma_1992
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T4
        Lamboidphages
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1101/gad.6.3.497

---
::

