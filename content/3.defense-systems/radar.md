---
title: RADAR
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Nucleic acid degrading
---

# RADAR
## Description

RADAR is comprised of two genes, encoding respectively for an adenosine triphosphatase (RdrA) and  a divergent adenosine deaminase (RdrB) (1), which are in some cases associated with a small membrane protein (RdrC or D) (1). 

RADAR was found to perform RNA editing of adenosine to inosine during phage infection. Editing sites are broadly distributed on the host transcriptome, which could prove deleterious to the host and explain the observed growth arrest of RADAR upon phage infection.  

## Molecular mechanism
RADAR mediates growth arrest upon infection and is therefore considered to be an Abortive infection system.

## Example of genomic structure

The RADAR system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

![radar](/radar/radar_I.svg){max-width=750px}

radar_I subsystem in the genome of *Dickeya dianthicola* (GCF_014893095.1) is composed of 2 proteins: rdrB_I (WP_192988590.1)and, rdrA_I (WP_192988591.1).

![radar](/radar/radar_II.svg){max-width=750px}

radar_II subsystem in the genome of *Klebsiella aerogenes* (GCF_008693885.1) is composed of 3 proteins: rdrD_II (WP_015705078.1), rdrB_II (WP_015705077.1)and, rdrA_II (WP_015705076.1).

## Distribution of the system among prokaryotes

The RADAR system is present in a total of 39 different species.

Among the 22k complete genomes of RefSeq, this system is present in 135 genomes (0.6 %).

![radar](/radar/Distribution_RADAR.svg){max-width=750px}

*Proportion of genome encoding the RADAR system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### radar_I

::molstar-pdbe-plugin
---
height: 700
dataUrl: /radar/radar_I,radar_I__rdrA_I,0,V-plddts_84.65854.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /radar/radar_I,radar_I__rdrB_I,0,V-plddts_87.10562.pdb
---
::

### radar_II

::molstar-pdbe-plugin
---
height: 700
dataUrl: /radar/radar_II,radar_II__rdrA_II,0,V-plddts_84.35322.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /radar/radar_II,radar_II__rdrB_II,0,V-plddts_86.83828.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /radar/radar_II,radar_II__rdrC_II,0,V-plddts_75.42507.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Duncan-Lowey_2022[<a href='https://doi.org/10.1016/j.cell.2023.01.012'>Duncan-Lowey et al., 2023</a>] --> Origin_0
    Origin_0[Citrobacter rodentium 
<a href='https://ncbi.nlm.nih.gov/protein/WP_012906049.1'>WP_012906049.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_012906048.1'>WP_012906048.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & T3 & T6
    Duncan-Lowey_2022[<a href='https://doi.org/10.1016/j.cell.2023.01.012'>Duncan-Lowey et al., 2023</a>] --> Origin_1
    Origin_1[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2548796856'>2548796856</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2548796855'>2548796855</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T2 & T4 & T6
    Duncan-Lowey_2022[<a href='https://doi.org/10.1016/j.cell.2023.01.012'>Duncan-Lowey et al., 2023</a>] --> Origin_2
    Origin_2[Streptococcus suis 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2706833061'>2706833061</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2706833062'>2706833062</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T4 & T5 & T6
    subgraph Title1[Reference]
        Gao_2020
        Duncan-Lowey_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
        Origin_2
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_0
        Expressed_1
        Expressed_2
end
    subgraph Title4[Phage infected]
        T2
        T4
        T5
        T3
        T6
        T2
        T4
        T5
        T3
        T6
        T2
        T4
        T6
        T2
        T4
        T5
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aba0372

---
::


## References

1. Gao L, Altae-Tran H, BÃ¶hning F, et al. Diverse enzymatic activities mediate antiviral immunity in prokaryotes. *Science*. 2020;369(6507):1077-1084. doi:10.1126/science.aba0372
