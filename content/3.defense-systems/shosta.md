---
title: ShosTA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF02481
---

# ShosTA
## Example of genomic structure

The ShosTA system is composed of 2 proteins: ShosA and, ShosT.

Here is an example found in the RefSeq database: 

![shosta](/shosta/ShosTA.svg){max-width=750px}

ShosTA system in the genome of *Escherichia coli* (GCF_011404895.1) is composed of 2 proteins: ShosA (WP_001567470.1)and, ShosT (WP_001567471.1).

## Distribution of the system among prokaryotes

The ShosTA system is present in a total of 299 different species.

Among the 22k complete genomes of RefSeq, this system is present in 668 genomes (2.9 %).

![shosta](/shosta/Distribution_ShosTA.svg){max-width=750px}

*Proportion of genome encoding the ShosTA system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### ShosTA

::molstar-pdbe-plugin
---
height: 700
dataUrl: /shosta/ShosTA,ShosTA__ShosA,0,V-plddts_93.0196.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /shosta/ShosTA,ShosTA__ShosT,0,V-plddts_91.38081.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2564403099'>2564403099</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2564403100'>2564403100</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> LambdaVir & SECphi4 & SECphi6 & SECphi18 & T7
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_1
    Origin_1[Escherichia coli P2 loci] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> Lambda & T7
    subgraph Title1[Reference]
        Millman_2022
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        LambdaVir
        SECphi4
        SECphi6
        SECphi18
        T7
        Lambda
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1016/j.chom.2022.09.017
    - doi: 10.1101/gr.133850.111

---
::

