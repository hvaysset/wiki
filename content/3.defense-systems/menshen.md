---
title: Menshen
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF03235, PF05973, PF12476, PF13175, PF13304, PF13476
---

# Menshen
## Example of genomic structure

The Menshen system is composed of 3 proteins: NsnA, NsnB and, NsnC_2623244837.

Here is an example found in the RefSeq database: 

![menshen](/menshen/Menshen.svg){max-width=750px}

Menshen system in the genome of *Citrobacter freundii* (GCF_003937345.2) is composed of 3 proteins: NsnA (WP_125363058.1), NsnB (WP_197964486.1)and, NsnC_2617187710 (WP_125363056.1).

## Distribution of the system among prokaryotes

The Menshen system is present in a total of 247 different species.

Among the 22k complete genomes of RefSeq, this system is present in 446 genomes (2.0 %).

![menshen](/menshen/Distribution_Menshen.svg){max-width=750px}

*Proportion of genome encoding the Menshen system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Menshen

::molstar-pdbe-plugin
---
height: 700
dataUrl: /menshen/Menshen,Menshen__NsnA,0,V-plddts_87.39628.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /menshen/Menshen,Menshen__NsnB,0,V-plddts_89.66947.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /menshen/Menshen,Menshen__NsnC,0,V-plddts_91.07244.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Solibacillus silvestris 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2562294789'>2562294789</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2562294790'>2562294790</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2562294791'>2562294791</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Solibacillus silvestris 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2562294789'>2562294789</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2562294790'>2562294790</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2562294791'>2562294791</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> Fado
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
        Fado
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017

---
::

