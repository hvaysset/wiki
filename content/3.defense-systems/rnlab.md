---
title: RnlAB
layout: article
tableColumns:
    article:
      doi: 10.1534/genetics.110.121798
      abstract: |
        RNase LS was originally identified as a potential antagonist of bacteriophage T4 infection. When T4 dmd is defective, RNase LS activity rapidly increases after T4 infection and cleaves T4 mRNAs to antagonize T4 reproduction. Here we show that rnlA, a structural gene of RNase LS, encodes a novel toxin, and that rnlB (formally yfjO), located immediately downstream of rnlA, encodes an antitoxin against RnlA. Ectopic expression of RnlA caused inhibition of cell growth and rapid degradation of mRNAs in ?rnlAB cells. On the other hand, RnlB neutralized these RnlA effects. Furthermore, overexpression of RnlB in wild-type cells could completely suppress the growth defect of a T4 dmd mutant, that is, excess RnlB inhibited RNase LS activity. Pull-down analysis showed a specific interaction between RnlA and RnlB. Compared to RnlA, RnlB was extremely unstable, being degraded by ClpXP and Lon proteases, and this instability may increase RNase LS activity after T4 infection. All of these results suggested that rnlA-rnlB define a new toxin-antitoxin (TA) system.
    Sensor: Monitor the integrity of the bacterial cell machinery
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF15933, PF15935, PF18869, PF19034
---

# RnlAB
## Example of genomic structure

The RnlAB system is composed of 2 proteins: RnlA and, RnlB.

Here is an example found in the RefSeq database: 

![rnlab](/rnlab/RnlAB.svg){max-width=750px}

RnlAB system in the genome of *Escherichia coli* (GCF_014338505.1) is composed of 2 proteins: RnlA (WP_000155570.1)and, RnlB (WP_000461704.1).

## Distribution of the system among prokaryotes

The RnlAB system is present in a total of 71 different species.

Among the 22k complete genomes of RefSeq, this system is present in 284 genomes (1.2 %).

![rnlab](/rnlab/Distribution_RnlAB.svg){max-width=750px}

*Proportion of genome encoding the RnlAB system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### RnlAB

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rnlab/RnlAB,RnlAB__RnlA,0,DF-plddts_91.3649.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rnlab/RnlAB,RnlAB__RnlB,0,DF-plddts_92.42543.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Koga_2011[<a href='https://doi.org/10.1534/genetics.110.121798'>Koga et al., 2011</a>] --> Origin_0
    Origin_0[Escherichia coli] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T4
    subgraph Title1[Reference]
        Koga_2011
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1534/genetics.110.121798

---
::

