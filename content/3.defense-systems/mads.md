---
title: MADS
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.03.30.534895
      abstract: |
        The constant arms race between bacteria and their phages has resulted in a large diversity of bacterial defence systems1,2, with many bacteria carrying several systems3,4. In response, phages often carry counter-defence genes5-9. If and how bacterial defence mechanisms interact to protect against phages with counter-defence genes remains unclear. Here, we report the existence of a novel defence system, coined MADS (Methylation Associated Defence System), which is located in a strongly conserved genomic defence hotspot in Pseudomonas aeruginosa and distributed across Gram-positive and Gram-negative bacteria. We find that the natural co-existence of MADS and a Type IE CRISPR-Cas adaptive immune system in the genome of P. aeruginosa SMC4386 provides synergistic levels of protection against phage DMS3, which carries an anti-CRISPR (acr) gene. Previous work has demonstrated that Acr-phages need to cooperate to overcome CRISPR immunity, with a first sacrificial phage causing host immunosuppression to enable successful secondary phage infections10,11. Modelling and experiments show that the co-existence of MADS and CRISPR-Cas provides strong and durable protection against Acr-phages by disrupting their cooperation and limiting the spread of mutants that overcome MADS. These data reveal that combining bacterial defences can robustly neutralise phage with counter-defence genes, even if each defence on its own can be readily by-passed, which is key to understanding how selection acts on defence combinations and their coevolutionary consequences.
    PFAM: PF00069, PF01170, PF02384, PF07714, PF08378, PF12728, PF13304, PF13588
---

# MADS

## To do 

## Structure

### MADS

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mads/MADS__mad1-plddts_82.3118.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mads/MADS__mad2-plddts_87.54253.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mads/MADS__mad3-plddts_93.27207.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mads/MADS__mad4-plddts_92.44061.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mads/MADS__mad5-plddts_93.90587.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mads/MADS__mad7-plddts_91.54141.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /mads/MADS__mad8-plddts_82.23514.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Maestri_2023[<a href='https://doi.org/10.1101/2023.03.30.534895'>Maestri et al., 2023</a>] --> Origin_0
    Origin_0[Pseudomonas aeruginosa 
<a href='https://ncbi.nlm.nih.gov/protein/WP_058876987.1'>WP_058876987.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_058876988.1'>WP_058876988.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_058876989.1'>WP_058876989.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_058876990.1'>WP_058876990.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_058876991.1'>WP_058876991.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_058876992.1'>WP_058876992.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_124170283.1'>WP_124170283.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_201267341.1'>WP_201267341.1</a>] --> Expressed_0[Pseudomonas aeruginosa]
    Expressed_0[Pseudomonas aeruginosa] ----> DMS3vir
    subgraph Title1[Reference]
        Maestri_2023
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        DMS3vir
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1101/2023.03.30.534895

---
::

