---
title: dCTPdeaminase
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01158-0
      abstract: |
        DNA viruses and retroviruses consume large quantities of deoxynucleotides (dNTPs) when replicating. The human antiviral factor SAMHD1 takes advantage of this vulnerability in the viral lifecycle, and inhibits viral replication by degrading dNTPs into their constituent deoxynucleosides and inorganic phosphate. Here, we report that bacteria use a similar strategy to defend against bacteriophage infection. We identify a family of defensive bacterial deoxycytidine triphosphate (dCTP) deaminase proteins that convert dCTP into deoxyuracil nucleotides in response to phage infection. We also identify a family of phage resistance genes that encode deoxyguanosine triphosphatase (dGTPase) enzymes, which degrade dGTP into phosphate-free deoxyguanosine and are distant homologues of human SAMHD1. Our results suggest that bacterial defensive proteins deplete specific deoxynucleotides (either dCTP or dGTP) from the nucleotide pool during phage infection, thus starving the phage of an essential DNA building block and halting its replication. Our study shows that manipulation of the dNTP pool is a potent antiviral strategy shared by both prokaryotes and eukaryotes..
    Sensor: Host integrity monitoring
    Activator: Unknown
    Effector: Nucleotide modifying
    PFAM: PF00383, PF14437
contributors:
    - Nathalie Bechon
relevantAbstracts:
    - doi: 10.1038/s41564-022-01162-4
    - doi: 10.1038/s41564-022-01158-0
---


# dCTPdeaminase
## Description
dCTPdeaminase is a family of systems. dCTPdeaminase from Escherichia coli has been shown to provide resistance against various lytic phages when expressed heterologously in another Escherichia coli by degrading the pool of dCTP available for phage DNA replication.
This system is mostly found in Proteobacteria but a few examples also exist in Acidobacteria, Actinobacteria, Bacteroidetes, Cyanobacteria, Firmicutes, Planctomyces, and Verrucomicrobia.
Those systems can be found in plasmids (around 8%).

## Mechanism
When activated by a phage infection, dCTPdeaminase, will convert deoxycytidine (dCTP/dCDP/dCMP) into deoxyuridine.
This action will deplete the pool of CTP nucleotide necessary for the phage replication and will stop the infection.
The trigger for dCTPdeaminase may be linked to the shutoff of RNAP ($\sigma$ S-dependent host RNA polymerase) that occur during phage infections.

## Example of genomic structure

The dCTPdeaminase system is composed of one protein: dCTPdeaminase.

Here is an example found in the RefSeq database: 

![dctpdeaminase](/dctpdeaminase/dCTPdeaminase.svg){max-width=750px}

dCTPdeaminase system in the genome of *Vibrio parahaemolyticus* (GCF_009883855.1) is composed of 1 protein: dCTPdeaminase (WP_029845369.1).

## Distribution of the system among prokaryotes

The dCTPdeaminase system is present in a total of 269 different species.

Among the 22k complete genomes of RefSeq, this system is present in 501 genomes (2.2 %).

![dctpdeaminase](/dctpdeaminase/Distribution_dCTPdeaminase.svg){max-width=750px}

*Proportion of genome encoding the dCTPdeaminase system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### dCTPdeaminase

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dctpdeaminase/dCTPdeaminase__dCTPdeaminase-plddts_91.37723.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_045268677.1'>WP_045268677.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & SECphi4 & SECphi6 & SECphi18 & T2 & T4 & T6 & T7
    Hsueh_2022[<a href='https://doi.org/10.1038/s41564-022-01162-4'>Hsueh et al., 2022</a>] --> Origin_1
    Origin_1[ AvcID
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_096882215.1'>WP_096882215.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T3 & SECphi17 & SECphi18 & SECphi27
    Hsueh_2022[<a href='https://doi.org/10.1038/s41564-022-01162-4'>Hsueh et al., 2022</a>] --> Origin_2
    Origin_2[ AvcID
Proteus mirabilis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_108717204.1'>WP_108717204.1</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T4
    Hsueh_2022[<a href='https://doi.org/10.1038/s41564-022-01162-4'>Hsueh et al., 2022</a>] --> Origin_3
    Origin_3[ AvcID
Vibrio parahaemolyticus 
<a href='https://ncbi.nlm.nih.gov/protein/WP_020839904.1'>WP_020839904.1</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T3 & T5 & T6 & SECphi18
    Hsueh_2022[<a href='https://doi.org/10.1038/s41564-022-01162-4'>Hsueh et al., 2022</a>] --> Origin_4
    Origin_4[ AvcID
Vibrio cholerae 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001901328.1'>WP_001901328.1</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> T2 & T3
    subgraph Title1[Reference]
        Tal_2022
        Hsueh_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
        Origin_4
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
end
    subgraph Title4[Phage infected]
        T5
        SECphi4
        SECphi6
        SECphi18
        T2
        T4
        T6
        T7
        T3
        SECphi17
        SECphi18
        SECphi27
        T4
        T3
        T5
        T6
        SECphi18
        T2
        T3
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


