---
title: Dpd
layout: article
tableColumns:
    article:
      doi: 10.1073/pnas.1518570113
      abstract: |
        The discovery of ?20-kb gene clusters containing a family of paralogs of tRNA guanosine transglycosylase genes, called tgtA5, alongside 7-cyano-7-deazaguanine (preQ0) synthesis and DNA metabolism genes, led to the hypothesis that 7-deazaguanine derivatives are inserted in DNA. This was established by detecting 2’-deoxy-preQ0 and 2’-deoxy-7-amido-7-deazaguanosine in enzymatic hydrolysates of DNA extracted from the pathogenic, Gram-negative bacteria Salmonella enterica serovar Montevideo. These modifications were absent in the closely related S. enterica serovar Typhimurium LT2 and from a mutant of S. Montevideo, each lacking the gene cluster. This led us to rename the genes of the S. Montevideo cluster as dpdA-K for 7-deazapurine in DNA. Similar gene clusters were analyzed in ?150 phylogenetically diverse bacteria, and the modifications were detected in DNA from other organisms containing these clusters, including Kineococcus radiotolerans, Comamonas testosteroni, and Sphingopyxis alaskensis. Comparative genomic analysis shows that, in Enterobacteriaceae, the cluster is a genomic island integrated at the leuX locus, and the phylogenetic analysis of the TgtA5 family is consistent with widespread horizontal gene transfer. Comparison of transformation efficiencies of modified or unmodified plasmids into isogenic S. Montevideo strains containing or lacking the cluster strongly suggests a restriction-modification role for the cluster in Enterobacteriaceae. Another preQ0 derivative, 2’-deoxy-7-formamidino-7-deazaguanosine, was found in the Escherichia coli bacteriophage 9g, as predicted from the presence of homologs of genes involved in the synthesis of the archaeosine tRNA modification. These results illustrate a deep and unexpected evolutionary connection between DNA and tRNA metabolism.
    PFAM: PF00176, PF00270, PF00271, PF01227, PF01242, PF04055, PF04851, PF06508, PF13091, PF13353, PF13394, PF14072
---

# Dpd
## Example of genomic structure

The Dpd system is composed of 15 proteins: FolE, QueD, DpdC, DpdA, DpdB, QueC, DpdD, DpdK, DpdJ, DpdI, DpdH, DpdG, DpdF, DpdE and, QueE.

Here is an example found in the RefSeq database: 

![dpd](/dpd/Dpd.svg){max-width=750px}

Dpd system in the genome of *Thalassotalea crassostreae* (GCF_001831495.1) is composed of 15 proteins: QueE (WP_068546614.1), DpdE (WP_068546526.1), DpdF (WP_068546528.1), DpdG (WP_068546530.1), DpdH (WP_070795901.1), DpdI (WP_068546533.1), DpdJ (WP_068546534.1), DpdK (WP_082897170.1), DpdD (WP_068546535.1), QueC (WP_068546536.1), DpdB (WP_068546537.1), DpdA (WP_068546538.1), DpdC (WP_157726628.1), QueD (WP_068546540.1)and, FolE (WP_068546542.1).

## Distribution of the system among prokaryotes

The Dpd system is present in a total of 100 different species.

Among the 22k complete genomes of RefSeq, this system is present in 226 genomes (1.0 %).

![dpd](/dpd/Distribution_Dpd.svg){max-width=750px}

*Proportion of genome encoding the Dpd system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Dpd

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd.Dpd__DpdF.0.DF-plddts_89.51241.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd.Dpd__DpdF.0.DF-plddts_89.51241.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdA,0,DF-plddts_94.55021.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdB,0,DF-plddts_93.00056.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdC,0,DF-plddts_93.71712.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdD,0,DF-plddts_85.62349.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdE,0,DF-plddts_88.00382.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdG,0,DF-plddts_91.59671.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdH,0,DF-plddts_85.20178.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdI,0,DF-plddts_83.71254.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdJ,0,DF-plddts_89.00672.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dpd/Dpd,Dpd__DpdK,0,DF-plddts_93.96529.pdb
---
::

## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1073/pnas.1518570113

---
::

