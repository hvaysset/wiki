---
title: Pycsar
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2021.09.031
      abstract: |
        The cyclic pyrimidines 3',5'-cyclic cytidine monophosphate (cCMP) and 3',5'-cyclic uridine monophosphate (cUMP) have been reported in multiple organisms and cell types. As opposed to the cyclic nucleotides 3',5'-cyclic adenosine monophosphate (cAMP) and 3',5'-cyclic guanosine monophosphate (cGMP), which are second messenger molecules with well-established regulatory roles across all domains of life, the biological role of cyclic pyrimidines has remained unclear. Here we report that cCMP and cUMP are second messengers functioning in bacterial immunity against viruses. We discovered a family of bacterial pyrimidine cyclase enzymes that specifically synthesize cCMP and cUMP following phage infection and demonstrate that these molecules activate immune effectors that execute an antiviral response. A crystal structure of a uridylate cyclase enzyme from this family explains the molecular mechanism of selectivity for pyrimidines as cyclization substrates. Defense systems encoding pyrimidine cyclases, denoted here Pycsar (pyrimidine cyclase system for antiphage resistance), are widespread in prokaryotes. Our results assign clear biological function to cCMP and cUMP as immunity signaling molecules in bacteria.
    Sensor: Unknown
    Activator: Signaling molecules
    Effector: Membrane disrupting, Nucleotides modifying
    PFAM: PF00004, PF00027, PF00211, PF00899, PF01734, PF10137, PF14461, PF14464, PF18145, PF18153, PF18303, PF18967
---

# Pycsar
## Example of genomic structure

The Pycsar system is composed of 2 proteins: AG_cyclase and, Effector_TIR.

Here is an example found in the RefSeq database: 

![pycsar](/pycsar/Pycsar.svg){max-width=750px}

Pycsar system in the genome of *Staphylococcus aureus* (GCF_003186105.1) is composed of 2 proteins: AG_cyclase (WP_065316016.1)and, 2TM_5 (WP_000073144.1).

## Distribution of the system among prokaryotes

The Pycsar system is present in a total of 276 different species.

Among the 22k complete genomes of RefSeq, this system is present in 559 genomes (2.5 %).

![pycsar](/pycsar/Distribution_Pycsar.svg){max-width=750px}

*Proportion of genome encoding the Pycsar system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Pycsar

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /pycsar/Pycsar,CBASS__TIR,0,V-plddts_90.36719.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pycsar/Pycsar,Pycsar__AG_cyclase,0,V-plddts_94.90778.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /pycsar/Pycsar,CBASS__TIR,0,V-plddts_90.36719.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pycsar/Pycsar,Pycsar__AG_cyclase,0,V-plddts_94.90778.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Tal_2021[<a href='https://doi.org/10.1016/j.cell.2021.09.031'>Tal et al., 2021</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2684943848'>2684943848</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2742823945'>2742823945</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & P1 & LambdaVir & SECphi27
    Tal_2021[<a href='https://doi.org/10.1016/j.cell.2021.09.031'>Tal et al., 2021</a>] --> Origin_1
    Origin_1[Xanthomonas perforans 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2637229924'>2637229924</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2628816548'>2628816548</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Tal_2021
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        T5
        P1
        LambdaVir
        SECphi27
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.cell.2021.09.031

---
::

