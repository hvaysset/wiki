---
title: JukAB
layout: article
tableColumns:
    article:
      doi: 10.1101/2022.09.17.508391
      abstract: |
        Jumbo bacteriophages of the ?KZ-like family are characterized by large genomes (>200 kb) and the remarkable ability to assemble a proteinaceous nucleus-like structure. The nucleus protects the phage genome from canonical DNA-targeting immune systems, such as CRISPR-Cas and restriction-modification. We hypothesized that the failure of common bacterial defenses creates selective pressure for immune systems that target the unique jumbo phage biology. Here, we identify the "jumbo phage killer"(Juk) immune system that is deployed by a clinical isolate of Pseudomonas aeruginosa to resist PhiKZ. Juk immunity rescues the cell by preventing early phage transcription, DNA replication, and nucleus assembly. Phage infection is first sensed by JukA (formerly YaaW), which localizes rapidly to the site of phage infection at the cell pole, triggered by ejected phage factors. The effector protein JukB is recruited by JukA, which is required to enable immunity and the subsequent degradation of the phage DNA. JukA homologs are found in several bacterial phyla and are associated with numerous other putative effectors, many of which provided specific antiPhiKZ activity when expressed in P. aeruginosa. Together, these data reveal a novel strategy for immunity whereby immune factors are recruited to the site of phage protein and DNA ejection to prevent phage progression and save the cell.
    PFAM: PF13099
---

# JukAB

## To do 

## Structure

### JukAB

::molstar-pdbe-plugin
---
height: 700
dataUrl: /jukab/JukAB__JukA-plddts_77.76916.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /jukab/JukAB__JukB-plddts_67.28863.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Li_2022[<a href='https://doi.org/10.1101/2022.09.17.508391'>Li et al., 2022</a>] --> Origin_0
    Origin_0[Pseudomonas aeruginosa 
<a href='https://ncbi.nlm.nih.gov/protein/WP_003137195.1'>WP_003137195.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_003137196.1'>WP_003137196.1</a>] --> Expressed_0[Pseudomonas aeruginosa]
    Expressed_0[Pseudomonas aeruginosa] ----> ΦΚZ
    subgraph Title1[Reference]
        Li_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        ΦΚZ
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1101/2022.09.17.508391

---
::

