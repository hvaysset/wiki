---
title: Rst_3HP
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
---

# Rst_3HP
## Example of genomic structure

The Rst_3HP system is composed of 3 proteins: Hp1, Hp2 and, Hp3.

Here is an example found in the RefSeq database: 

![rst_3hp](/rst_3hp/Rst_3HP.svg){max-width=750px}

Rst_3HP system in the genome of *Klebsiella pneumoniae* (GCF_016403065.1) is composed of 3 proteins: Hp3 (WP_004151009.1), Hp2 (WP_004151008.1)and, Hp1 (WP_004151007.1).

## Distribution of the system among prokaryotes

The Rst_3HP system is present in a total of 83 different species.

Among the 22k complete genomes of RefSeq, this system is present in 420 genomes (1.8 %).

![rst_3hp](/rst_3hp/Distribution_Rst_3HP.svg){max-width=750px}

*Proportion of genome encoding the Rst_3HP system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Rst_3HP

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_3hp/Rst_3HP,Rst_3HP__Hp1,0,V-plddts_92.97739.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_3hp/Rst_3HP,Rst_3HP__Hp2,0,V-plddts_89.72402.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_3hp/Rst_3HP,Rst_3HP__Hp3,0,V-plddts_95.1843.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli P2 loci 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000508501.1'>WP_000508501.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_112026686.1'>WP_112026686.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_000756244.1'>WP_000756244.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        P1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.02.018

---
::
