---
title: AbiU
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF10592
---

# AbiU
## Example of genomic structure

The AbiU system is composed of one protein: AbiU.

Here is an example found in the RefSeq database: 

![abiu](/abiu/AbiU.svg){max-width=750px}

AbiU system in the genome of *Fulvivirga lutea* (GCF_017068455.1) is composed of 1 protein: AbiU (WP_205721428.1).

## Distribution of the system among prokaryotes

The AbiU system is present in a total of 390 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1017 genomes (4.5 %).

![abiu](/abiu/Distribution_AbiU.svg){max-width=750px}

*Proportion of genome encoding the AbiU system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiU

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abiu/AbiU__AbiU-plddts_84.61928.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAG17058.'>AAG17058.</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        936
        c2
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006

---
::

