---
title: FS_Sma
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2022.07.014
      abstract: |
        Bacteria encode sophisticated anti-phage systems that are diverse and versatile and display high genetic mobility. How this variability and mobility occurs remains largely unknown. Here, we demonstrate that a widespread family of pathogenicity islands, the phage-inducible chromosomal islands (PICIs), carry an impressive arsenal of defense mechanisms, which can be disseminated intra- and inter-generically by helper phages. These defense systems provide broad immunity, blocking not only phage reproduction, but also plasmid and non-cognate PICI transfer. Our results demonstrate that phages can mobilize PICI-encoded immunity systems to use them against other mobile genetic elements, which compete with the phages for the same bacterial hosts. Therefore, despite the cost, mobilization of PICIs may be beneficial for phages, PICIs, and bacteria in nature. Our results suggest that PICIs are important players controlling horizontal gene transfer and that PICIs and phages establish mutualistic interactions that drive bacterial ecology and evolution.
    PFAM: PF02452
---

# FS_Sma

## To do 

## Structure

### FS_Sma

::molstar-pdbe-plugin
---
height: 700
dataUrl: /fs_sma/FS_Sma__Sma-plddts_94.14969.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Staphylococcus aureus' PICI SaPIpT1028 
<a href='https://ncbi.nlm.nih.gov/protein/RXS01985.1'>RXS01985.1</a>] --> Expressed_0[Staphylococcus aureus]
    Expressed_0[Staphylococcus aureus] ----> 80alpha & phiNM1 & phiNM2 & phi53 & phi55 & phi6199 & phi7206 & phi6490 & phiSLT & Sushi & phi2339 & phi96 & ROSA & phi11 & phi12 & phi85 & phi7094 & phiSA2
    subgraph Title1[Reference]
        Fillol-Salom_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        80alpha
        phiNM1
        phiNM2
        phi53
        phi55
        phi6199
        phi7206
        phi6490
        phiSLT
        Sushi
        phi2339
        phi96
        ROSA
        phi11
        phi12
        phi85
        phi7094
        phiSA2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1016/j.cell.2022.07.014

---
::

