---
title: Druantia
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aar4120
      abstract: |
        The arms race between bacteria and phages led to the development of sophisticated antiphage defense systems, including CRISPR-Cas and restriction-modification systems. Evidence suggests that known and unknown defense systems are located in "defense islands" in microbial genomes. Here, we comprehensively characterized the bacterial defensive arsenal by examining gene families that are clustered next to known defense genes in prokaryotic genomes. Candidate defense systems were systematically engineered and validated in model bacteria for their antiphage activities. We report nine previously unknown antiphage systems and one antiplasmid system that are widespread in microbes and strongly protect against foreign invaders. These include systems that adopted components of the bacterial flagella and condensin complexes. Our data also suggest a common, ancient ancestry of innate immunity components shared between animals, plants, and bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00145, PF00270, PF00271, PF04851, PF09369, PF14236
---

# Druantia
## Example of genomic structure

The Druantia system have been describe in a total of 3 subsystems.

Here is some example found in the RefSeq database:

![druantia](/druantia/Druantia_I.svg){max-width=750px}

Druantia_I subsystem in the genome of *Escherichia coli* (GCF_002220215.1) is composed of 5 proteins: DruA (WP_000549798.1), DruB (WP_001315973.1), DruC (WP_021520530.1), DruD (WP_000455180.1)and, DruE_1 (WP_089180326.1).

![druantia](/druantia/Druantia_II.svg){max-width=750px}

Druantia_II subsystem in the genome of *Collimonas pratensis* (GCF_001584185.1) is composed of 4 proteins: DruM (WP_082793204.1), DruE_2 (WP_061945149.1), DruG (WP_061945151.1)and, DruF (WP_150119800.1).

![druantia](/druantia/Druantia_III.svg){max-width=750px}

Druantia_III subsystem in the genome of *Acinetobacter baumannii* (GCF_012935125.1) is composed of 2 proteins: DruH (WP_005120035.1)and, DruE_3 (WP_002036795.1).

## Distribution of the system among prokaryotes

The Druantia system is present in a total of 284 different species.

Among the 22k complete genomes of RefSeq, this system is present in 827 genomes (3.6 %).

![druantia](/druantia/Distribution_Druantia.svg){max-width=750px}

*Proportion of genome encoding the Druantia system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Druantia_I

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_I,Druantia_I__DruA,0,V-plddts_88.89293.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_I,Druantia_I__DruB,0,V-plddts_82.83057.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_I,Druantia_I__DruC,0,V-plddts_85.18836.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_I,Druantia_I__DruD,0,V-plddts_91.26112.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_I,Druantia__DruE,0,V-plddts_86.11628.pdb
---
::

### Druantia_II

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_II,Druantia_II__DruF,0,DF-plddts_86.11001.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_II,Druantia_II__DruG,0,DF-plddts_80.40261.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_II,Druantia_II__DruM,0,DF-plddts_90.00131.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_II,Druantia__DruE_2,0,DF-plddts_84.77074.pdb
---
::

### Druantia_III

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_III,Druantia_III__DruH,0,DF-plddts_82.87318.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /druantia/Druantia_III,Druantia__DruE_3,0,DF-plddts_83.26091.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Doron_2018[<a href='https://doi.org/10.1126/science.aar4120'>Doron et al., 2018</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/ERA40829.1'>ERA40829.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ERA40830.1'>ERA40830.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ERA40831.1'>ERA40831.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ERA40832.1'>ERA40832.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ERA40833.1'>ERA40833.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & P1 & Lambda & T3 & T7 & PhiV-1 & Lambdavir & SECphi18 & SECphi27
    subgraph Title1[Reference]
        Gao_2020
        Doron_2018
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_0
end
    subgraph Title4[Phage infected]
        T2
        T4
        T5
        P1
        Lambda
        T3
        T7
        PhiV-1
        Lambdavir
        SECphi18
        SECphi27
        T2
        T4
        T5
        P1
        Lambda
        T3
        T7
        PhiV-1
        Lambdavir
        SECphi18
        SECphi27
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aar4120

---
::

