---
title: DmdDE
layout: article
tableColumns:
    article:
      doi: 10.1038/s41586-022-04546-y
      abstract: |
        Horizontal gene transfer can trigger rapid shifts in bacterial evolution. Driven by a variety of mobile genetic elements—in particular bacteriophages and plasmids—the ability to share genes within and across species underpins the exceptional adaptability of bacteria. Nevertheless, invasive mobile genetic elements can also present grave risks to the host; bacteria have therefore evolved a vast array of defences against these elements1. Here we identify two plasmid defence systems conserved in the Vibrio cholerae El Tor strains responsible for the ongoing seventh cholera pandemic2-4. These systems, termed DdmABC and DdmDE, are encoded on two major pathogenicity islands that are a hallmark of current pandemic strains. We show that the modules cooperate to rapidly eliminate small multicopy plasmids by degradation. Moreover, the DdmABC system is widespread and can defend against bacteriophage infection by triggering cell suicide (abortive infection, or Abi). Notably, we go on to show that, through an Abi-like mechanism, DdmABC increases the burden of large low-copy-number conjugative plasmids, including a broad-host IncC multidrug resistance plasmid, which creates a fitness disadvantage that counterselects against plasmid-carrying cells. Our results answer the long-standing question of why plasmids, although abundant in environmental strains, are rare in pandemic strains; have implications for understanding the dissemination of antibiotic resistance plasmids; and provide insights into how the interplay between two defence systems has shaped the evolution of the most successful lineage of pandemic V. cholerae.
---

# DmdDE
## Example of genomic structure

The DmdDE system is composed of 2 proteins: DdmE and, DdmD.

Here is an example found in the RefSeq database: 

![dmdde](/dmdde/DdmDE.svg){max-width=750px}

DdmDE subsystem in the genome of *Vibrio vulnificus* (GCF_002850455.1) is composed of 2 proteins: DdmE (WP_101957190.1)and, DdmD (WP_101957191.1).

## Distribution of the system among prokaryotes

The DmdDE system is present in a total of 50 different species.

Among the 22k complete genomes of RefSeq, this system is present in 145 genomes (0.6 %).

![dmdde](/dmdde/Distribution_DmdDE.svg){max-width=750px}

*Proportion of genome encoding the DmdDE system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### DdmDE

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dmdde/DdmDE,DdmDE__DdmD,0,V-plddts_86.22213.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dmdde/DdmDE,DdmDE__DdmE,0,V-plddts_90.70804.pdb
---
::

## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41586-022-04546-y

---
::
