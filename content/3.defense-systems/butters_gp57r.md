---
title: Butters_gp57r
layout: article
tableColumns:
    article:
      doi: 10.1101/2023.01.03.522681
      abstract: |
        During lysogeny temperate phages establish a truce with the bacterial host. In this state, the phage genome (prophage) is maintained within the host environment. Consequently, many prophages have evolved systems to protect the host from heterotypic viral attack. This phenomenon of prophages mediating defense of their host against competitor phages is widespread among temperate mycobacteriophages. We previously showed that the Mycobacterium phage Butters prophage encodes a two-component system (gp30/31) that inhibits infection from a subset of mycobacteriophages that include PurpleHaze, but not Island3. Here we show that Butters gp57r is both necessary and sufficient to inhibit infection by Island3 and other phages. Gp57r acts post-DNA injection and its antagonism results in the impairment of Island3 DNA amplification. Gp57r inhibition of Island3 is absolute with no defense escape mutants. However, mutations mapping to minor tail proteins allow PurpleHaze to overcome gp57r defense. Gp57r has a HEPN domain which is present in many proteins involved in inter-genomic conflicts, suggesting that gp57r may inhibit heterotypic phage infections via its HEPN domain. We also show that Butters gp57r has orthologues in clinical isolates of Mycobacterium abscessus sp. including the phage therapy candidate strain GD91 which was found to be resistant to the panel of phages tested. It is conceivable that this GD91 orthologue of gp57r may mediate resistance to the subset of phages tested. Challenges of this nature underscore the importance of elucidating mechanisms of antiphage systems and mutations that allow for escape from inhibition. IMPORTANCE The evolutionary arms race between phages and their bacteria host is ancient. During lysogeny, temperate phages establish a ceasefire with the host where they do not kill the host but derive shelter from it. Within the phenomenon of prophage-mediated defense, some temperate phages contribute genes that make their host more fit and resistant to infections by other phages. This arrangement has significance for both phage and bacterial evolutionary dynamics. Further, the prevalence of such antiphage systems poses a challenge to phage therapy. Thus, studies aimed at elucidating antiphage systems will further our understanding of phage-bacteria evolution as well as help with efforts to engineer therapeutic phages that circumvent antiphage systems.
---

# Butters_gp57r

## To do 

## Structure

### Butters_gp57r

::molstar-pdbe-plugin
---
height: 700
dataUrl: /butters_gp57r/Butters_gp57r__gp57r-plddts_90.7432.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Mohammed_2023[<a href='https://doi.org/10.1101/2023.01.03.522681'>Mohammed et al., 2023</a>] --> Origin_0
    Origin_0[Mycobacterium phage Butters 
<a href='https://ncbi.nlm.nih.gov/protein/YP_009304936.1'>YP_009304936.1</a>] --> Expressed_0[Mycobacterium smegmatis mc2 155]
    Expressed_0[Mycobacterium smegmatis mc2 155] ----> Island3 & Brujita & Gyzlar & LHTSCC & Norz & PurpleHaze & Perplexer & Bud
    subgraph Title1[Reference]
        Mohammed_2023
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Island3
        Brujita
        Gyzlar
        LHTSCC
        Norz
        PurpleHaze
        Perplexer
        Bud
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1101/2023.01.03.522681

---
::
