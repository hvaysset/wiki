---
title: ISG15-like
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
---

# ISG15-like
## Example of genomic structure

The ISG15-like system is composed of 4 proteins: BilD, BilC, BilB and, BilA.

Here is an example found in the RefSeq database: 

![isg15-like](/isg15-like/ISG15-like.svg){max-width=750px}

ISG15-like system in the genome of *Rhizobium phaseoli* (GCF_001664285.1) is composed of 4 proteins: BilA (WP_064823699.1), BilB (WP_150124924.1), BilC (WP_150124925.1)and, BilD (WP_190304495.1).

## Distribution of the system among prokaryotes

The ISG15-like system is present in a total of 28 different species.

Among the 22k complete genomes of RefSeq, this system is present in 43 genomes (0.2 %).

![isg15-like](/isg15-like/Distribution_ISG15-like.svg){max-width=750px}

*Proportion of genome encoding the ISG15-like system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### ISG15-like

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilA,0,V-plddts_93.65636.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilB,0,V-plddts_95.49136.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilC,0,V-plddts_92.06734.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilD,0,V-plddts_93.10226.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilA,0,V-plddts_93.65636.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilB,0,V-plddts_95.49136.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilC,0,V-plddts_92.06734.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilD,0,V-plddts_93.10226.pdb
---
::

Example 3:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilA,0,V-plddts_93.65636.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilB,0,V-plddts_95.49136.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilC,0,V-plddts_92.06734.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilD,0,V-plddts_93.10226.pdb
---
::

Example 4:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilA,0,V-plddts_93.65636.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilB,0,V-plddts_95.49136.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilC,0,V-plddts_92.06734.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilD,0,V-plddts_93.10226.pdb
---
::

Example 5:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilA,0,V-plddts_93.65636.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilB,0,V-plddts_95.49136.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilC,0,V-plddts_92.06734.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /isg15-like/ISG15-like,ISG15-like__BilD,0,V-plddts_93.10226.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Collimonas sp. OK412 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609810443'>2609810443</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609810442'>2609810442</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609810441'>2609810441</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2609810440'>2609810440</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T7 & SECphi17
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_1
    Origin_1[Caulobacter sp. Root343 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2644236682'>2644236682</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2644236681'>2644236681</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2644236680'>2644236680</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2644236679'>2644236679</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T4 & T6 & T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T7 & SECphi17
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_2
    Origin_2[Cupriavidus sp. SHE 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2654619719'>2654619719</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2654619720'>2654619720</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2654619721'>2654619721</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2654619722'>2654619722</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T2 & T4 & T6 & T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_3
    Origin_3[Paraburkholderia caffeinilytica 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2843959492'>2843959492</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2843959493'>2843959493</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2843959494'>2843959494</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2843959495'>2843959495</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T6 & SECphi27
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_4
    Origin_4[Thiomonas sp. FB-6 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2523655165'>2523655165</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2523655166'>2523655166</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2523655167'>2523655167</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2523655168'>2523655168</a>] --> Expressed_4[Escherichia coli]
    Expressed_4[Escherichia coli] ----> SECphi27
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
        Origin_4
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
        Expressed_4
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T7
        SECphi17
        T4
        T6
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T7
        SECphi17
        T2
        T4
        T6
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T6
        SECphi27
        SECphi27
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017

---
::
