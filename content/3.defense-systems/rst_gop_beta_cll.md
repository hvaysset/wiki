---
title: Rst_gop_beta_cll
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.02.018
      abstract: |
        Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF14350
---

# Rst_gop_beta_cll
## Example of genomic structure

The Rst_gop_beta_cll system is composed of 3 proteins: gop, beta and, cll.

Here is an example found in the RefSeq database: 

![rst_gop_beta_cll](/rst_gop_beta_cll/Rst_gop_beta_cll.svg){max-width=750px}

Rst_gop_beta_cll system in the genome of *Escherichia coli* (GCF_003018615.1) is composed of 3 proteins: cll (WP_001357997.1), beta (WP_001357996.1)and, gop (WP_000931915.1).

## Distribution of the system among prokaryotes

The Rst_gop_beta_cll system is present in a total of 14 different species.

Among the 22k complete genomes of RefSeq, this system is present in 37 genomes (0.2 %).

![rst_gop_beta_cll](/rst_gop_beta_cll/Distribution_Rst_gop_beta_cll.svg){max-width=750px}

*Proportion of genome encoding the Rst_gop_beta_cll system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Rst_gop_beta_cll

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_gop_beta_cll/Rst_gop_beta_cll,Rst_gop_beta_cll__beta,0,V-plddts_93.90323.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_gop_beta_cll/Rst_gop_beta_cll,Rst_gop_beta_cll__cll,0,V-plddts_87.60729.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rst_gop_beta_cll/Rst_gop_beta_cll,Rst_gop_beta_cll__gop,0,V-plddts_74.94876.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Enterobacteria phage P4 
<a href='https://ncbi.nlm.nih.gov/protein/WP_105493229.1'>WP_105493229.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ENG39111.1'>ENG39111.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/NP_042032.1'>NP_042032.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & P1
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Lambda
        P1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.02.018

---
::

