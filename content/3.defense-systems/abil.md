---
title: AbiL
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13175, PF13304, PF13707
---

# AbiL
## Example of genomic structure

The AbiL system is composed of 2 proteins: AbiLii2 and, AbiLi2.

Here is an example found in the RefSeq database: 

![abil](/abil/AbiL.svg){max-width=750px}

AbiL system in the genome of *Fusobacterium nucleatum* (GCF_003019785.1) is composed of 2 proteins: AbiLii2 (WP_005903821.1)and, AbiLi2 (WP_005903823.1).

## Distribution of the system among prokaryotes

The AbiL system is present in a total of 456 different species.

Among the 22k complete genomes of RefSeq, this system is present in 783 genomes (3.4 %).

![abil](/abil/Distribution_AbiL.svg){max-width=750px}

*Proportion of genome encoding the AbiL system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiL

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abil/AbiL,AbiL__AbiLi,0,V-plddts_89.17612.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abil/AbiL,AbiL__AbiLii,0,V-plddts_86.69766.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAB53710.1'>AAB53710.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AAB53711.1'>AAB53711.1</a>] --> Expressed_0[lactococci ]
    Expressed_0[lactococci ] ----> 936 & c2
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        936
        c2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006

---
::

