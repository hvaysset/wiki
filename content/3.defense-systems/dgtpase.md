---
title: dGTPase
layout: article
tableColumns:
    article:
      doi: 10.1016/j.cell.2021.09.031
      abstract: |
        The cyclic pyrimidines 3',5'-cyclic cytidine monophosphate (cCMP) and 3',5'-cyclic uridine monophosphate (cUMP) have been reported in multiple organisms and cell types. As opposed to the cyclic nucleotides 3',5'-cyclic adenosine monophosphate (cAMP) and 3',5'-cyclic guanosine monophosphate (cGMP), which are second messenger molecules with well-established regulatory roles across all domains of life, the biological role of cyclic pyrimidines has remained unclear. Here we report that cCMP and cUMP are second messengers functioning in bacterial immunity against viruses. We discovered a family of bacterial pyrimidine cyclase enzymes that specifically synthesize cCMP and cUMP following phage infection and demonstrate that these molecules activate immune effectors that execute an antiviral response. A crystal structure of a uridylate cyclase enzyme from this family explains the molecular mechanism of selectivity for pyrimidines as cyclization substrates. Defense systems encoding pyrimidine cyclases, denoted here Pycsar (pyrimidine cyclase system for antiphage resistance), are widespread in prokaryotes. Our results assign clear biological function to cCMP and cUMP as immunity signaling molecules in bacteria.
    Sensor: Monitoring of the host cell machinery integrity
    Activator: Direc
    Effector: Nucleotide modifying
    PFAM: PF01966, PF13286
---

# dGTPase
## Example of genomic structure

The dGTPase system is composed of one protein: Sp_dGTPase.

Here is an example found in the RefSeq database: 

![dGTPase](/dGTPase.svg){max-width=750px}

dGTPase system in the genome of *Acinetobacter pittii* (GCF_002012285.1) is composed of 1 protein: Sp_dGTPase (WP_213033921.1).

## Distribution of the system among prokaryotes

The dGTPase system is present in a total of 353 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1532 genomes (6.7 %).

![Distribution_dGTPase](/Distribution_dGTPase.svg){max-width=750px}

*Proportion of genome encoding the dGTPase system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### dGTPase

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dgtpase/dGTPase__Sp_dGTPase-plddts_94.35719.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2704680458'>2704680458</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T7
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_1
    Origin_1[Mesorhizobium ssp. 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2522901616'>2522901616</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> SECphi4 & SECphi6 & SECphi18 & SECphi27 & T7
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_2
    Origin_2[Pseudoalteromonas luteoviolacea 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2731093246'>2731093246</a>] --> Expressed_2[Escherichia coli]
    Expressed_2[Escherichia coli] ----> T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T2
    Tal_2022[<a href='https://doi.org/10.1038/s41564-022-01158-0'>Tal et al., 2022</a>] --> Origin_3
    Origin_3[Shewanella putrefaciens 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2524134135'>2524134135</a>] --> Expressed_3[Escherichia coli]
    Expressed_3[Escherichia coli] ----> T5 & SECphi4 & SECphi6 & SECphi18 & SECphi27 & T2 & T6 & T7 & SECphi17
    subgraph Title1[Reference]
        Tal_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Phage infected]
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T7
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T7
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T2
        T5
        SECphi4
        SECphi6
        SECphi18
        SECphi27
        T2
        T6
        T7
        SECphi17
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41564-022-01162-4

---
::

