---
title: Old_exonuclease
layout: article
tableColumns:
    article:
      doi: 10.1128/jb.177.3.497-501.1995
      abstract: |
        The Old protein of bacteriophage P2 is responsible for interference with the growth of phage lambda and for killing of recBC mutant Escherichia coli. We have purified Old fused to the maltose-binding protein to 95% purity and characterized its enzymatic properties. The Old protein fused to maltose-binding protein has exonuclease activity on double-stranded DNA as well as nuclease activity on single-stranded DNA and RNA. The direction of digestion of double-stranded DNA is from 5' to 3', and digestion initiates at either the 5'-phosphoryl or 5'-hydroxyl terminus. The nuclease is active on nicked circular DNA, degrades DNA in a processive manner, and releases 5'-phosphoryl mononucleotides.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF13175, PF13304
---

# Old_exonuclease
## Example of genomic structure

The Old_exonuclease system is composed of one protein: Old_exonuclease.

Here is an example found in the RefSeq database: 

![old_exonuclease](/old_exonuclease/Old_exonuclease.svg){max-width=750px}

Old_exonuclease system in the genome of *Escherichia coli* (GCF_016904335.1) is composed of 1 protein: Old_exonuclease (WP_015979595.1).

## Distribution of the system among prokaryotes

The Old_exonuclease system is present in a total of 53 different species.

Among the 22k complete genomes of RefSeq, this system is present in 102 genomes (0.4 %).

![old_exonuclease](/old_exonuclease/Distribution_Old_exonuclease.svg){max-width=750px}

*Proportion of genome encoding the Old_exonuclease system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Old_exonuclease

::molstar-pdbe-plugin
---
height: 700
dataUrl: /old_exonuclease/Old_exonuclease__Old_exonuclease-plddts_88.62156.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Rousset_2022[<a href='https://doi.org/10.1016/j.chom.2022.02.018'>Rousset et al., 2022</a>] --> Origin_0
    Origin_0[Enterobacteria phage P2 
<a href='https://ncbi.nlm.nih.gov/protein/NP_046798.1'>NP_046798.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T4 & LF82_P8 & Al505_P2
    subgraph Title1[Reference]
        Rousset_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Lambda
        T4
        LF82_P8
        Al505_P2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

**Rousset, F. et al. Phages and their satellites encode hotspots of antiviral systems. Cell Host & Microbe 30, 740-753.e5 (2022).**
Bacteria carry diverse genetic systems to defend against viral infection, some of which are found within prophages where they inhibit competing viruses. Phage satellites pose additional pressures on phages by hijacking key viral elements to their own benefit. Here, we show that E. coli P2-like phages and their parasitic P4-like satellites carry hotspots of genetic variation containing reservoirs of anti-phage systems. We validate the activity of diverse systems and describe PARIS, an abortive infection system triggered by a phage-encoded anti-restriction protein. Antiviral hotspots participate in inter-viral competition and shape dynamics between the bacterial host, P2-like phages, and P4-like satellites. Notably, the anti-phage activity of satellites can benefit the helper phage during competition with virulent phages, turning a parasitic relationship into a mutualistic one. Anti-phage hotspots are present across distant species and constitute a substantial source of systems that participate in the competition between mobile genetic elements.


