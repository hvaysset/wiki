---
title: MazEF
layout: article
tableColumns:
    article:
      doi: 10.1007/s00438-004-1048-y
      abstract: |
        The Escherichia coli gene pair mazEF is a regulatable chromosomal toxin-antitoxin module: mazF encodes a stable toxin and mazE encodes for a labile antitoxin that overcomes the lethal effect of MazF. Because MazE is labile, inhibition of mazE expression results in cell death. We studied the effect of mazEF on the development of bacteriophage P1 upon thermoinduction of the prophage P1CM c1ts and upon infection with virulent phage particles (P1vir). In several E. coli strains, we showed that the ?mazEF derivative strains produced significantly more phages than did the parent strain. In addition, upon induction of K38(P1CM c1ts), nearly all of the ?mazEF mutant cells lysed; in contrast, very few of the parental mazEF + K38 cells underwent lysis. However, most of these cells did not remain viable. Thus, while the ?mazEF cells die as a result of the lytic action of the phage, most of the mazEF + cells are killed by a different mechanism, apparently through the action of the chromosomal mazEF system itself. Furthermore, the introduction of lysogens into a growing non-lysogenic culture is lethal to ?mazEF but not for mazEF + cultures. Thus, although mazEF action causes individual cells to die, upon phage growth this is generally beneficial to the bacterial culture because it causes P1 phage exclusion from the bacterial population. These results provide additional support for the view that bacterial cultures may share some of the characteristics of multicellular organisms.
    PFAM: PF02452, PF04014
---

# MazEF

## To do 

## Experimental validation
<mermaid>
graph LR;
    Maestri_2023[<a href='https://doi.org/10.1101/2023.03.30.534895'>Maestri et al., 2023</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000581937.1'>WP_000581937.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000254738.1'>WP_000254738.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> P1
    subgraph Title1[Reference]
        Maestri_2023
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        P1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstract
::relevant-abstracts
---
items:
    - doi: 10.1007/s00438-004-1048-y

---
::

