---
title: Azaca
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00271
---

# Azaca
## Example of genomic structure

The Azaca system is composed of 3 proteins: ZacA, ZacB and, ZacC.

Here is an example found in the RefSeq database: 

![azaca](/azaca/Azaca.svg){max-width=750px}

Azaca system in the genome of *Ornithinimicrobium sp.* (GCF_023923205.1) is composed of 3 proteins: ZacA (WP_252620090.1), ZacB (WP_252620091.1)and, ZacC (WP_252620092.1).

## Distribution of the system among prokaryotes

The Azaca system is present in a total of 156 different species.

Among the 22k complete genomes of RefSeq, this system is present in 206 genomes (0.9 %).

![azaca](/azaca/Distribution_Azaca.svg){max-width=750px}

*Proportion of genome encoding the Azaca system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Azaca

::molstar-pdbe-plugin
---
height: 700
dataUrl: /azaca/Azaca,Azaca__ZacA,0,V-plddts_85.13072.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /azaca/Azaca,Azaca__ZacB,0,V-plddts_87.34712.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /azaca/Azaca,Azaca__ZacC,0,V-plddts_86.69875.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus massilioanorexius 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2547646535'>2547646535</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2547646536'>2547646536</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2547646537'>2547646537</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus massilioanorexius 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2547646535'>2547646535</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2547646536'>2547646536</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2547646537'>2547646537</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> SBSphiC
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
        SBSphiC
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2022.09.017

---
::

