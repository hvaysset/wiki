---
title: PD-T4-9
layout: article
tableColumns:
    article:
      doi: 10.1038/s41564-022-01219-4
      abstract: |
        The ancient, ongoing coevolutionary battle between bacteria and their viruses, bacteriophages, has given rise to sophisticated immune systems including restriction-modification and CRISPR-Cas. Many additional anti-phage systems have been identified using computational approaches based on genomic co-location within defence islands, but these screens may not be exhaustive. Here we developed an experimental selection scheme agnostic to genomic context to identify defence systems in 71 diverse E. coli strains. Our results unveil 21 conserved defence systems, none of which were previously detected as enriched in defence islands. Additionally, our work indicates that intact prophages and mobile genetic elements are primary reservoirs and distributors of defence systems in E. coli, with defence systems typically carried in specific locations or hotspots. These hotspots encode dozens of additional uncharacterized defence system candidates. Our findings reveal an extended landscape of antiviral immunity in E. coli and provide an approach for mapping defence systems in other species.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF02556
---

# PD-T4-9
## Example of genomic structure

The PD-T4-9 system is composed of 3 proteins: PD-T4-9_C, PD-T4-9_B and, PD-T4-9_A.

Here is an example found in the RefSeq database: 

![pd-t4-9](/pd-t4-9/PD-T4-9.svg){max-width=750px}

PD-T4-9 system in the genome of *Escherichia coli* (GCF_018223665.1) is composed of 3 proteins: PD-T4-9_A (WP_061089969.1), PD-T4-9_B (WP_061089970.1)and, PD-T4-9_C (WP_061089971.1).

## Distribution of the system among prokaryotes

The PD-T4-9 system is present in a total of 93 different species.

Among the 22k complete genomes of RefSeq, this system is present in 148 genomes (0.6 %).

![pd-t4-9](/pd-t4-9/Distribution_PD-T4-9.svg){max-width=750px}

*Proportion of genome encoding the PD-T4-9 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### PD-T4-9

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-t4-9/PD-T4-9,PD-T4-9__PD-T4-9_A,0,V-plddts_93.71807.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-t4-9/PD-T4-9,PD-T4-9__PD-T4-9_B,0,V-plddts_71.02357.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pd-t4-9/PD-T4-9,PD-T4-9__PD-T4-9_C,0,V-plddts_93.05735.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Vassallo_2022[<a href='https://doi.org/10.1038/s41564-022-01219-4'>Vassallo et al., 2022</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/RCP66309.1'>RCP66309.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/RCP66310.1'>RCP66310.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/RCP66311.1'>RCP66311.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6
    subgraph Title1[Reference]
        Vassallo_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41564-022-01219-4

---
::

