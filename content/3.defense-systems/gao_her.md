---
title: Gao_Her
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF01935, PF10412, PF13289
---

# Gao_Her
## Example of genomic structure

The Gao_Her system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

![gao_her](/gao_her/Gao_Her_DUF.svg){max-width=750px}

Gao_Her_DUF subsystem in the genome of *Enterobacter roggenkampii* (GCF_014524505.1) is composed of 2 proteins: DUF4297 (WP_188074283.1)and, HerA_DUF (WP_063614829.1).

![gao_her](/gao_her/Gao_Her_SIR.svg){max-width=750px}

Gao_Her_SIR subsystem in the genome of *Escherichia coli* (GCF_012221565.1) is composed of 2 proteins: SIR2 (WP_167839366.1)and, HerA_SIR2 (WP_021577682.1).

## Distribution of the system among prokaryotes

The Gao_Her system is present in a total of 127 different species.

Among the 22k complete genomes of RefSeq, this system is present in 233 genomes (1.0 %).

![gao_her](/gao_her/Distribution_Gao_Her.svg){max-width=750px}

*Proportion of genome encoding the Gao_Her system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Gao_Her_DUF

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_her/Gao_Her_DUF,Gao_Her_DUF__DUF4297,0,V-plddts_91.87572.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_her/Gao_Her_DUF,Gao_Her_DUF__HerA_DUF,0,V-plddts_88.91697.pdb
---
::

### Gao_Her_SIR

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_her/Gao_Her_SIR,Gao_Her_SIR__HerA_SIR2,0,V-plddts_86.52691.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_her/Gao_Her_SIR,Gao_Her_SIR__SIR2,0,V-plddts_87.76893.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[ SIR2 + HerA
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_021577682.1'>WP_021577682.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_021577683.1'>WP_021577683.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T3 & T7 & PhiV-1
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_1
    Origin_1[ DUF4297 + HerA
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_016239655.1'>WP_016239655.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_016239654.1'>WP_016239654.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> T4 & P1 & Lambda & T3 & T7
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        Lambda
        T3
        T7
        PhiV-1
        T4
        P1
        Lambda
        T3
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aba0372

---
::

