---
title: Gao_Mza
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00023, PF04542, PF04545, PF10592, PF10593, PF13589, PF13606, PF14390
---

# Gao_Mza
## Example of genomic structure

The Gao_Mza system is composed of 5 proteins: MzaB, MzaC, MzaA, MzaD and, MzaE.

Here is an example found in the RefSeq database: 

![gao_mza](/gao_mza/Gao_Mza.svg){max-width=750px}

Gao_Mza system in the genome of *Enterobacter roggenkampii* (GCF_023023065.1) is composed of 5 proteins: MzaE (WP_045418899.1), MzaD (WP_045418897.1), MzaC (WP_025912266.1), MzaB (WP_045418895.1)and, MzaA (WP_045418893.1).

## Distribution of the system among prokaryotes

The Gao_Mza system is present in a total of 57 different species.

Among the 22k complete genomes of RefSeq, this system is present in 99 genomes (0.4 %).

![gao_mza](/gao_mza/Distribution_Gao_Mza.svg){max-width=750px}

*Proportion of genome encoding the Gao_Mza system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Gao_Mza

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_mza/Gao_Mza,Gao_Mza__MzaA,0,V-plddts_73.82237.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_mza/Gao_Mza,Gao_Mza__MzaB,0,V-plddts_88.82333.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_mza/Gao_Mza,Gao_Mza__MzaC,0,V-plddts_83.55691.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_mza/Gao_Mza,Gao_Mza__MzaD,0,V-plddts_88.07953.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_mza/Gao_Mza,Gao_Mza__MzaE,0,V-plddts_87.92462.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Salmonella enterica 
<a href='https://ncbi.nlm.nih.gov/protein/VEA06816.1'>VEA06816.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/VEA06814.1'>VEA06814.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/VEA06812.1'>VEA06812.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/VEA06810.1'>VEA06810.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/VEA06808.1'>VEA06808.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T5 & Lambda & M13
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T2
        T4
        T5
        Lambda
        M13
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aba0372

---
::

