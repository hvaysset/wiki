---
title: AbiH
layout: article
tableColumns:
    article:
      doi: 10.1111/j.1574-6968.1996.tb08446.x
      abstract: |
        A gene which encodes resistance by abortive infection (Abi+) to bacteriophage was cloned from Lactococcus lactis ssp. lactis biovar. diacetylactis S94. This gene was found to confer a reduction in efficiency of plating and plaque size for prolate-headed bacteriophage phi 53 (group I of homology) and total resistance to the small isometric-headed bacteriophage phi 59 (group III of homology). The cloned gene is predicted to encode a polypeptide of 346 amino acid residues with a deduced molecular mass of 41 455 Da. No homology with any previously described genes was found. A probe was used to determine the presence of this gene in two strains on 31 tested.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF14253
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1111/j.1574-6968.1996.tb08446.x

---

# AbiH
## Example of genomic structure

The AbiH system is composed of one protein: AbiH.

Here is an example found in the RefSeq database: 

![abih](/abih/AbiH.svg){max-width=750px}

AbiH system in the genome of *Agrobacterium tumefaciens* (GCF_005221405.1) is composed of 1 protein: AbiH (WP_045021548.1).

## Distribution of the system among prokaryotes

The AbiH system is present in a total of 408 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1277 genomes (5.6 %).

![abih](/abih/Distribution_AbiH.svg){max-width=750px}

*Proportion of genome encoding the AbiH system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiH

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abih/AbiH__AbiH-plddts_91.3485.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococci 
<a href='https://ncbi.nlm.nih.gov/protein/CAA66252.1'>CAA66252.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        936
        c2
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>



