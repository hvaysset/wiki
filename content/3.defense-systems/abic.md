---
title: AbiC
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF16872
---

# AbiC
The AbiC system is composed of one protein: AbiC.

Here is an example found in the RefSeq database: 

![abic](/abic/AbiC.svg){max-width=750px}

AbiC system in the genome of *Enterococcus faecium* (GCF_012933295.2) is composed of 1 protein: AbiC (WP_098388098.1).

## Distribution of the system among prokaryotes

The AbiC system is present in a total of 110 different species.

Among the 22k complete genomes of RefSeq, this system is present in 196 genomes (0.9 %).

![abic](/abic/Distribution_AbiC.svg){max-width="750"}

*Proportion of genome encoding the AbiC system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiC

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abic/AbiC__AbiC-plddts_83.80335.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae's  PICI KpCIFDAARGOS_1313 
<a href='https://ncbi.nlm.nih.gov/protein/QRS09118.1'>QRS09118.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & Lambda & HK97 & HK544 & HK578 & T7
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae's  PICI KpCIFDAARGOS_1313 
<a href='https://ncbi.nlm.nih.gov/protein/QRS09118.1'>QRS09118.1</a>] --> Expressed_1[Salmonella enterica]
    Expressed_1[Salmonella enterica] ----> P22 & BTP1 & ES18
    Fillol-Salom_2022[<a href='https://doi.org/10.1016/j.cell.2022.07.014'>Fillol-Salom et al., 2022</a>] --> Origin_0
    Origin_0[Klebsiella pneumoniae's  PICI KpCIFDAARGOS_1313 
<a href='https://ncbi.nlm.nih.gov/protein/QRS09118.1'>QRS09118.1</a>] --> Expressed_2[Klebsiella pneumoniae]
    Expressed_2[Klebsiella pneumoniae] ----> Pokey & Raw & Eggy
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_1
    Origin_1[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAA53569.1'>AAA53569.1</a>] --> Expressed_3[lactococci]
    Expressed_3[lactococci] ----> 936 & P335
    subgraph Title1[Reference]
        Fillol-Salom_2022
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Phage infected]
        T5
        Lambda
        HK97
        HK544
        HK578
        T7
        P22
        BTP1
        ES18
        Pokey
        Raw
        Eggy
        936
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006

---
::

