---
title: Gao_Tmn
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
---

# Gao_Tmn
## Example of genomic structure

The Gao_Tmn system is composed of one protein: TmnA.

Here is an example found in the RefSeq database: 

![gao_tmn](/gao_tmn/Gao_Tmn.svg){max-width=750px}

Gao_Tmn system in the genome of *Salmonella enterica* (GCF_006384195.1) is composed of 1 protein: TmnA (WP_130525902.1).

## Distribution of the system among prokaryotes

The Gao_Tmn system is present in a total of 82 different species.

Among the 22k complete genomes of RefSeq, this system is present in 414 genomes (1.8 %).

![gao_tmn](/gao_tmn/Distribution_Gao_Tmn.svg){max-width=750px}

*Proportion of genome encoding the Gao_Tmn system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Gao_Tmn

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_tmn/Gao_Tmn__TmnA-plddts_82.0142.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001683567.1'>WP_001683567.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & P1 & PhiV-1 & PhiX
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T2
        P1
        PhiV-1
        PhiX
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aba0372

---
::
