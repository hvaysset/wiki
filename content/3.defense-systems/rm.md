---
title: RM
layout: article
tableColumns:
    article:
      doi: 10.1093/nar/gku734
      abstract: |
        The roles of restriction-modification (R-M) systems in providing immunity against horizontal gene transfer (HGT) and in stabilizing mobile genetic elements (MGEs) have been much debated. However, few studies have precisely addressed the distribution of these systems in light of HGT, its mechanisms and its vectors. We analyzed the distribution of R-M systems in 2261 prokaryote genomes and found their frequency to be strongly dependent on the presence of MGEs, CRISPR-Cas systems, integrons and natural transformation. Yet R-M systems are rare in plasmids, in prophages and nearly absent from other phages. Their abundance depends on genome size for small genomes where it relates with HGT but saturates at two occurrences per genome. Chromosomal R-M systems might evolve under cycles of purifying and relaxed selection, where sequence conservation depends on the biochemical activity and complexity of the system and total gene loss is frequent. Surprisingly, analysis of 43 pan-genomes suggests that solitary R-M genes rarely arise from the degradation of R-M systems. Solitary genes are transferred by large MGEs, whereas complete systems are more frequently transferred autonomously or in small MGEs. Our results suggest means of testing the roles for R-M systems and their associations with MGEs.
    Sensor: Detecting invading nucleic acid
    Activator: Direct
    Effector: Nucleic acid degrading
    PFAM: PF00270, PF02384, PF04313, PF04851, PF12008, PF12161, PF18766
---

# RM
## Example of genomic structure

The RM system have been describe in a total of 5 subsystems.

Here is some example found in the RefSeq database:

![rm](/rm/RM_Type_I.svg){max-width=750px}

RM_Type_I subsystem in the genome of *Aeromonas veronii* (GCF_014169835.1) is composed of 4 proteins: Type_I_MTases (WP_182963881.1), Type_I_MTases (WP_182963881.1), Type_I_S (WP_182963883.1)and, Type_I_REases (WP_182963884.1).

![rm](/rm/RM_Type_II.svg){max-width=750px}

RM_Type_II subsystem in the genome of *Mannheimia haemolytica* (GCF_007965905.1) is composed of 2 proteins: Type_II_MTases (WP_006248352.1)and, Type_II_REases (WP_006253295.1).

![rm](/rm/RM_Type_IIG.svg){max-width=750px}

RM_Type_IIG subsystem in the genome of *Spirochaeta africana* (GCF_000242595.2) is composed of 1 protein: Type_IIG (WP_014455422.1).

![rm](/rm/RM_Type_III.svg){max-width=750px}

RM_Type_III subsystem in the genome of *Pannonibacter phragmitetus* (GCF_001484065.1) is composed of 2 proteins: Type_III_MTases (WP_058898889.1)and, Type_III_REases (WP_058898890.1).

![rm](/rm/RM_Type_IV.svg){max-width=750px}

RM_Type_IV subsystem in the genome of *Clostridioides difficile* (GCF_018884605.1) is composed of 1 protein: Type_IV_REases (WP_021364579.1).

## Distribution of the system among prokaryotes

The RM system is present in a total of 4699 different species.

Among the 22k complete genomes of RefSeq, this system is present in 19087 genomes (83.7 %).

![rm](/rm/Distribution_RM.svg){max-width=750px}

*Proportion of genome encoding the RM system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### DISARM_1

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_1,DISARM_1__drmD,0,DF-plddts_85.45851.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_1,DISARM_1__drmMI,0,DF-plddts_86.22485.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_1,DISARM__drmA,0,DF-plddts_88.08452.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_1,DISARM__drmB,0,DF-plddts_88.41231.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_1,DISARM__drmC,0,DF-plddts_93.3381.pdb
---
::

### DISARM_2

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_2,DISARM_2__drmE,0,V-plddts_88.46395.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_2,DISARM_2__drmMII,0,V-plddts_92.6996.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_2,DISARM__drmA,0,V-plddts_87.64454.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_2,DISARM__drmB,0,V-plddts_89.69894.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/DISARM_2,DISARM__drmC,0,V-plddts_87.93933.pdb
---
::

### RM

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/RM__Type_I_REases-plddts_86.33796.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /rm/RM__Type_I_S-plddts_91.98582.pdb
---
::

## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1093/nar/gku734

---
::

