---
title: BREX
layout: article
tableColumns:
    article:
      doi: 10.15252/embj.201489455
      abstract: |
        The perpetual arms race between bacteria and phage has resulted in the evolution of efficient resistance systems that protect bacteria from phage infection. Such systems, which include the CRISPR-Cas and restriction-modification systems, have proven to be invaluable in the biotechnology and dairy industries. Here, we report on a six-gene cassette in Bacillus cereus which, when integrated into the Bacillus subtilis genome, confers resistance to a broad range of phages, including both virulent and temperate ones. This cassette includes a putative Lon-like protease, an alkaline phosphatase domain protein, a putative RNA-binding protein, a DNA methylase, an ATPase-domain protein, and a protein of unknown function. We denote this novel defense system BREX (Bacteriophage Exclusion) and show that it allows phage adsorption but blocks phage DNA replication. Furthermore, our results suggest that methylation on non-palindromic TAGGAG motifs in the bacterial genome guides self/non-self discrimination and is essential for the defensive function of the BREX system. However, unlike restriction-modification systems, phage DNA does not appear to be cleaved or degraded by BREX, suggesting a novel mechanism of defense. Pan genomic analysis revealed that BREX and BREX-like systems, including the distantly related Pgl system described in Streptomyces coelicolor, are widely distributed in ~10% of all sequenced microbial genomes and can be divided into six coherent subtypes in which the gene composition and order is conserved. Finally, we detected a phage family that evades the BREX defense, implying that anti-BREX mechanisms may have evolved in some phages as part of their arms race with bacteria.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00069, PF00176, PF00270, PF00271, PF01507, PF01555, PF02384, PF04851, PF07669, PF07714, PF08378, PF08665, PF08747, PF08849, PF10923, PF13337, PF16565
relevantAbstracts:
    - doi: 10.1093/nar/gkaa290
    - doi: 10.1093/nar/gky1125
    - doi: 10.15252/embj.201489455
---

# BREX
## Description


BREX (for Bacteriophage Exclusion) is a family of anti-phage defense systems. BREX systems are active against both lytic and lysogenic phages. They allow phage adsorption but block phage DNA replication, and are considered to be [RM](/defense-systems/rm)-like systems (1,2). BREX systems are found in around 10% of sequenced microbial genomes (1).

BREX systems can be divided into six subtypes, and are encoded by 4 to 8 genes, some of these genes being mandatory while others are subtype-specific (1).

## Molecular mechanism

*B. cereus* BREX Type 1 system was reported to methylate target motifs in the bacterial genome (1). The methylation activity of this system has been hypothesized to allow for self from non-self discrimination, as it is the case for Restriction-Modification ([RM)](/defense-systems/rm) systems. 

However, the mechanism through which BREX Type 1 systems defend against phages is distinct from RM systems, and does not seem to degrade phage nucleic acids (1). 

To date, BREX molecular mechanism remains to be described.


## Example of genomic structure

The BREX system have been describe in a total of 6 subsystems.

BREX systems necessarily include the pglZ gene (encoding for a putative alkaline phosphatase), which is accompanied by either brxC or pglY. These two genes share only a distant homology but have been hypothesized to fulfill the same function among the different BREX subtypes (1).

Goldfarb and colleagues reported a 6-gene cassette from *Bacillus cereus* as being the model for BREX Type 1. BREX Type 1 are the most widespread BREX systems, and present two core genes (pglZ and brxC).  Four other genes  are associated with BREX Type 1 : *pglX (*encoding for a putative methyltransferase),  *brxA (*encoding an RNA-binding anti-termination protein)*, brxB (*unknown functio*n), brxC (*encoding for a protein with ATP-binding domain) and *brxL* (encoding for a putative protease) (1,2).

Type 2 BREX systems include the system formerly known as Pgl , which is comprised of four genes  (pglW, X, Y, and Z) (3), to which Goldfarb and colleagues found often associated two additional genes (brxD, and brxHI).

Although 4 additional BREX subtypes have been proposed, BREX Type 1 and Type 2 remain the only ones to be experimentally validated. A detailed description of the other subtypes can be found in Goldfarb *et al*., 2015.

Here is some example found in the RefSeq database:

![brex](/brex/BREX_I.svg){max-width=750px}

BREX_I subsystem in the genome of *Kaistella sp.* (GCF_020410745.1) is composed of 6 proteins: brxL (WP_226063319.1), pglZA (WP_226063320.1), pglX1 (WP_226063321.1), brxC (WP_226063322.1), brxB_DUF1788 (WP_226063323.1)and, brxA_DUF1819 (WP_226063324.1).

![brex](/brex/BREX_II.svg){max-width=750px}

BREX_II subsystem in the genome of *Streptomyces hygroscopicus* (GCF_001447075.1) is composed of 5 proteins: brxD (WP_058082289.1), pglZ2 (WP_058082290.1), pglY (WP_058082291.1), pglX2 (WP_058082292.1)and, pglW (WP_237280966.1).

## Distribution of the system among prokaryotes

The BREX system is present in a total of 732 different species.

Among the 22k complete genomes of RefSeq, this system is present in 1612 genomes (7.1 %).

![brex](/brex/Distribution_BREX.svg){max-width=750px}

*Proportion of genome encoding the BREX system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### BREX_II

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_II,BREX__brxD,0,DF-plddts_88.40179.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_II,BREX__pglW,0,DF-plddts_77.91126.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_II,BREX__pglX2,0,DF-plddts_87.98644.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_II,BREX__pglY,0,DF-plddts_83.07386.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_II,BREX__pglZ2,0,DF-plddts_86.2672.pdb
---
::

### BREX_III

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_III,BREX__brxA,0,DF-plddts_92.03753.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_III,BREX__brxC,0,DF-plddts_85.62129.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_III,BREX__brxF,0,DF-plddts_95.59973.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_III,BREX__brxHII,0,DF-plddts_82.85526.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_III,BREX__pglXI,0,DF-plddts_86.98487.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_III,BREX__pglZ3,0,DF-plddts_89.03152.pdb
---
::

### BREX_IV

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_IV,BREX__PglZ,0,DF-plddts_86.90171.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_IV,BREX__brxC,0,DF-plddts_85.82022.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_IV,BREX__brxL,0,DF-plddts_92.06923.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_IV,BREX__brxP,0,DF-plddts_88.53431.pdb
---
::

### BREX_V

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_V,BREX__brxA_DUF1819,0,DF-plddts_96.04893.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_V,BREX__brxB_DUF1788,0,DF-plddts_90.45365.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_V,BREX__brxC,0,DF-plddts_82.61479.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_V,BREX__brxHII,0,DF-plddts_83.9134.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_V,BREX__pglX1,0,DF-plddts_92.49306.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_V,BREX__pglZA,0,DF-plddts_92.2016.pdb
---
::

### BREX_VI

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_VI,BREX__brxB_DUF1788,0,DF-plddts_91.60152.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_VI,BREX__brxC,0,DF-plddts_86.1139.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_VI,BREX__brxD,0,DF-plddts_90.67427.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_VI,BREX__brxE,0,DF-plddts_90.30329.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_VI,BREX__brxHI,0,DF-plddts_87.87451.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_VI,BREX__pglX1,0,DF-plddts_78.09923.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_VI,BREX__pglZA,0,DF-plddts_88.59061.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /brex/BREX_VI,BREX_brxA,0,DF-plddts_93.93224.pdb
---
::

## Experimental validation

<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Gordeeva_2017[<a href='https://doi.org/10.1093/nar/gky1125'>Gordeeva et al., 2019</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_085962535.1'>WP_085962535.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_000566901.1'>WP_000566901.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_001019648.1'>WP_001019648.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_021524842.1'>WP_021524842.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/WP_001180895.1'>WP_001180895.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001193074.1'>WP_001193074.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda
    Goldfarb_2015[<a href='https://doi.org/10.15252/embj.201489455'>Goldfarb et al., 2015</a>] --> Origin_1
    Origin_1[Bacillus cereus 
<a href='https://ncbi.nlm.nih.gov/protein/ZP_02596040.1'>ZP_02596040.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ZP_02596039.1'>ZP_02596039.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ZP_02596038.1'>ZP_02596038.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ZP_02596037.1'>ZP_02596037.1</a>,
<a href='https://ncbi.nlm.nih.gov/protein/ZP_02596036.1'>ZP_02596036.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/ZP_02596035.1'>ZP_02596035.1</a>] --> Expressed_1[Bacillus subtilis ]
    Expressed_1[Bacillus subtilis ] ----> SPbeta & SP16 & Zeta & phi3T & SPO2 & SPO1 & SP82G
    subgraph Title1[Reference]
        Gao_2020
        Gordeeva_2017
        Goldfarb_2015
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
        Origin_1
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        Lambda
        Lambda
        SPbeta
        SP16
        Zeta
        phi3T
        SPO2
        SPO1
        SP82G
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
