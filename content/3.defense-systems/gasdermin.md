---
title: GasderMIN
layout: article
tableColumns:
    article:
      doi: 10.1126/science.abj8432
      abstract: |
        Gasdermin proteins form large membrane pores in human cells that release immune cytokines and induce lytic cell death. Gasdermin pore formation is triggered by caspase-mediated cleavage during inflammasome signaling and is critical for defense against pathogens and cancer. We discovered gasdermin homologs encoded in bacteria that defended against phages and executed cell death. Structures of bacterial gasdermins revealed a conserved pore-forming domain that was stabilized in the inactive state with a buried lipid modification. Bacterial gasdermins were activated by dedicated caspase-like proteases that catalyzed site-specific cleavage and the removal of an inhibitory C-terminal peptide. Release of autoinhibition induced the assembly of large and heterogeneous pores that disrupted membrane integrity. Thus, pyroptosis is an ancient form of regulated cell death shared between bacteria and animals.
    Sensor: Unknown
    Activator: Unknown
    Effector: Membrane disrupting
---

# GasderMIN
## Example of genomic structure

The GasderMIN system is composed of one protein: bGSDM.

Here is an example found in the RefSeq database: 

![gasdermin](/gasdermin/GasderMIN.svg){max-width=750px}

GasderMIN system in the genome of *Rhodoplanes sp.* (GCF_001579845.1) is composed of 1 protein: bGSDM (WP_068019379.1).

## Distribution of the system among prokaryotes

The GasderMIN system is present in a total of 25 different species.

Among the 22k complete genomes of RefSeq, this system is present in 29 genomes (0.1 %).

![gasdermin](/gasdermin/Distribution_GasderMIN.svg){max-width=750px}

*Proportion of genome encoding the GasderMIN system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### GasderMIN

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gasdermin/GasderMIN__ATPase-plddts_79.44181.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gasdermin/GasderMIN__Protease-plddts_85.56354.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gasdermin/GasderMIN__Protease1-plddts_86.2054.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gasdermin/GasderMIN__Protease2-plddts_85.10965.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gasdermin/GasderMIN__bGSDM-plddts_83.88977.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gasdermin/GasderMIN__bGSDM1-plddts_65.95252.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Johnson_2022[<a href='https://doi.org/10.1371/journal.pgen.1010065'>Johnson et al., 2022</a>] --> Origin_0
    Origin_0[Lysobacter enzymogenes 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2841794907'>2841794907</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2841794908'>2841794908</a>,
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2841794909'>2841794909</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2841794910'>2841794910</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T5 & T4 & T6
    subgraph Title1[Reference]
        Johnson_2022
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T5
        T4
        T6
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.abj8432

---
::
