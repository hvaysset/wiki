---
title: Dazbog
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2022.09.017
      abstract: |
        Bacterial anti-phage systems are frequently clustered in microbial genomes, forming defense islands. This property enabled the recent discovery of multiple defense systems based on their genomic co-localization with known systems, but the full arsenal of anti-phage mechanisms remains unknown. We report the discovery of 21 defense systems that protect bacteria from phages, based on computational genomic analyses and phage-infection experiments. We identified multiple systems with domains involved in eukaryotic antiviral immunity, including those homologous to the ubiquitin-like ISG15 protein, dynamin-like domains, and SEFIR domains, and show their participation in bacterial defenses. Additional systems include domains predicted to manipulate DNA and RNA molecules, alongside toxin-antitoxin systems shown here to function in anti-phage defense. These systems are widely distributed in microbial genomes, and in some bacteria, they form a considerable fraction of the immune arsenal. Our data substantially expand the inventory of defense systems utilized by bacteria to counteract phage infection.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
relevantAbstracts:
    - doi: 10.1016/j.chom.2022.09.017
contributors : 
    - Aude Bernheim
---

# Dazbog

## Description

The Dazbog system is composed of 2 proteins: DzbB and, DzbA. Dazbog was name after a slavic god, likely a solar deity.DzbA has homology to Pfam14072 :ref{doi=10.1016/j.chom.2022.09.017}

## Molecular mechanism
As far as we are aware, the molecular mechanism is unknown. 

## Example of genomic structure

The Dazbog system is composed of 2 proteins: DzbB and, DzbA.

Here is an example found in the RefSeq database: 

![dazbog](/dazbog/Dazbog.svg){max-width=750px}

Dazbog system in the genome of *Bacillus cereus* (GCF_001518875.1) is composed of 2 proteins: DzbA (WP_082188833.1)and, DzbB (WP_059303380.1).

## Distribution of the system among prokaryotes

The Dazbog system is present in a total of 66 different species.

Among the 22k complete genomes of RefSeq, this system is present in 73 genomes (0.3 %).

![dazbog](/dazbog/Distribution_Dazbog.svg){max-width=750px}

*Proportion of genome encoding the Dazbog system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Dazbog

Example 1:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /dazbog/Dazbog,Dazbog__DzbA,0,V-plddts_87.87307.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dazbog/Dazbog,Dazbog__DzbB,0,V-plddts_93.82789.pdb
---
::

Example 2:
::molstar-pdbe-plugin
---
height: 700
dataUrl: /dazbog/Dazbog,Dazbog__DzbA,0,V-plddts_87.87307.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dazbog/Dazbog,Dazbog__DzbB,0,V-plddts_93.82789.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus cereus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2689007484'>2689007484</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2689007485'>2689007485</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T2 & T4 & T6 & T5 & T7
    Millman_2022[<a href='https://doi.org/10.1016/j.chom.2022.09.017'>Millman et al., 2022</a>] --> Origin_0
    Origin_0[Bacillus cereus 
<a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2689007484'>2689007484</a>, <a href='https://img.jgi.doe.gov/cgi-bin/m/main.cgi?section=GeneDetail&page=geneDetail&gene_oid=2689007485'>2689007485</a>] --> Expressed_1[Bacillus subtilis]
    Expressed_1[Bacillus subtilis] ----> Fado & SPR
    subgraph Title1[Reference]
        Millman_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        T2
        T4
        T6
        T5
        T7
        Fado
        SPR
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>


