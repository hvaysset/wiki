---
title: Stk2
layout: article
tableColumns:
    article:
      doi: 10.1016/j.chom.2016.08.010
      abstract: |
        Organisms from all domains of life are infected by viruses. In eukaryotes, serine/threonine kinases play a central role in antiviral response. Bacteria, however, are not commonly known to use protein phosphorylation as part of their defense against phages. Here we identify Stk2, a staphylococcal serine/threonine kinase that provides efficient immunity against bacteriophages by inducing abortive infection. A phage protein of unknown function activates the Stk2 kinase. This leads to the Stk2-dependent phosphorylation of several proteins involved in translation, global transcription control, cell-cycle control, stress response, DNA topology, DNA repair, and central metabolism. Bacterial host cells die as a consequence of Stk2 activation, thereby preventing propagation of the phage to the rest of the bacterial population. Our work shows that mechanisms of viral defense that rely on protein phosphorylation constitute a conserved antiviral strategy across multiple domains of life.
    Sensor: Sensing of phage protein
    Activator: Direct
    Effector: Other (protein modifying)
    PFAM: PF00069, PF07714
---

# Stk2
## Description

Eukaryotic-like serine/threonine kinases have a variety of functions in prokaryotes. Recently, a single-gene system (Stk2) encoding for a Serine/threonine kinase from Staphylococcus epidermidis has been found to have anti-phage activity both in its native host and in a heterologous S.aureus host (Depardieu et al., 2016). 

## Molecular mechanism

Stk2 is an Abortive infection system, which triggers cell death upon phage infection, probably through phosphorylation of diverse essential cellular pathways (Depardieu et al., 2016). Stk2 was shown to detect a phage protein named Pack, which was proposed to be involved in phage genome packaging (Depardieu et al., 2016)


## Example of genomic structure

The Stk2 system is composed of one protein: Stk2.

Here is an example found in the RefSeq database: 

![stk2](/stk2/Stk2.svg){max-width=750px}

Stk2 system in the genome of *Staphylococcus aureus* (GCF_009739755.1) is composed of 1 protein: Stk2 (WP_001001347.1).

## Distribution of the system among prokaryotes

The Stk2 system is present in a total of 29 different species.

Among the 22k complete genomes of RefSeq, this system is present in 141 genomes (0.6 %).

![stk2](/stk2/Distribution_Stk2.svg){max-width=750px}

*Proportion of genome encoding the Stk2 system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Stk2

::molstar-pdbe-plugin
---
height: 700
dataUrl: /stk2/Stk2__Stk2-plddts_91.79541.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Depardieu_2016[<a href='https://doi.org/10.1016/j.chom.2016.08.010'>Depardieu et al., 2016</a>] --> Origin_0
    Origin_0[Staphylococcus epidermidis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001001347.1'>WP_001001347.1</a>] --> Expressed_0[Staphylococcus epidermidis]
    Expressed_0[Staphylococcus epidermidis] ----> CNPx
    Depardieu_2016[<a href='https://doi.org/10.1016/j.chom.2016.08.010'>Depardieu et al., 2016</a>] --> Origin_0
    Origin_0[Staphylococcus epidermidis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_001001347.1'>WP_001001347.1</a>] --> Expressed_1[Staphylococcus aureus]
    Expressed_1[Staphylococcus aureus] ----> phage80alpha & phage85 & phiNM1 & phiNM2 & phiNM4
    subgraph Title1[Reference]
        Depardieu_2016
end
    subgraph Title2[System origin]
        Origin_0
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
end
    subgraph Title4[Phage infected]
        CNPx
        phage80alpha
        phage85
        phiNM1
        phiNM2
        phiNM4
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1016/j.chom.2016.08.010

---
::

