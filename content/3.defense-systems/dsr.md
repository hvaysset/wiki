---
title: Dsr
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Sensing phage protein
    Activator: Direct
    Effector: Nucleotide modifying
    PFAM: PF13289
---

# Dsr
## Example of genomic structure

The Dsr system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

![dsr](/dsr/Dsr_I.svg){max-width=750px}

Dsr_I subsystem in the genome of *Escherichia coli* (GCF_016904235.1) is composed of 1 protein: Dsr1 (WP_204608492.1).

![dsr](/dsr/Dsr_II.svg){max-width=750px}

Dsr_II subsystem in the genome of *Escherichia coli* (GCF_009950125.1) is composed of 1 protein: Dsr2 (WP_178103017.1).

## Distribution of the system among prokaryotes

The Dsr system is present in a total of 246 different species.

Among the 22k complete genomes of RefSeq, this system is present in 641 genomes (2.8 %).

![dsr](/dsr/Distribution_Dsr.svg){max-width=750px}

*Proportion of genome encoding the Dsr system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### Dsr_I

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dsr/Dsr_I__Dsr1-plddts_87.99578.pdb
---
::

### Dsr_II

::molstar-pdbe-plugin
---
height: 700
dataUrl: /dsr/Dsr_II__Dsr2-plddts_86.62203.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[ DSR1
Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_029488749.1'>WP_029488749.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T3 & T7 & PhiV-1
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_1
    Origin_1[ DSR2
Cronobacter sakazakii 
<a href='https://ncbi.nlm.nih.gov/protein/WP_015387030.1'>WP_015387030.1</a>] --> Expressed_1[Escherichia coli]
    Expressed_1[Escherichia coli] ----> Lambda
    Garb_2022[<a href='https://doi.org/10.1038/s41564-022-01207-8'>Garb et al., 2022</a>] --> Origin_2
    Origin_2[ DSR2
Bacillus subtilis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_029317421.1'>WP_029317421.1</a>] --> Expressed_2[Bacillus subtilis ]
    Expressed_2[Bacillus subtilis ] ----> SPR
    Garb_2022[<a href='https://doi.org/10.1038/s41564-022-01207-8'>Garb et al., 2022</a>] --> Origin_3
    Origin_3[ DSR1
Bacillus subtilis 
<a href='https://ncbi.nlm.nih.gov/protein/WP_053564071.1'>WP_053564071.1</a>] --> Expressed_3[Bacillus subtilis]
    Expressed_3[Bacillus subtilis] ----> phi29
    subgraph Title1[Reference]
        Gao_2020
        Garb_2022
end
    subgraph Title2[System origin]
        Origin_0
        Origin_1
        Origin_2
        Origin_3
end
    subgraph Title3[Expression species]
        Expressed_0
        Expressed_1
        Expressed_2
        Expressed_3
end
    subgraph Title4[Phage infected]
        T3
        T7
        PhiV-1
        Lambda
        SPR
        phi29
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1038/s41564-022-01207-8
    - doi: 10.1126/science.aba0372

---
::

