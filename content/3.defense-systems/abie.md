---
title: AbiE
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF08843, PF09407, PF09952, PF11459, PF13338, PF17194
---

# AbiE
AbiE is a family of an anti-phage defense systems. They act through a Toxin-Antitoxin mechanism, and are comprised of a pair of genes, with one gene being toxic while the other confers immunity to this toxicity. 

It is classified as an Abortive infection system.

## Molecular mechanism

AbiE systems are encoded by two mandatory genes, abiEi and abiEii (1,2).  The latter encodes for AbiEii, a GTP-binding nucleotidyltransferase (NTase) which expression induce a reversible growth arrest.  On the other hand, abiEi encodes for a AbiEi a transcriptional autorepressor that  binds to the promoter of the abiE operon.

Based on this mechanisms, AbiE systems are classified as Type IV Toxin-Antitoxin system, where the antitoxin and toxin are both proteins that do not directly interact with each other.

## Example of genomic structure

The AbiE system is composed of 2 proteins: AbiEi_1 and, AbiEii.

Here is an example found in the RefSeq database: 

![abie](/abie/AbiE.svg){max-width=750px}

AbiE system in the genome of *Desulfuromonas versatilis* (GCF_019704135.1) is composed of 2 proteins: AbiEi_1 (WP_221251730.1)and, AbiEii (WP_221251731.1).

## Distribution of the system among prokaryotes

The AbiE system is present in a total of 962 different species.

Among the 22k complete genomes of RefSeq, this system is present in 3742 genomes (16.4 %).

![abie](/abie/Distribution_AbiE.svg){max-width=750px}

*Proportion of genome encoding the AbiE system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### AbiE

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abie/AbiE,AbiE__AbiEi,0,V-plddts_85.81224.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abie/AbiE,AbiE__AbiEii,0,V-plddts_90.73768.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/AAB52382.1'>AAB52382.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/AAB52383.1'>AAB52383.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        936
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1093/nar/gkt1419

---
::

