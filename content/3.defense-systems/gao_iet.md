---
title: Gao_Iet
layout: article
tableColumns:
    article:
      doi: 10.1126/science.aba0372
      abstract: |
        Bacteria and archaea are frequently attacked by viruses and other mobile genetic elements and rely on dedicated antiviral defense systems, such as restriction endonucleases and CRISPR, to survive. The enormous diversity of viruses suggests that more types of defense systems exist than are currently known. By systematic defense gene prediction and heterologous reconstitution, here we discover 29 widespread antiviral gene cassettes, collectively present in 32% of all sequenced bacterial and archaeal genomes, that mediate protection against specific bacteriophages. These systems incorporate enzymatic activities not previously implicated in antiviral defense, including RNA editing and retron satellite DNA synthesis. In addition, we computationally predict a diverse set of other putative defense genes that remain to be characterized. These results highlight an immense array of molecular functions that microbes use against viruses.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00004, PF00082
---

# Gao_Iet
## Example of genomic structure

The Gao_Iet system is composed of 2 proteins: IetS and, IetA.

Here is an example found in the RefSeq database: 

![gao_iet](/gao_iet/Gao_Iet.svg){max-width=750px}

Gao_Iet system in the genome of *Escherichia coli* (GCF_014169855.1) is composed of 2 proteins: IetS (WP_001551050.1)and, IetA (WP_000385105.1).

## Distribution of the system among prokaryotes

The Gao_Iet system is present in a total of 189 different species.

Among the 22k complete genomes of RefSeq, this system is present in 436 genomes (1.9 %).

![gao_iet](/gao_iet/Distribution_Gao_Iet.svg){max-width=750px}

*Proportion of genome encoding the Gao_Iet system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Gao_Iet

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_iet/Gao_Iet,Gao_Iet__IetA,0,V-plddts_85.57017.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /gao_iet/Gao_Iet,Gao_Iet__IetS,0,V-plddts_90.4167.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Gao_2020[<a href='https://doi.org/10.1126/science.aba0372'>Gao et al., 2020</a>] --> Origin_0
    Origin_0[Escherichia coli 
<a href='https://ncbi.nlm.nih.gov/protein/WP_000385105.1'>WP_000385105.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_001551050.1'>WP_001551050.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> Lambda & T3 & T7 & PhiV-1
    subgraph Title1[Reference]
        Gao_2020
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        Lambda
        T3
        T7
        PhiV-1
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1126/science.aba0372

---
::

