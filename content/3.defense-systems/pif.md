---
title: Pif
layout: article
tableColumns:
    article:
      doi: 10.1007/BF00327934
      abstract: |
        We report the molecular cloning of the pif region of the F plasmid and its physical dissection by subcloning and deletion analysis. Examination of the polypeptide products synthesized in maxicells by plasmids carrying defined pif sequences has shown that the region specifies at least two proteins of molecular weights 80,000 and 40,000, the genes for which appear to lie in the same transcriptional unit. In addition, analysis of pif-lacZ fusion plasmids has detected a pif promoter and determined the direction of transcription across the pif region.
    Sensor: Sensing of phage protein
    Activator: Unknown
    Effector: Membrane disrupting (?)
    PFAM: PF07693
---

# Pif
## Example of genomic structure

The Pif system is composed of 2 proteins: PifC and, PifA.

Here is an example found in the RefSeq database: 

![pif](/pif/Pif.svg){max-width=750px}

Pif system in the genome of *Escherichia coli* (GCF_018628815.1) is composed of 2 proteins: PifA (WP_000698737.1)and, PifC (WP_000952217.1).

## Distribution of the system among prokaryotes

The Pif system is present in a total of 28 different species.

Among the 22k complete genomes of RefSeq, this system is present in 143 genomes (0.6 %).

![pif](/pif/Distribution_Pif.svg){max-width=750px}

*Proportion of genome encoding the Pif system for the 14 phyla with more than 50 genomes in the RefSeq database.* 

## Structure

### Pif

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pif/Pif,Pif__PifA,0,V-plddts_83.95828.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /pif/Pif,Pif__PifC,0,V-plddts_91.50188.pdb
---
::

## Experimental validation
<mermaid>
graph LR;
    Cheng_2004[<a href='https://doi.org/10.1016/j.virol.2004.06.001'>Cheng et al., 2004</a>] --> Origin_0
    Origin_0[Escherichia coli F-plasmid 
<a href='https://ncbi.nlm.nih.gov/protein/WP_028985935.1'>WP_028985935.1</a>, <a href='https://ncbi.nlm.nih.gov/protein/WP_028985936.1'>WP_028985936.1</a>] --> Expressed_0[Escherichia coli]
    Expressed_0[Escherichia coli] ----> T7
    subgraph Title1[Reference]
        Cheng_2004
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        T7
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
## Relevant abstracts

::relevant-abstracts
---
items:
    - doi: 10.1007/BF00327934
    - doi: 10.1016/j.virol.2004.06.001
    - doi: 10.1128/jb.173.20.6507-6514.1991

---
::

