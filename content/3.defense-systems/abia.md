---
title: AbiA
layout: article
tableColumns:
    article:
      doi: 10.1016/j.mib.2005.06.006
      abstract: |
        Abortive infection (Abi) systems, also called phage exclusion, block phage multiplication and cause premature bacterial cell death upon phage infection. This decreases the number of progeny particles and limits their spread to other cells allowing the bacterial population to survive. Twenty Abi systems have been isolated in Lactococcus lactis, a bacterium used in cheese-making fermentation processes, where phage attacks are of economical importance. Recent insights in their expression and mode of action indicate that, behind diverse phenotypic and molecular effects, lactococcal Abis share common traits with the well-studied Escherichia coli systems Lit and Prr. Abis are widespread in bacteria, and recent analysis indicates that Abis might have additional roles other than conferring phage resistance.
    Sensor: Unknown
    Activator: Unknown
    Effector: Unknown
    PFAM: PF00078, PF18160, PF18732
relevantAbstracts:
    - doi: 10.1023/A:1002027321171
    - doi: 10.1016/j.mib.2005.06.006
    - doi: 10.1093/nar/gkac467
---

# AbiA 


The AbiA system have been describe in a total of 2 subsystems.

Here is some example found in the RefSeq database:

![abia](/abia/AbiA_large.svg){max-width=750px}

AbiA_large subsystem in the genome of *Lactobacillus amylovorus* (GCF_002706375.1) is composed of 1 protein: AbiA_large (WP_056940268.1).

![abia](/abia/AbiA_small.svg){max-width=750px}

AbiA_small subsystem in the genome of *Mesobacillus foraminis* (GCF_003667765.1) is composed of 2 proteins: AbiA_small (WP_121614402.1)and, AbiA_SLATT (WP_121614403.1).

## Distribution of the system among prokaryotes 

The AbiA system is present in a total of 35 different species.



Among the 22k complete genomes of RefSeq, this system is present in 50 genomes (0.2 %).

![abia](/abia/Distribution_AbiA.svg){max-width=750px}

*Proportion of genome encoding the AbiA system for the 14 phyla with more than 50 genomes in the RefSeq database.* *Pie chart of the repartition of all the subsystems found in the RefSeq database.*

## Structure

### AbiA_large

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abia/AbiA_large__AbiA_large-plddts_90.82916.pdb
---
::

### AbiA_small

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abia/AbiA_small,AbiA_small__AbiA_SLATT,0,DF-plddts_94.13374.pdb
---
::

::molstar-pdbe-plugin
---
height: 700
dataUrl: /abia/AbiA_small,AbiA_small__AbiA_small,0,DF-plddts_89.82347.pdb
---
::

## Experimental validation

<mermaid>
graph LR;
    Chopin_2005[<a href='https://doi.org/10.1016/j.mib.2005.06.006'>Chopin et al., 2005</a>] --> Origin_0
    Origin_0[lactococcal plasmid  
<a href='https://ncbi.nlm.nih.gov/protein/AAA65072.1'>AAA65072.1</a>] --> Expressed_0[lactococci]
    Expressed_0[lactococci] ----> 936 & c2 & P335
    subgraph Title1[Reference]
        Chopin_2005
end
    subgraph Title2[System origin]
        Origin_0
end
    subgraph Title3[Expression species]
        Expressed_0
end
    subgraph Title4[Phage infected]
        936
        c2
        P335
end
    style Title1 fill:none,stroke:none,stroke-width:none
    style Title2 fill:none,stroke:none,stroke-width:none
    style Title3 fill:none,stroke:none,stroke-width:none
    style Title4 fill:none,stroke:none,stroke-width:none
</mermaid>
