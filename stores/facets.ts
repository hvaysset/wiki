import { defineStore } from 'pinia'
import type { FacetDistribution, FacetStat } from 'meilisearch';


export interface Facets {
    facetDistribution: FacetDistribution | undefined;
    facetStat: FacetStat | undefined;
}

export const useFacetsStore = defineStore('facets', () => {


    const facets: Ref<Facets> = ref({ facetDistribution: undefined, facetStat: undefined })


    function setFacets(newFacets: Facets) {
        facets.value = newFacets

    }


    return { facets, setFacets }
})