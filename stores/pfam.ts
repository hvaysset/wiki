import { defineStore } from 'pinia'
import { ref } from 'vue'
import * as d3 from "d3";
import { unref } from 'vue'

export interface PfamContent {
    body: PfamHmm[]
}

export interface PfamHmm {
    AC: string
    CL: string
    DE: string
    GA: string
    ID: string
    ML: string
    TP: string
}

export const usePfamStore = defineStore('pfam', () => {
    const pfam = ref(new Map<string, PfamHmm>())



    async function initPfam() {
        if (pfam.value.size < 1) {
            const data = await d3.csv("/pfam-a-hmm.csv"); 
            if (data.length > 1) {
                pfam.value = new Map(data.map(pfam => {
                    return [pfam.AC.split(".")[0], { ...unref(pfam) }];
                }))
            }
        }
    }
    return { pfam, initPfam }
})