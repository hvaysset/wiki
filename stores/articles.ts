import { defineStore } from 'pinia'
import { ref } from 'vue'


export interface CslJson {
    id: string
    type: string
    title: string
    "container-title": string
    page: string,
    volume: string,
    abstract: string
    URL: string
    DOI: string
    journalAbbreviation: string
    language: string
    author: Array<{ family: string, given: string }>
    issued: {
        "date-parts": Array<string>
    },
}

export const useArticlesStore = defineStore('articles', () => {
    const articles = ref(new Map<string, CslJson>())
    function add(article: CslJson) {
        const doi = article.DOI
        if (articles.value.has(doi)) return
        articles.value.set(article.DOI, article)
    }

    return { articles, add }
})